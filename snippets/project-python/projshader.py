# Pygame/PyopenGL example by Bastiaan Zap, May 2009
#
# Projective Textures
#
# Employed techniques:
#
# - Vertex and Fragment shaders
# - Display Lists
# - Texturing

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.arrays import vbo
from OpenGL.raw.GL.ARB.vertex_array_object import glGenVertexArrays, glBindVertexArray
from OpenGL.GL.framebufferobjects import *

import random
from math import * # trigonometry
import math

import pygame # just to get a display

import sys
import time

import shader
import projutil
import plyreader
import transformations
import numpy as np
from ctypes import *
from numpy.linalg import inv

# get an OpenGL surface

pygame.init()
pygame.display.set_mode((1024,1024), pygame.OPENGL|pygame.DOUBLEBUF)

# Create and Compile a shader
# but fail with a meaningful message if something goes wrong

vertex_shader = shader.createAndCompileShader(GL_VERTEX_SHADER,"""
varying vec3 normal, lightDir, lookDir;
varying vec4 vertex_color;

void main()
{
  normal = gl_NormalMatrix * gl_Normal;
  vec4 posVert = gl_ModelViewMatrix * gl_Vertex;
  vec4 lightPos = gl_ModelViewMatrix * gl_LightSource[0].position;
  vec4 lightPos2 = gl_ModelViewMatrix * gl_LightSource[1].position;
  lookDir = vec3( lightPos2.xyz );

  // Put World coordinates of Vertex, multiplied by TextureMatrix[0]
  // into TexCoord[0]
  gl_TexCoord[0] = gl_TextureMatrix[0]*gl_ModelViewMatrix*gl_Vertex;
  lightDir = vec3( lightPos.xyz-posVert.xyz );
  gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
  vertex_color = gl_Color;
}
""")

fragment_shader = shader.createAndCompileShader(GL_FRAGMENT_SHADER,"""
uniform sampler2D projMap;
varying vec3 lookDir;
varying vec3 normal, lightDir;
varying vec4 vertex_color;

void main (void)
{
  //vec4 final_color = 0.15 * vertex_color + (gl_FrontLightModelProduct.sceneColor * gl_FrontMaterial.ambient) + 
  //  (gl_LightSource[0].ambient * gl_FrontMaterial.ambient);
  vec4 final_color = (gl_FrontLightModelProduct.sceneColor * gl_FrontMaterial.ambient) + 
    (gl_LightSource[0].ambient * gl_FrontMaterial.ambient);

  vec3 L = normalize(lightDir);
  vec3 N = normalize(lookDir);

  float lambert = dot(N,L);

  if( gl_TexCoord[0].z>0.0 ) // in front of projector and facing projector?
  {
    // project texture - see notes for pitfall
    vec4 ProjMapColor = texture2DProj(projMap, gl_TexCoord[0].xyz);
    if (( ProjMapColor[0] > 0.0 ) && ( ProjMapColor[1] > 0.0 ) && ( ProjMapColor[2] > 0.0 )) {
        final_color = ProjMapColor;
        //final_color[3] = lambert;
    } else {
        final_color = vec4(0.0,0.0,0.0,0.0);
    }
    //final_color += ProjMapColor*lambert*pow(length(L),-2.0);
  }

  gl_FragColor = final_color;
  //gl_FragColor = vertex_color;
}
""")

# build shader program

program = glCreateProgram()
glAttachShader(program,vertex_shader)
glAttachShader(program,fragment_shader)
glLinkProgram(program)

# try to activate/enable shader program
# handle errors wisely

try:
    glUseProgram(program)
except OpenGL.error.GLError:
    print glGetProgramInfoLog(program)
    raise

done = False

t=0

textures = []
for i in xrange( 15 ):
    textures.append( projutil.loadImage( i, imageName = "/tmp/pmvs/bundler/visualize/%08d.jpg"%( i ) ) )

cams = plyreader.read_bundler( '/tmp/pmvs/bundler/bundle.rd.out' )

glActiveTexture( GL_TEXTURE0 )
for idx, texture, w, h in textures:
    glBindTexture( GL_TEXTURE_2D, texture )

loc = glGetUniformLocation(program,"projMap");
glUniform1i(loc, 0) # use first texturing unit in shader

glEnable(GL_DEPTH_TEST)

v,c,n,i = plyreader.read_ply( 'mesh2.ply' )
vertices = np.array(v, dtype=np.float32)
indices = np.array(i, dtype=np.int32)
print "numpied my arrays"

colours = np.array(c, dtype=np.uint8)
normals = np.array(n, dtype=np.float32)

vertexPositions = vbo.VBO(vertices)
indexPositions = vbo.VBO(indices, target=GL_ELEMENT_ARRAY_BUFFER)
colorPositions = vbo.VBO(colours)
normalPositions = vbo.VBO(normals)

y = 2.0
z = -5.0

render_w = 512
render_h = 512

rendertarget = glGenTextures( 1 )

glBindTexture( GL_TEXTURE_2D, rendertarget )
glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                 GL_REPEAT);
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                 GL_REPEAT );
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

# occupy 1024x1024 texture memory

glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,render_w,render_h,0,GL_RGBA, GL_UNSIGNED_BYTE, None)

glBindTexture( GL_TEXTURE_2D, rendertarget )
glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                 GL_REPEAT);
glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                 GL_REPEAT );
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

fbo=c_uint(1) # WTF? Did not find a way to get there easier
# A simple number would always result in a "Segmentation
# Fault" for me

glGenFramebuffers(1)

newtarget = 0
while not done:

    for event in pygame.event.get():
        pass

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        y -= 0.05
    if keys[pygame.K_RIGHT]:
        y += 0.05
    if keys[pygame.K_UP]:
        z -= 0.05
    if keys[pygame.K_DOWN]:
        z += 0.05

    # for event in pygame.event.get():
       #  if event.type == pygame.KEYPRESSED:

    t=t+1

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo)
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, 
        GL_TEXTURE_2D, rendertarget, 0)

    # render to texture
    glPushAttrib(GL_VIEWPORT_BIT)
    glViewport(0, 0, render_w, render_h)

    # Projector position and angle - this is rather rough so far

    # the texture matrix stores the intended projection
    # however, signs seem to be reversed. This somehow makes sense, as
    # we're transforming the texture coordinates
    for idx, texture, w, h in textures:
        if ( idx == newtarget ):
            break

    glBindTexture( GL_TEXTURE_2D, texture )

    glMatrixMode(GL_TEXTURE)
    glLoadIdentity()
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE )
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER )
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER )
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

    # transform and bias
    glTranslatef(.5, .5, .0)
    glScalef(.5, .5, 1.0)

    ( focal, R, T ) = cams[ idx ]
    fov = (2.0*math.atan2( int(h), (2*focal) )) * 180 / math.pi

    b = T # -R.dot(T)

    m = np.matrix(R)

    r,s,t = transformations.euler_from_matrix( m, 'sxyz' )

    # Change perspective here to 2*atan( height / (2*focal_length) )
    aspect = float(w) / float(h)
    gluPerspective( fov, aspect, 0.000001, 1 )

    ra = r*180.0/math.pi
    sa = s*180.0/math.pi
    ta = t*180.0/math.pi

    glTranslate(b[0],b[1],b[2])
    glRotate( ta, 0, 0, 1 )
    glRotate( sa, 0, 1, 0 )
    glRotate( ra, 1, 0, 0 )

    #loc = glGetUniformLocation(program,"lookDir");
    #glUniform3f(loc, b[0],b[1],b[2]) # use first texturing unit in shader
    
    # Set view
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    #gluPerspective(90,1,0.0001,1000)
    glOrtho(-5, 5, -5, 5, 0.000001, 1000)
    #gluLookAt(0,2,-1,0,2,0,0,1,0)
    gluLookAt(0,y,-100,0,y,0,0,1,0)

    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glColor4f(1.0,1.0,1.0,0.0)

    vT = -R.dot( T )
    vU = -R.dot( [0.0,0.0,-1.0] )
    glLightfv(GL_LIGHT0,GL_POSITION,[vT[0],vT[1],vT[2]])
    glLightfv(GL_LIGHT1,GL_POSITION,[vU[0],vU[1],vU[2]])

    # VIEW STUFF

    indexPositions.bind()
    vertexPositions.bind()
    glEnableVertexAttribArray(0)

    glEnableClientState(GL_VERTEX_ARRAY)
    glVertexAttribPointer(0, 3, GL_FLOAT, False, 0, None)

    glEnableClientState(GL_COLOR_ARRAY)
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, False, 0, None)
    colorPositions.bind()
    glColorPointer( 3, GL_UNSIGNED_BYTE, 0, None )

    glEnableClientState( GL_NORMAL_ARRAY )
    normalPositions.bind()
    glEnableVertexAttribArray(2)
    glVertexAttribPointer(2, 3, GL_FLOAT, False, 0, None)
    glNormalPointer( GL_FLOAT, 0, None )
    # glEnable(GL_NORMALIZE)

    glDrawElements(GL_TRIANGLES, len(i)*3, GL_UNSIGNED_INT, None) #This line does work too!

    glPopAttrib() # Reset viewport to screen format
    # do not render to texture anymore - "switch off" rtt
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

    glBindTexture( GL_TEXTURE_2D, rendertarget )
    data = glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE)
    projutil.jpg_file_write( 'test', newtarget, data, render_w,render_h )

    time.sleep(0.01);

    pygame.display.flip()

    newtarget = newtarget + 1
    if (newtarget >= len(textures)):
        done = True
        break

pygame.quit()

