
import numpy as np

#ply
#format ascii 1.0
#comment Author: CloudCompare (TELECOM PARISTECH/EDF R&D)
#obj_info Generated by CloudCompare!
#element vertex 154889
#property float x
#property float y
#property float z
#property uchar red
#property uchar green
#property uchar blue
#property float nx
#property float ny
#property float nz
#property float scalar_Default
#element face 309134
#property list uchar int vertex_indices
#end_header

def read_ply( filename ):
    numV = 0
    numF = 0
    f = open( filename, 'r' )
    f.readline() # ply
    f.readline() # format
    f.readline() # comment
    f.readline() # obj_info
    elvertex = f.readline().strip()
    print "elvertex: ", elvertex
    line = f.readline()
    while ( line.startswith( "property" ) ):
        line = f.readline()
    elface = line.strip()
    line = f.readline()
    print "elface: ", elface
    while ( line.startswith( "property" ) ):
        line = f.readline()
    if ( line.startswith( "end_header" ) ):
        print "end reached"

    spl = elvertex.split(" ")
    numV = int( spl[2] )
    print numV
    
    spl = elface.split(" ")
    numF = int( spl[2] )
    print numF

    verts = []
    colours = []
    normals = []
    for i in xrange(numV):
        line = f.readline()
        vals = line.split( " " )
        vert = []
        colour = []
        normal = []
        vert.append( float(vals[0]) )
        vert.append( float(vals[1]) )
        vert.append( float(vals[2]) )
        colour.append( int(vals[3]) )
        colour.append( int(vals[4]) )
        colour.append( int(vals[5]) )
        normal.append( float(vals[6]) )
        normal.append( float(vals[7]) )
        normal.append( float(vals[8]) )
        verts.append( vert )
        colours.append( colour )
        normals.append( normal )

    indices = []
    for i in xrange(numF):
        line = f.readline()
        vals = line.split( " " )
        index = []
        index.append( int(vals[1]) )
        index.append( int(vals[2]) )
        index.append( int(vals[3]) )
        indices.append( index )

    print "finished reading ply"

    return verts, colours, normals, indices


# num_cams num_vertices
# focal length 0 0
# R
# T

## Bundle file v0.3
# 15 3251
#3490.106011461392 0.000000000000 0.000000000000
#0.999927802890 0.003444188067 -0.011512018805
#0.003091754919 -0.999530192632 -0.030493197100
#-0.011611634679 0.030455403239 -0.999468678025
#0.049210301111 0.141290972139 -0.279058537660

def read_bundler( filename ):
    f = open( filename, 'r' )
    f.readline() # # Bundle file v0.3
    line = f.readline().strip()
    numcams = int(line.split(" ")[0])

    cams = []
    for i in xrange(numcams):
        line = f.readline().strip()
        focal = float(line.split(" ")[0])
        line = f.readline().strip()
        R1 = line.split( " " )
        line = f.readline().strip()
        R2 = line.split( " " )
        line = f.readline().strip()
        R3 = line.split( " " )
        line = f.readline().strip()
        Ts = line.split( " " )

        R = np.array([[ float(R1[0]),float(R1[1]),float(R1[2]) ], [ float(R2[0]),float(R2[1]),float(R2[2]) ], [ float(R3[0]),float(R3[1]),float(R3[2]) ]])
        T = np.array([ float(Ts[0]),float(Ts[1]),float(Ts[2]) ])

        cams.append( ( focal, R, T ) )

    return cams

