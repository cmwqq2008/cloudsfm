from OpenGL.GL import *
from OpenGL.GLU import *

try: 
    from PIL import Image
except ImportError, err: 
    import Image

def loadImage( idx, imageName = "nehe_wall.bmp" ): 
    """Load an image file as a 2D texture using PIL"""
    im = Image.open(imageName) 
    # im = im.transpose( Image.FLIP_TOP_BOTTOM )
    try: 
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBA", 0, -1) 
    except SystemError: 
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)

    ID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, ID) 
    glPixelStorei(GL_UNPACK_ALIGNMENT,1)
    glTexImage2D( GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image ) 
    return idx, ID, ix, iy

def jpg_file_write(name, number, data, w, h):
    im = Image.frombuffer("RGBA", (w,h), data, "raw", "RGBA", 0, 0)
    fnumber = "%08d" % number
    im.save(name + fnumber + ".png")


