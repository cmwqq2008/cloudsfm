uniform int nTextures;
uniform sampler2DArray textures;
uniform vec3[] projectPos;

varying vec3 glPos;
varying vec3 normalVec;

void main() {
    int currentTexture=0;
    float minAngle = 360.0f;
    float angleThreshold = 10.0f;
    // Select the texture with the lowest angle
    for(int i =0; i <nTextures; i++){
        float alpha = texture2DArray(textures,vec3(gl_TexCoord[0].xy, i)).a;
        if(alpha != 1.0f) {
            //TODO check calculation of projDirection
            vec3 projDirection =(projectPos-glPos);
            float angle = dot(normalVec,projDirection*-1);
            if(angle<minAngle) {
                minAngle = angle;
            }
        }
    }
    
    float minDistance= -1.0f;
    // Select the texture with the lowest distance among those around the lowest angle
    for(i=0; i <nTextures; i++){
        float alpha = texture2DArray(textures,vec3(gl_TexCoord[0].xy, i)).a;
        // Condition 1: Must be visible
        if (alpha != 1.0f) {
            //TODO check calculation of projDirection
            vec3 projDirection =( projectPos-glPos);
            float angle = dot(normalVec,projDirection);
            // Condition 2: The angle must be between +-10.0f of the lowest angle
            if(angle<(minAngle+angleThreshold) || angle>(minAngle+angleThreshold)) {
                // TODO check calculation of distance between both points within RÂ³
                float dx = projectPos[i].x - glPos.x;
                float dy = projectPos[i].y - glPos.y;
                float dz = projectPos[i].z - glPos.z;
                float d = sqrt(dx * dx + dy * dy + dz * dz);
                // select the texture with the lowest distance
                if(d<minDistance || minDistance ==-1.0f) {
                    minDistance = d;
                    currentTexture = i;
                }
            }
        }
    }
    gl_FragColor = texture2DArray(textures,vec3(gl_TexCoord[0].xy, currentTexture));
} 

