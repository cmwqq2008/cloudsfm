import glob

try: 
    from PIL import Image
except ImportError, err: 
    import Image

w = 0
h = 0
images = []
for file in glob.glob("*.png"):
    im = Image.open(file) 
    images.append( im )
    w = im.size[0]
    h = im.size[1]

ortho = Image.new("RGBA", (w, h), None)
pixels = ortho.load()

for y in range(0, h):
    for x in range(0, w):
        offset = y*w + x
        xy = (x, y)
        besta = 0
        bestrgba = (0,0,0,0)
        for im in images:
            rgba = im.getpixel(xy)
            if ( rgba[ 3 ] > besta ):
                besta = rgba[ 3 ]
                bestrgba = rgba
        
        pixels[x,y] = (bestrgba[0], bestrgba[1], bestrgba[2], 255 )

ortho.save( 'orthophoto.jpg' )

