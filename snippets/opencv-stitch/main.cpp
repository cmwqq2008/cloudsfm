#include <stdio.h>    
#include <opencv2/opencv.hpp>    
#include <opencv2/stitching/stitcher.hpp>  
  
using namespace cv;    
using namespace std;  
      
int main( int argc, char *argv[] )
{  
     std::vector<Mat> vImg;  
     Mat rImg;  

     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000000.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000001.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000002.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000003.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000004.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000005.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000006.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000007.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000008.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000009.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000010.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000011.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000012.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000013.jpg") );  
     vImg.push_back( imread("/tmp/pmvs/bundler/visualize/00000014.jpg") ); 
      
     Stitcher stitcher = Stitcher::createDefault();  

     stitcher.setWarper(new TransverseMercatorWarper());
    //stitcher.setFeaturesFinder(new detail::SurfFeaturesFinder(1000,3,4,3,4));
     stitcher.setRegistrationResol(0.5);
     stitcher.setSeamEstimationResol(0.1);
     stitcher.setCompositingResol(-1);
     stitcher.setPanoConfidenceThresh(1);
     stitcher.setWaveCorrection(true);
     stitcher.setWaveCorrectKind(detail::WAVE_CORRECT_HORIZ);
     stitcher.setFeaturesMatcher(new detail::BestOf2NearestMatcher(false,0.3));
     stitcher.setBundleAdjuster(new detail::BundleAdjusterRay());

    // unsigned long AAtime=0, BBtime=0; //check processing time  
     //AAtime = getTickCount(); //check processing time  
      
     stitcher.stitch(vImg, rImg);  

    std::vector<detail::CameraParams> cameras = stitcher.cameras();
    for (std::vector<detail::CameraParams>::iterator it = cameras.begin() ; it != cameras.end(); ++it) {
        detail::CameraParams cam = *it;
        cout << cam.R << endl;
        cout << cam.t << endl;
        cout << cam.K() << endl;
    }

     //BBtime = getTickCount(); //check processing time   
     //printf("%.2lf sec \n",  (BBtime - AAtime)/getTickFrequency() ); //check processing time  

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY );
    compression_params.push_back(85);

     imwrite("/tmp/pmvs/mosaic.jpg", rImg, compression_params );    

    return 0;
}

