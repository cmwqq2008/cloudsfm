#define JPEG_HDFS_SRC_MGR_BUFFER_SIZE ( 1 << 10 )
 
typedef struct _jpeg_hdfs_src_mgr
{
    jpeg_source_mgr mgr;
    unsigned int bytes_read;
    hdfsFile file;
    hdfsFS   fs;
    unsigned char *buffer;
} jpeg_hdfs_src_mgr;


static void hdfs_init_source( j_decompress_ptr cinfo );
static boolean hdfs_fill_input_buffer( j_decompress_ptr cinfo );
static void hdfs_skip_input_data( j_decompress_ptr cinfo, long num_bytes );
static void hdfs_term_source( j_decompress_ptr cinfo );
static void jpeg_hdfs_src( j_decompress_ptr cinfo, jpeg_hdfs_src_mgr * src, const char *filename );

static void hdfs_init_source( j_decompress_ptr cinfo ) 
{
}

static boolean hdfs_fill_input_buffer( j_decompress_ptr cinfo )
{
    jpeg_hdfs_src_mgr* src = (jpeg_hdfs_src_mgr*)cinfo->src;
    int bytes_read = 0;

    src->buffer = (unsigned char *)malloc(sizeof(unsigned char) * JPEG_HDFS_SRC_MGR_BUFFER_SIZE );

    bytes_read = hdfsRead( src->fs, src->file, src->buffer, JPEG_HDFS_SRC_MGR_BUFFER_SIZE );
    if ( bytes_read <= 0 ) {
        fprintf( stderr, "Failure reading file\n" );
        free( src->buffer );
        return FALSE;
    }

    src->mgr.next_input_byte = src->buffer;
    src->mgr.bytes_in_buffer = (size_t)bytes_read;
    if ( 0 == src->mgr.bytes_in_buffer ) {
        /* The image file is truncated. We insert EOI marker to tell the library to stop processing. */
        src->buffer[ 0 ] = (JOCTET)0xFF;
        src->buffer[ 1 ] = (JOCTET)JPEG_EOI;
        src->mgr.bytes_in_buffer = 2;
    }
    return TRUE;
}

static void hdfs_skip_input_data( j_decompress_ptr cinfo, long num_bytes )
{
    jpeg_hdfs_src_mgr* src = (jpeg_hdfs_src_mgr*)cinfo->src;
 
    if ( 1 > num_bytes )
        return;
    if ( num_bytes <= src->mgr.bytes_in_buffer )
    {
        src->mgr.next_input_byte += (size_t)num_bytes;
        src->mgr.bytes_in_buffer -= (size_t)num_bytes;
    }
    else
    {
        src->mgr.bytes_in_buffer = 0;
        if ( hdfsSeek( src->fs, src->file, num_bytes - (long)src->mgr.bytes_in_buffer) == -1 ) {
            fprintf( stderr, "Could not seek in file\n" );
            free( src->buffer );
            exit( -1 );
        }
    }
}

static void hdfs_term_source( j_decompress_ptr cinfo ) 
{
    jpeg_hdfs_src_mgr* src = (jpeg_hdfs_src_mgr*)cinfo->src;
    if ( src->file != NULL )
    {
        hdfsCloseFile( src->fs, src->file );
        free( src->buffer );
        src->file = NULL;
    }
}

static void jpeg_hdfs_src( j_decompress_ptr cinfo, jpeg_hdfs_src_mgr * src, const char *filename )
{
    //--- get line of text ---
    src->file = hdfsOpenFile( src->fs, filename, O_RDONLY, 0, 0, 0);
    if( !src->file ) {
        fprintf(stderr, "Failed to open %s for reading!\n", filename);
        exit(-1);
    }

    src->bytes_read = 0;
    src->mgr.init_source = hdfs_init_source;
    src->mgr.fill_input_buffer = hdfs_fill_input_buffer;
    src->mgr.skip_input_data = hdfs_skip_input_data;
    src->mgr.resync_to_restart = jpeg_resync_to_restart;
    src->mgr.term_source = hdfs_term_source;
    src->mgr.bytes_in_buffer = 0;
    src->mgr.next_input_byte = (JOCTET*)src->buffer;
    cinfo->src = (jpeg_source_mgr*)src;
}


