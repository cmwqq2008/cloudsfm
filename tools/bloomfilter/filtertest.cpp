/*
 **************************************************************************
 *                                                                        *
 *                           Open Bloom Filter                            *
 *                                                                        *
 * Description: Basic Bloom Filter Usage                                  *
 * Author: Arash Partow - 2000                                            *
 * URL: http://www.partow.net                                             *
 * URL: http://www.partow.net/programming/hashfunctions/index.html        *
 *                                                                        *
 * Copyright notice:                                                      *
 * Free use of the Bloom Filter Library is permitted under the guidelines *
 * and in accordance with the most current version of the Common Public   *
 * License.                                                               *
 * http://www.opensource.org/licenses/cpl1.0.php                          *
 *                                                                        *
 **************************************************************************
*/



/*
   Description: This example demonstrates basic usage of the Bloom filter.
                Initially some values are inserted then they are subsequently
                queried, noting any false positives or errors.
*/


#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>

#include "bloom_filter.hpp"
#include "hash_search.h"

void generateKey( std::string& closestring, std::string& realstring );


int rksearch(char* pattern, char* text);
int hash(char* str);
int hashn(char* str, int n);
int hash_increment(char* str, int prevIndex, int prevHash, int keyLength);


int main()
{

   bloom_parameters parameters;

   // How many elements roughly do we expect to insert?
   parameters.projected_element_count = 160000;

   // Maximum tolerable false positive probability? (0,1)
   parameters.false_positive_probability = 0.001; // 1 in 100

   // Simple randomizer (optional)
   parameters.random_seed = 0xA5A5A5A5;

   if (!parameters)
   {
      std::cout << "Error - Invalid set of bloom filter parameters!" << std::endl;
      return 1;
   }

   parameters.compute_optimal_parameters();

    std::cout << parameters.optimal_parameters.number_of_hashes << "\n";
    std::cout << parameters.optimal_parameters.table_size << "\n";

    //Instantiate Bloom Filter
    bloom_filter filter(parameters);

    //Instantiate Bloom Filter
    bloom_filter filter2(parameters);

          // Insert some numbers
      for (std::size_t i = 0; i < 100; ++i)
      {
         filter.insert(i);
      }
      // Query the existence of invalid numbers
      for (int i = -1; i > -100; --i)
      {
         if (filter.contains(i))
         {
            std::cout << "BF falsely contains: " << i << std::endl;
         }
      }
      for (int i = 100; i < 200; i++)
      {
         if (filter.contains(i))
         {
            std::cout << "BF falsely contains: " << i << std::endl;
         }
      }

    std::string one;
    std::string two;
    generateKey( one, two );

    std::cout << one << "\n";
    std::cout << two << "\n";

    filter.insert( one );
    if ( ! filter.contains( two ) ) {
        std::cout << "it's not in there!\n";
    }
    if ( ! filter.contains( one ) ) {
        std::cout << "Dumped it in and can't find it again\n";
    }

    unsigned modulus = hash_search::prime_generator( 2147483647 );

    std::vector<int> hashes;
    for ( int i = 0; i < 8192; i++ ) {
        std::string one;
        std::string two;
        generateKey( one, two );    

        int hone = hash_search::hasher( one, modulus );
        int htwo = hash_search::hasher( two, modulus );

        hashes.push_back( htwo );

        filter.insert( hone );
    }

    int numMatches = 0;
    for(unsigned int i = 0; i < hashes.size(); i++) {
        if ( filter.contains( hashes[ i ] ) ) {
            numMatches++;
        }
    }

    std::cout << "Matched " << numMatches << " through the filter out of 8192\n";
    
    numMatches = 0;
    for ( int i = 0; i < 8192; i++ ) {
        std::string one;
        std::string two;
        generateKey( one, two );

        int hone = hash_search::hasher( one, modulus );
        int htwo = hash_search::hasher( two, modulus );
    
        if ( filter.contains( hone )) {
            numMatches++;
        }
        if ( filter.contains( htwo )) {
            numMatches++;
        }
    }

    std::cout << "Matched " << numMatches << " through a second random generator out of 8192\n";

/*   std::string str_list[] = { "AbC", "iJk", "XYZ" };

   // Insert into Bloom Filter
   {
      // Insert some strings
      for (std::size_t i = 0; i < (sizeof(str_list) / sizeof(std::string)); ++i)
      {
         filter.insert(str_list[i]);
      }

      // Insert some numbers
      for (std::size_t i = 0; i < 100; ++i)
      {
         filter.insert(i);
      }
   }

   // Query Bloom Filter
   {
      // Query the existence of strings
      for (std::size_t i = 0; i < (sizeof(str_list) / sizeof(std::string)); ++i)
      {
         if (filter.contains(str_list[i]))
         {
            std::cout << "BF contains: " << str_list[i] << std::endl;
         }
      }

      // Query the existence of numbers
      for (std::size_t i = 0; i < 100; ++i)
      {
         if (filter.contains(i))
         {
            std::cout << "BF contains: " << i << std::endl;
         }
      }

      std::string invalid_str_list[] = { "AbCX", "iJkX", "XYZX" };

      // Query the existence of invalid strings
      for (std::size_t i = 0; i < (sizeof(invalid_str_list) / sizeof(std::string)); ++i)
      {
         if (filter.contains(invalid_str_list[i]))
         {
            std::cout << "BF falsely contains: " << invalid_str_list[i] << std::endl;
         }
      }

      // Query the existence of invalid numbers
      for (int i = -1; i > -100; --i)
      {
         if (filter.contains(i))
         {
            std::cout << "BF falsely contains: " << i << std::endl;
         }
      }
   }*/

   return 0;
}

#define NUM_FEATURES 128

void generateKey( std::string& closestring, std::string& realstring )
{
    unsigned char features[ NUM_FEATURES ];
    int i = 0;

    for ( i = 0; i < NUM_FEATURES; i++ ) {
        features[ i ] = rand()%255;
    }

    // 40000 / 128 == 312.5
    // == a difference of 16 on either side on average.
    // Exploiting that knowledge, let's define characters that define
    // buckets that describe these values.
    // The buckets have a bandwidth of 3*16 == 48 and those
    // buckets correctly describe 

    // Assuming a value 17, then 17-16 == 1 and 17+16 == 33
    // Assuming a value 17, then 17-16 == 1 and 17+16 == 33

 //   0..32 = 1
 //   32..64 = 2
 //   64..96 = 3
 //   96..128 = 4

    // Now let's bucketize these values    
    for ( i = 0; i < NUM_FEATURES; i++ ) {
        features[i] /= 16;
    }
    // Now it has values 0-16 in the array only, or 4 bits.
    long intermediate = 0;

    // .... .... | .... .... | .... .... | .... .... | .... .... | .... .... | .... .... | .... ....
    //   0   4      8    12     16    20   24   28      32   36    40    44     48   52     56   60
    // stuff it into a long of 8 bytes
    for ( i = 0; i < NUM_FEATURES; i+=16 ) {
        intermediate = 0L;
        intermediate |= ((long)features[ i ]) << 60;
        intermediate |= ((long)features[ i+1 ]) << 56;
        intermediate |= ((long)features[ i+2 ]) << 52;
        intermediate |= ((long)features[ i+3 ]) << 48;
        intermediate |= ((long)features[ i+4 ]) << 44;
        intermediate |= ((long)features[ i+5 ]) << 40;
        intermediate |= ((long)features[ i+6 ]) << 36;
        intermediate |= ((long)features[ i+7 ]) << 32;
        intermediate |= ((long)features[ i+8 ]) << 28;
        intermediate |= ((long)features[ i+9 ]) << 24;
        intermediate |= ((long)features[ i+10 ]) << 20;
        intermediate |= ((long)features[ i+11 ]) << 16;
        intermediate |= ((long)features[ i+12 ]) << 12;
        intermediate |= ((long)features[ i+13 ]) << 8;
        intermediate |= ((long)features[ i+14 ]) << 4;
        intermediate |= ((long)features[ i+15 ]);

        realstring.append( reinterpret_cast<const char *>(&intermediate), 8 );
    }

    // Now let's randomize these values    
    for ( i = 0; i < NUM_FEATURES; i++ ) {
        if ( rand() % 20 ) {
            features[i] += rand()%1;
        } else {
            features[i] -= rand()%7;
        }
    }

    // .... .... | .... .... | .... .... | .... .... | .... .... | .... .... | .... .... | .... ....
    //   0   4      8    12     16    20   24   28      32   36    40    44     48   52     56   60
    // stuff it into a long of 8 bytes
    for ( i = 0; i < NUM_FEATURES; i+=16 ) {
        intermediate = 0L;
        intermediate |= ((long)features[ i ]) << 60;
        intermediate |= ((long)features[ i+1 ]) << 56;
        intermediate |= ((long)features[ i+2 ]) << 52;
        intermediate |= ((long)features[ i+3 ]) << 48;
        intermediate |= ((long)features[ i+4 ]) << 44;
        intermediate |= ((long)features[ i+5 ]) << 40;
        intermediate |= ((long)features[ i+6 ]) << 36;
        intermediate |= ((long)features[ i+7 ]) << 32;
        intermediate |= ((long)features[ i+8 ]) << 28;
        intermediate |= ((long)features[ i+9 ]) << 24;
        intermediate |= ((long)features[ i+10 ]) << 20;
        intermediate |= ((long)features[ i+11 ]) << 16;
        intermediate |= ((long)features[ i+12 ]) << 12;
        intermediate |= ((long)features[ i+13 ]) << 8;
        intermediate |= ((long)features[ i+14 ]) << 4;
        intermediate |= ((long)features[ i+15 ]);

        closestring.append( reinterpret_cast<const char *>(&intermediate), 8 );
    }
}

/*
#define MODULUS 
int rksearch(char* pattern, char* text)
{
    int pattern_hash, text_hash, pattern_len, num_iterations, i; 
    
    // are the pattern and the text legitimate strings? 
    if (pattern == NULL || text == NULL) return -1;

    // get the lengths of the strings and the number of iterations 
    pattern_len = strlen(pattern);
    num_iterations = strlen(text) - pattern_len + 1;

    // Do initial hashes 
    pattern_hash = hash(pattern);
    text_hash = hashn(text, pattern_len);

    // Main comparison loop 
    for (i = 0; i < num_iterations; i++) {
        if (pattern_hash == text_hash && 
            !strncmp(pattern, &(text[i]), pattern_len)) return i;

        text_hash = hash_increment(text, i, text_hash, pattern_len);
    } 
    
    // Pattern not found so return -1 
    return -1;
}

// hash function for fingerprinting 
int hash(char* str)
{
    int sum = 0; 
    while (*str != '\0') 
        sum += (int) *str; 
    return sum % MODULUS;
}

int hashn(char* str, int n)
{
    char ch = str[n];
    int sum; 
    str[n] = '\0';
    sum = hash(str);
    str[n] = ch;
    return sum;
}

int hash_increment(char* str, int prevIndex, int prevHash, int keyLength)
{
    int val = (prevHash - ((int) str[prevIndex]) 
        + ((int) str[prevIndex + keyLength])) % MODULUS;

    return (val < 0) ? (val + MODULUS) : val;
}
*/

