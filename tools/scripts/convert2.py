#!/usr/bin/python

#    <f> <k1> <k2>   [the focal length, followed by two radial distortion coeffs]
#    <R>             [a 3x3 matrix representing the camera rotation]
#    <t>             [a 3-vector describing the camera translation]

# Bundle file v0.3
#22 987
#3123.09667969 0 0
#0.986993267302 0.0721865572511 -0.143643440377
#0.118587708498 -0.930198811166 0.347371290489
#-0.1085416384 -0.359887643331 -0.926661950543
#0.0239035872314 0.128250215289 0.151442383711

import argparse
import sys
import cv
import os
from decimal import *
import math
import numpy as np

def undistort( photoname, cx, cy, fx, fy, k1, k2, k3, p1, p2 ):
    newphotoname = "../photos/%s"%(photoname)

    img = cv.LoadImage(newphotoname)

    try:
        os.stat("visualize")
    except:
        os.mkdir("visualize")

    filename = os.path.basename( photoname )
    outfile = 'visualize/%s'%(filename)

    width, height = cv.GetSize(img)

    # Camera Daten fuer Canon IXUS 230 HS
    #camera_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
    #cv.SetReal2D(camera_matrix, 0, 0, 3311.065782)
    #cv.SetReal2D(camera_matrix, 0, 1, 0.0)
    #cv.SetReal2D(camera_matrix, 0, 2, 1938.994120)
    #cv.SetReal2D(camera_matrix, 1, 0, 0.0)
    #cv.SetReal2D(camera_matrix, 1, 1, 3310.8017082)
    #cv.SetReal2D(camera_matrix, 1, 2, 1498.703570)
    #cv.SetReal2D(camera_matrix, 2, 0, 0.0)
    #cv.SetReal2D(camera_matrix, 2, 1, 0.0)
    #cv.SetReal2D(camera_matrix, 2, 2, 1.0)

    #dist_coeffs = cv.CreateMat(1, 5, cv.CV_32FC1)
    #cv.SetReal2D(dist_coeffs, 0, 0, -0.0717928444 ) #k1
    #cv.SetReal2D(dist_coeffs, 0, 1, 0.06356405    ) #k2
    #cv.SetReal2D(dist_coeffs, 0, 2, -0.0033542759 )    #p1
    #cv.SetReal2D(dist_coeffs, 0, 3, -0.0059580178 )    #p2
    #cv.SetReal2D(dist_coeffs, 0, 4, -0.033985134  ) #k3

    # Camera Daten fuer Canon IXUS 230 HS
    camera_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
    cv.SetReal2D(camera_matrix, 0, 0, fx) #fx
    cv.SetReal2D(camera_matrix, 0, 1, 0.0)
    cv.SetReal2D(camera_matrix, 0, 2, cx) #cx
    cv.SetReal2D(camera_matrix, 1, 0, 0.0)
    cv.SetReal2D(camera_matrix, 1, 1, fy) #fy
    cv.SetReal2D(camera_matrix, 1, 2, cy)  #cy
    cv.SetReal2D(camera_matrix, 2, 0, 0.0)
    cv.SetReal2D(camera_matrix, 2, 1, 0.0)
    cv.SetReal2D(camera_matrix, 2, 2, 1.0)

    dist_coeffs = cv.CreateMat(1, 5, cv.CV_32FC1)
    cv.SetReal2D(dist_coeffs, 0, 0, k1 ) #k1
    cv.SetReal2D(dist_coeffs, 0, 1, k2    ) #k2
    cv.SetReal2D(dist_coeffs, 0, 2, p1 )    #p1
    cv.SetReal2D(dist_coeffs, 0, 3, p2 )    #p2
    cv.SetReal2D(dist_coeffs, 0, 4, k3  ) #k3

    map1 = cv.CreateImage((width, height), cv.IPL_DEPTH_32F, 1)
    map2 = cv.CreateImage((width, height), cv.IPL_DEPTH_32F, 1)
    cv.InitUndistortMap(camera_matrix, dist_coeffs, map1, map2)

    undistimage = cv.CloneImage(img)
    cv.Remap(img, undistimage, map1, map2)

    cv.SaveImage( outfile, undistimage )
    print "saving to %s\n"%(outfile)

    return outfile

if __name__ == "__main__":

    getcontext().prec = 20

    parser = argparse.ArgumentParser(description='Unit tests')
    parser.add_argument('geocal', type=str, help='metriccal.txt file to process')
    parser.add_argument('x', type=float, help='offset x')
    parser.add_argument('y', type=float, help='offset y')
    parser.add_argument('z', type=float, help='offset z')

    args = parser.parse_args()

    geocal = open(args.geocal, 'r')
    if ( geocal == None ):
        sys.exit(-1)

    outfile = open('bundler.out', 'w')
    if (outfile == None ):
        sys.exit(-1)
    listfile = open('list.txt', 'w')
    if (outfile == None ):
        sys.exit(-1)

    state = 0
    numcams = 0
    images = []
    w, h = 0, 0
    f, cx, cy, r1, r2, r3, p1, p2 = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0

    for line in geocal:
        if ( state == 0 ):
            state = 1
            continue
        if ( state == 1 ):
            state = 2
            # 4000 3000 3353.6903332581618997 1971.6304532068443223 1537.9993684516241501 -0.08662337425445970629 0.09235489298178750639 -0.069690974880429926164 -0.0030774355703823670985 -0.0051118251444754502291
            (w, h, f, cx, cy, r1, r2, r3, p1, p2) = [t(s) for t,s in zip((int, int, float, float, float, float, float, float, float, float),line.split())]
            continue
        if ( state == 2 ):
            state = 3
            continue

        # IMG_1387_geotag.JPG 282875.14993548230268 9111511.7142964545637 137.13659486960932554 4.2879687790818348248 9.3912893630985134763 -160.89448696858616472
        (imgname, x, y, z, omega, phi, kappa) = [t(s) for t,s in zip((str, float, float, float, float, float, float),line.split())]
        images.append( ( imgname, x, y, z, omega, phi, kappa ) )
        numcams = numcams + 1

    outfile.write( "# Bundle file v0.3\n" )
    outfile.write( "%s 0\n"%( numcams ) )

    for i in xrange(len(images)):
        (imgname, x, y, z, omega, phi, kappa) = images[i]

        mult = math.pi/180.0
        omega = omega * mult
        phi = phi * mult
        kappa = kappa * mult

        R11 = math.cos(phi)*math.cos(kappa)
        R12 = math.sin(omega)*math.sin(phi)*math.cos(kappa)+math.cos(omega)*math.sin(kappa)
        R13 = math.sin(omega)*math.sin(kappa)-math.cos(omega)*math.sin(phi)*math.cos(kappa)
        R21 = -math.cos(phi)*math.sin(kappa)
        R22 = math.cos(kappa)*math.cos(omega)-math.sin(omega)*math.sin(phi)*math.sin(kappa)
        R23 = math.sin(omega)*math.cos(kappa)+math.cos(omega)*math.sin(phi)*math.sin(kappa)
        R31 = math.sin(phi)
        R32 = -math.cos(phi)*math.sin(omega)
        R33 = math.cos(omega)*math.cos(phi)

        #T: 0.0239035872314 -0.128250215289 -0.151442383711
        #C: -0.0223638502941 0.172074886963 0.0992186709115
        #R: 0.986993267302 0.0721865572511 -0.143643440377
        #-0.118587708498 0.930198811166 -0.347371290489
        #0.1085416384 0.359887643331 0.926661950543
        #R = np.array( [(0.986993267302,0.0721865572511,-0.143643440377),(-0.118587708498,0.930198811166,-0.347371290489),(0.1085416384,0.359887643331,0.926661950543)], dtype=float )
        #C = np.array( [(-0.0223638502941, 0.172074886963, 0.0992186709115)], dtype=float ).reshape(3)
        #T = np.dot( -R, C )

        R = np.array( [(R11,R12,R13),(R21,R22,R23),(R31,R32,R33)], dtype=float )
        C = np.array( [x+args.x, y+args.y, z+args.z], dtype=float )
        T = np.dot( -R, C )

        print T

        outfile.write( "%.16f 0 0\n"%( f ) )
        outfile.write( "%.20f %.20f %.20f\n"%( R11, R12, R13 ))
        outfile.write( "%.20f %.20f %.20f\n"%( R21, R22, R23 ))
        outfile.write( "%.20f %.20f %.20f\n"%( R31, R32, R33 ))

        outfile.write( "%.18f %.18f %.18f\n"%( T[0], T[1], T[2] ) )

        # cx, cy, fx, fy, k1, k2, k3, p1, p2
        undistortedfile = undistort( imgname, cx, cy, f, f, r1, r2, r3, p1, p2 )

        listfile.write( "%s\n"%( undistortedfile ))

    geocal.close()
    outfile.close()

