#!/usr/bin/python

#    <f> <k1> <k2>   [the focal length, followed by two radial distortion coeffs]
#    <R>             [a 3x3 matrix representing the camera rotation]
#    <t>             [a 3-vector describing the camera translation]

# Bundle file v0.3
#22 987
#3123.09667969 0 0
#0.986993267302 0.0721865572511 -0.143643440377
#0.118587708498 -0.930198811166 0.347371290489
#-0.1085416384 -0.359887643331 -0.926661950543
#0.0239035872314 0.128250215289 0.151442383711

import argparse
import sys
import cv
import os
from decimal import *
import math

def undistort( photoname, cx, cy, fx, fy, k1, k2, k3, p1, p2 ):
    newphotoname = "../photos/%s"%(photoname)

    img = cv.LoadImage(newphotoname)

    try:
        os.stat("visualize")
    except:
        os.mkdir("visualize")

    filename = os.path.basename( photoname )
    outfile = 'visualize/%s'%(filename)

    width, height = cv.GetSize(img)

    # Camera Daten fuer Canon IXUS 230 HS
    #camera_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
    #cv.SetReal2D(camera_matrix, 0, 0, 3311.065782)
    #cv.SetReal2D(camera_matrix, 0, 1, 0.0)
    #cv.SetReal2D(camera_matrix, 0, 2, 1938.994120)
    #cv.SetReal2D(camera_matrix, 1, 0, 0.0)
    #cv.SetReal2D(camera_matrix, 1, 1, 3310.8017082)
    #cv.SetReal2D(camera_matrix, 1, 2, 1498.703570)
    #cv.SetReal2D(camera_matrix, 2, 0, 0.0)
    #cv.SetReal2D(camera_matrix, 2, 1, 0.0)
    #cv.SetReal2D(camera_matrix, 2, 2, 1.0)

    #dist_coeffs = cv.CreateMat(1, 5, cv.CV_32FC1)
    #cv.SetReal2D(dist_coeffs, 0, 0, -0.0717928444 ) #k1
    #cv.SetReal2D(dist_coeffs, 0, 1, 0.06356405    ) #k2
    #cv.SetReal2D(dist_coeffs, 0, 2, -0.0033542759 )    #p1
    #cv.SetReal2D(dist_coeffs, 0, 3, -0.0059580178 )    #p2
    #cv.SetReal2D(dist_coeffs, 0, 4, -0.033985134  ) #k3

    # Camera Daten fuer Canon IXUS 230 HS
    camera_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
    cv.SetReal2D(camera_matrix, 0, 0, fx) #fx
    cv.SetReal2D(camera_matrix, 0, 1, 0.0)
    cv.SetReal2D(camera_matrix, 0, 2, cx) #cx
    cv.SetReal2D(camera_matrix, 1, 0, 0.0)
    cv.SetReal2D(camera_matrix, 1, 1, fy) #fy
    cv.SetReal2D(camera_matrix, 1, 2, cy)  #cy
    cv.SetReal2D(camera_matrix, 2, 0, 0.0)
    cv.SetReal2D(camera_matrix, 2, 1, 0.0)
    cv.SetReal2D(camera_matrix, 2, 2, 1.0)

    dist_coeffs = cv.CreateMat(1, 5, cv.CV_32FC1)
    cv.SetReal2D(dist_coeffs, 0, 0, k1 ) #k1
    cv.SetReal2D(dist_coeffs, 0, 1, k2    ) #k2
    cv.SetReal2D(dist_coeffs, 0, 2, p1 )    #p1
    cv.SetReal2D(dist_coeffs, 0, 3, p2 )    #p2
    cv.SetReal2D(dist_coeffs, 0, 4, k3  ) #k3

    map1 = cv.CreateImage((width, height), cv.IPL_DEPTH_32F, 1)
    map2 = cv.CreateImage((width, height), cv.IPL_DEPTH_32F, 1)
    cv.InitUndistortMap(camera_matrix, dist_coeffs, map1, map2)

    undistimage = cv.CloneImage(img)
    cv.Remap(img, undistimage, map1, map2)

    cv.SaveImage( outfile, undistimage )
    print "saving to %s\n"%(outfile)

    return outfile

if __name__ == "__main__":

    getcontext().prec = 20

    parser = argparse.ArgumentParser(description='Unit tests')
    parser.add_argument('metriccal', type=str, help='metriccal.txt file to process')
    parser.add_argument('geocal', type=str, help='metriccal.txt file to process')

    args = parser.parse_args()

    metriccal = open(args.metriccal, 'r')
    if ( metriccal == None ):
        sys.exit(-1)
    geocal = open(args.geocal, 'r')
    if ( geocal == None ):
        sys.exit(-1)

    outfile = open('bundler.out', 'w')
    if (outfile == None ):
        sys.exit(-1)
    listfile = open('list.txt', 'w')
    if (outfile == None ):
        sys.exit(-1)


    state = 0
    numcams = 0
    images = []
    focals1 = []
    focals2 = []
    rdistortion = []
    tdistortion = []
    pos = []
    R1 = []
    R2 = []
    R3 = []
    for line in infile:
        if ( state == 0 ):
            if not "camModelList.size()" in line:
                continue
            else:
                (a, b) = [t(s) for t,s in zip((str,int),line.split())]
                numcams = b
                print "numcams: %d\n"%(numcams)
                state = 1
            continue
        if ( state == 1 ):
            state = 2
            (a, b, c) = [t(s) for t,s in zip((str,int, int),line.split())]
            images.append( (a, b, c ) )
            continue
        if ( state == 2 ):
            state = 3
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            focals1.append( (a,b,c) )
            continue
        if ( state == 3 ):
            state = 4
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            focals2.append( (a,b,c) )
            continue
        if ( state < 5 ):
            state = state + 1
            continue
        if ( state == 5 ):
            state = state + 1
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            rdistortion.append( (a, b, c) )
            continue
        if ( state == 6 ):
            state = state + 1
            (a, b) = [t(s) for t,s in zip((float, float, float),line.split())]
            tdistortion.append( (a, b) )
            continue
        if ( state < 7 ):
            state = state + 1
            continue
        if ( state == 7 ):
            state = state + 1
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            pos.append( ( a, b, c ) )
            continue
        if ( state == 8 ):
            state = state + 1
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            R1.append(( a, b, c ))
            continue
        if ( state == 9 ):
            state = state + 1
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            R2.append(( a, b, c ))
            continue
        if ( state == 10 ):
            state = 1
            (a, b, c) = [t(s) for t,s in zip((float, float, float),line.split())]
            R3.append(( a, b, c ))
            continue

    outfile.write( "# Bundle file v0.3\n" )
    outfile.write( "%s 0\n"%( numcams ) )

    # IMG_1387_geotag.JPG 282875.14993548230268 9111511.7142964545637 137.13659486960932554 4.2879687790818348248 9.3912893630985134763 -160.89448696858616472
    for line in geocal:
        


    for i in xrange(len(images)):
        outfile.write( "%.16f 0 0\n"%( focals1[i][0] ) )
        outfile.write( "%.20f %.20f %.20f\n"%( R1[i][0], R1[i][1], R1[i][2] ) )
        outfile.write( "%.20f %.20f %.20f\n"%( R2[i][0], R2[i][1], R2[i][2] ) )
        outfile.write( "%.20f %.20f %.20f\n"%( R3[i][0], R3[i][1], R3[i][2] ) )
        outfile.write( "%.18f %.18f %.18f\n"%( pos[i][0], pos[i][1], pos[i][2] ) )

        # cx, cy, fx, fy, k1, k2, k3, p1, p2
        undistortedfile = undistort( images[i][0], focals1[i][2], focals2[i][2],
            focals1[i][0], focals2[i][1], rdistortion[i][0], rdistortion[i][1], rdistortion[i][2],
            tdistortion[i][0], tdistortion[i][1] )

        listfile.write( "%s\n"%( undistortedfile ))

        mult = 180/math.pi
        print "%s: %f/%f/%f\n"%(images[i][0], omega*mult, phi*mult, mykappa*mult)

    infile.close()
    outfile.close()

