#ifndef STRING_SEARCH_H
#define STRING_SEARCH_H
 
#include <string>
 
namespace hash_search {
 
    const size_t BASE = 257; // small ASCII
    /**
    @param s String for which to return a hash value
    @param modulus number (preferably prime) to use for hashing
    @return Hash value
    */
    int hasher(const std::string &s, const unsigned &modulus);
 
    unsigned prime_generator(unsigned min); // will generate the next prime at least as big as min
}
 
#endif

