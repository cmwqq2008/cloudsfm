#include "hash_search.h"
#include <string>
#include <cmath>
#include <cassert>


int hash_search::hasher(const std::string &s, const unsigned &modulus) {
    int result = 0;
    size_t len = s.size();
 
    for (size_t i = 0; i < len; ++i) {
        result = (result * hash_search::BASE + s.at(i)) % modulus;
    }

    return result;
}
 
unsigned hash_search::prime_generator(unsigned min) {
    assert(min > 1);
    unsigned max_factor, i;
 
    while (true) {
        max_factor = std::sqrt((double)min);
        for (i = 2; i <= max_factor; ++i) {
            if (min % i == 0) break;
        }
        if (i > max_factor) return min;  // result is prime
        ++min;
    }
}
