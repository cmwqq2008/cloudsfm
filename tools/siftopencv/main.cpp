/** @internal
** @file     sift.c
** @author   Andrea Vedaldi
** @brief    Scale Invariant Feature Transform (SIFT) - MEX
**/

/*
Copyright (C) 2007-12 Andrea Vedaldi and Brian Fulkerson.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#include <string>
#include <flann/flann.hpp>

#include <vl/mathop.h>
#include <vl/sift.h>
//#include <math.h>
//#include <assert.h>

#include <opencv.hpp>
//#include <highgui.h>

// #include "epnp.h"

#include <vl/generic.h>
#include "bloom_filter.hpp"
#include "hash_search.h"

#include <stdlib.h>
#include <string>
#include <vector>

#define ALLOC_FRAMES    4 * 12000 * 4
#define ALLOC_DESCS     128 * 12000 * 4
#define HASH_SPACE      16

typedef struct
{
	int k1 ;
	int k2 ;
	double score ;
} Pair ;

void desaturateImage( IplImage *img );

VL_INLINE void
	transpose_descriptor (vl_sift_pix* dst, vl_sift_pix* src)
{
	int const BO = 8 ;  /* number of orientation bins */
	int const BP = 4 ;  /* number of spatial bins     */
	int i, j, t ;

	for (j = 0 ; j < BP ; ++j) {
		int jp = BP - 1 - j ;
		for (i = 0 ; i < BP ; ++i) {
			int o  = BO * i + BP*BO * j  ;
			int op = BO * i + BP*BO * jp ;
			dst [op] = src[o] ;
			for (t = 1 ; t < BO ; ++t)
				dst [BO - t + op] = src [t + o] ;
		}
	}
}


static int
	korder (void const* a, void const* b) {
		double x = ((double*) a) [2] - ((double*) b) [2] ;
		if (x < 0) return -1 ;
		if (x > 0) return +1 ;
		return 0 ;
}


vl_bool
	check_sorted (double const * keys, vl_size nkeys)
{
	vl_uindex k ;
	for (k = 0 ; k + 1 < nkeys ; ++ k) {
		if (korder(keys, keys + 4) > 0) {
			return VL_FALSE ;
		}
		keys += 4 ;
	}
	return VL_TRUE ;
}

int calculate_simhash( std::vector<int> hashes );


unsigned int generateHash(const char * string, size_t len) {

/*    unsigned int hash = 0;
    for(size_t i = 0; i < len; ++i) 
        hash = 65599 * hash + string[i];
    return hash ^ (hash >> 16);
*/

    unsigned int b    = 378551;
    unsigned int a    = 63689;
    unsigned int hash = 0;

    for(std::size_t i = 0; i < len; i++)
    {
        hash = hash * a + string[i];
        a    = a * b;
    }

    return (hash & 0x7FFFFFFF);

}

void VLSIFT(IplImage* image, vl_uint8* DATAdescr, double* DATAframes, int* nframes){
	//Take IplImage -> convert to SINGLE (float):
	float* frame = (float*)malloc(image->height*image->width*sizeof(float));
	uchar* Ldata      = (uchar *)image->imageData;
	for(int i=0;i<image->height;i++)
		for(int j=0;j<image->width;j++) {
			frame[j*image->height+i*image->nChannels] = (float)Ldata[i*image->widthStep+j*image->nChannels];
        }

	// VL SIFT computation:
	vl_sift_pix const *data ;
	int                M, N ;
	data = (vl_sift_pix*)frame;
	M = image->height;
	N = image->width;

	int                verbose = 1;
	int                O     =   4; //Octaves
	int                S     =   5; //Levels
	int                o_min =   1;

	double             edge_thresh = 4;
	double             peak_thresh = 1;
	double             norm_thresh = 0;
	double             magnif      = -1;
	double             window_size = -1;

	//mxArray           *ikeys_array = 0 ; //?
	double            *ikeys = 0 ; //?
	int                nikeys = -1 ; //?
	vl_bool            force_orientations = 0 ;
	vl_bool            floatDescriptors = 0 ;

	/* -----------------------------------------------------------------
	*                                                            Do job
	* -------------------------------------------------------------- */
	{
		VlSiftFilt        *filt ;
		vl_bool            first ;
		double            *frames = 0 ;
		vl_uint8              *descr  = 0 ;
		int                reserved = 0, i,j,q ;

		/* create a filter to process the image */
		filt = vl_sift_new (M, N, O, S, o_min) ;

		if (peak_thresh >= 0) vl_sift_set_peak_thresh (filt, peak_thresh) ;
		if (edge_thresh >= 0) vl_sift_set_edge_thresh (filt, edge_thresh) ;
		if (norm_thresh >= 0) vl_sift_set_norm_thresh (filt, norm_thresh) ;
		if (magnif      >= 0) vl_sift_set_magnif      (filt, magnif) ;
		if (window_size >= 0) vl_sift_set_window_size (filt, window_size) ;

		if (verbose) {
			printf("vl_sift: filter settings:\n") ;
			printf("vl_sift:   octaves      (O)      = %d\n",
				vl_sift_get_noctaves      (filt)) ;
			printf("vl_sift:   levels       (S)      = %d\n",
				vl_sift_get_nlevels       (filt)) ;
			printf("vl_sift:   first octave (o_min)  = %d\n",
				vl_sift_get_octave_first  (filt)) ;
			printf("vl_sift:   edge thresh           = %g\n",
				vl_sift_get_edge_thresh   (filt)) ;
			printf("vl_sift:   peak thresh           = %g\n",
				vl_sift_get_peak_thresh   (filt)) ;
			printf("vl_sift:   norm thresh           = %g\n",
				vl_sift_get_norm_thresh   (filt)) ;
			printf("vl_sift:   window size           = %g\n",
				vl_sift_get_window_size   (filt)) ;
			printf("vl_sift:   float descriptor      = %d\n",
				floatDescriptors) ;

			printf((nikeys >= 0) ?
				"vl_sift: will source frames? yes (%d read)\n" :
			"vl_sift: will source frames? no\n", nikeys) ;
			printf("vl_sift: will force orientations? %s\n",
				force_orientations ? "yes" : "no") ;
		}

		/* ...............................................................
		*                                             Process each octave
		* ............................................................ */
		i     = 0 ;
		first = 1 ;
		while (1) {
			int                   err ;
			VlSiftKeypoint const *keys  = 0 ;
			int                   nkeys = 0 ;

			/* Calculate the GSS for the next octave .................... */
              if (first) {
                first = 0 ;
                err = vl_sift_process_first_octave (filt, data) ;
              } else {
                err = vl_sift_process_next_octave  (filt) ;
              }

			if (err) break ;

			if (verbose > 1) {
				printf("vl_sift: GSS octave %d computed\n",
					vl_sift_get_octave_index (filt));
			}

			/* Run detector ............................................. */
			if (nikeys < 0) {
				vl_sift_detect (filt) ;

				keys  = vl_sift_get_keypoints  (filt) ;
				nkeys = vl_sift_get_nkeypoints (filt) ;
				i     = 0 ;

				if (verbose > 1) {
					printf ("vl_sift: detected %d (unoriented) keypoints\n", nkeys) ;
				}
			} else {
				nkeys = nikeys ;
			}

			/* For each keypoint ........................................ */
			for (; i < nkeys ; ++i) {
				double                angles [4] ;
				int                   nangles ;
				VlSiftKeypoint        ik ;
				VlSiftKeypoint const *k ;

				/* Obtain keypoint orientations ........................... */
				if (nikeys >= 0) {
					vl_sift_keypoint_init (filt, &ik,
						ikeys [4 * i + 1] - 1,
						ikeys [4 * i + 0] - 1,
						ikeys [4 * i + 2]) ;

					if (ik.o != vl_sift_get_octave_index (filt)) {
						break ;
					}

					k = &ik ;

					/* optionally compute orientations too */
					if (force_orientations) {
						nangles = vl_sift_calc_keypoint_orientations
							(filt, angles, k) ;
					} else {
						angles [0] = VL_PI / 2 - ikeys [4 * i + 3] ;
						nangles    = 1 ;
					}
				} else {
					k = keys + i ;
					nangles = vl_sift_calc_keypoint_orientations
						(filt, angles, k) ;
				}

				/* For each orientation ................................... */
				for (q = 0 ; q < nangles ; ++q) {
					vl_sift_pix  buf [128] ;
					vl_sift_pix rbuf [128] ;

					/* compute descriptor (if necessary) */
					vl_sift_calc_keypoint_descriptor (filt, buf, k, angles [q]) ;
					transpose_descriptor (rbuf, buf) ;

					/* make enough room for all these keypoints and more */
					if (reserved < (*nframes) + 1) {
						reserved += 2 * nkeys ;
						frames = (double*)realloc (frames, 4 * sizeof(double) * reserved) ;
						descr  = (vl_uint8*)realloc (descr,  128 * sizeof(vl_uint8) * reserved) ;
					}

					/* Save back with MATLAB conventions. Notice tha the input
					* image was the transpose of the actual image. */
					frames [4 * (*nframes) + 0] = k -> y ;
					frames [4 * (*nframes) + 1] = k -> x ;
					frames [4 * (*nframes) + 2] = k -> sigma ;
					frames [4 * (*nframes) + 3] = VL_PI / 2 - angles [q] ;

                    //unsigned char sbuf[128];
                    //std::string key;

                    std::vector<int> hashes;
					for (j = 0 ; j < 128 ; ++j) {
						float x = 512.0F * rbuf [j] ;
						x = (x < 255.0F) ? x : 255.0F ;
						descr[128 * (*nframes) + j] = (vl_uint8)x ;
                      //  sbuf[j] = (vl_uint8)((128-(vl_uint8)x));
                      //  key.append( 1, sbuf[j] );
                    }
                   // int hone = hash_search::hasher( key, modulus );
                    //filter.insert( hone );

					++ (*nframes) ;
				} /* next orientation */
			} /* next keypoint */
		} /* next octave */

		if (verbose) {
			printf ("vl_sift: found %d keypoints\n", (*nframes)) ;
		}
		// save variables:
		memcpy(DATAframes, frames, 4 * (*nframes ) * sizeof(double));
		memcpy(DATAdescr, descr, 128 * (*nframes ) * sizeof(vl_uint8));
		
		/* cleanup */
		vl_sift_delete (filt) ;
	} /* end: do job */


	return;
}

/*
int preMatch(bloom_filter& filter, unsigned modulus, vl_uint8* L2_pt, int K2 ){
	//Match descriptors!

	//double thresh ;
	//int  K1, K2, ND ;
	//K1 = Tnframes;
	//K2 = Qnframes;
	int ND = 128;
	//vl_uint8* L1_pt  ;
	//vl_uint8* L2_pt ;
	//int* nMatches;
	//double* MATCHES;

	int k1, k2 ;              
    int numMatches = 0;
    int bin;
    int j;
    unsigned char sbuf[128];

		/* For each point P2[k2] in the second image...              
		for(k2 =  0 ; k2 < K2 ; ++k2, L2_pt += ND) {

            std::string key;

            std::vector<int> hashes;
    		for(bin = 0 ; bin < ND ; bin++) {  
                sbuf[bin] = (vl_uint8)((128-L2_pt[ bin ]));
                key.append( 1, sbuf[bin] );
            }
            int hone = hash_search::hasher( key, modulus );
            if ( filter.contains( hone )) {
                numMatches++;
            }

		}                                                                 

    fprintf( stdout, "prematch indicates %d potential matches\n", numMatches );

	return numMatches;
}
*/

void VLGOODMATCH(vl_uint8* L1_pt,vl_uint8* L2_pt, int K1, int K2, double thresh, int* matchcount ){
	//Match descriptors!

	//double thresh ;
	//int  K1, K2, ND ;
	//K1 = Tnframes;
	//K2 = Qnframes;
	int ND = 128;
	//vl_uint8* L1_pt  ;
	//vl_uint8* L2_pt ;
	//int* nMatches;
	//double* MATCHES;

	int k1, k2 ;                                                        
	const int maxval = 0x7fffffff ;                         
    vl_uint8* saveptr = L2_pt;
    int sum1 = 0;
    int sum2 = 0;
    int savesum = 0;

	for(k1 = 0 ; k1 < K1 ; ++k1, L1_pt += ND ) {    //kalooo!                    

		int best = maxval ;                                     
		int second_best = maxval ;                              
		int bestk = -1 ;                                                  
        
		/* For each point P2[k2] in the second image... */                
		for(k2 =  0 ; k2 < K2 ; ++k2, L2_pt += ND) {                      

            if ( k2 == k1 ) {
                continue;
            }

			int bin ;                                                       
			int acc = 0 ;                                         
            sum1 = 0;
            sum2 = 0;

			for(bin = 0 ; bin < ND ; ++bin) {                               
				int delta =                                         
					((int) L1_pt[bin]) -                              
					((int) L2_pt[bin]) ;                              
				acc += delta*delta ;

                if ( acc > second_best ) {
                    break;
                }
			}                                                               

			/* Filter the best and second best matching point. */           
			if(acc < best) {                                                
				second_best = best ;                                          
				best = acc ;                                                  
				bestk = k2 ;
			} else if(acc < second_best) {                                  
				second_best = acc ;                                           
			}                                                               
		}                                                                 

		L2_pt = saveptr;                         

		if(thresh * (float) best < (float) second_best &&                 
			bestk != -1 ) {  
            matchcount[k1]++;
            matchcount[bestk]++;
        }
	}                                                                   


	return;
}

void FLANNMATCH( vl_uint8* L1_pt,vl_uint8* L2_pt, int K1, int K2, double thresh, int* nMatches, double *MATCHES )
{
    int nn = 2;

    std::vector <float> data(K1*128);
    for(int j=0; j < K1; j++) {
        for(int k=0; k < 128; k++) {
            data[j*128 + k] = L1_pt[j*128 + k];
        }
    }

    flann::Matrix<float> dataset(&data[0], K1, 128);
    flann::Index<flann::L2<float> > index(dataset, flann::KDTreeIndexParams(4));
    index.buildIndex();

    std::vector <float> queries_data(K2*128);
    for(int k=0; k < K2; k++) {
        for(int l=0; l < 128; l++) {
            queries_data[k*128 + l] = L2_pt[k*128 + l];
        }
    }

    std::vector <int> indices_data(K2*nn);
    std::vector <float> dists_data(K2*nn);

    flann::Matrix<float> queries(&queries_data[0], K2, 128);
    flann::Matrix<int> indices(&indices_data[0], K2, nn);
    flann::Matrix<float> dists(&dists_data[0], K2, nn);

    index.knnSearch(queries, indices, dists, nn, flann::SearchParams(128));

    for(int k=0; k < K2; k++) {
        float dist1 = dists_data[k*2];
        float dist2 = dists_data[k*2 + 1];

        if ( dist1*thresh < dist2 ) {
            *MATCHES++ = k;
            *MATCHES++ = indices_data[k*2];
            //KeypointMatch kp(k, indices_data[k*2]);
            //matches.push_back(kp);
        }
    }

}

void VLMATCH(vl_uint8* L1_pt,vl_uint8* L2_pt, int K1, int K2, double thresh, int* nMatches, double* MATCHES ){
	//Match descriptors!

	//double thresh ;
	//int  K1, K2, ND ;
	//K1 = Tnframes;
	//K2 = Qnframes;
	int ND = 128;
	//vl_uint8* L1_pt  ;
	//vl_uint8* L2_pt ;
	//int* nMatches;
	//double* MATCHES;

	Pair* pairs_begin = (Pair*) malloc(sizeof(Pair) * (K1+K2)) ;
	Pair* pairs_iterator = pairs_begin ;

	int k1, k2 ;                                                        
	const int maxval = 0x7fffffff ;                         
    vl_uint8* saveptr = L2_pt;
    int sum1 = 0;
    int sum2 = 0;
    int savesum = 0;

	for(k1 = 0 ; k1 < K1 ; ++k1, L1_pt += ND ) {    //kalooo!                    

		int best = maxval ;                                     
		int second_best = maxval ;                              
		int bestk = -1 ;                                                  
        
		/* For each point P2[k2] in the second image... */                
		for(k2 =  0 ; k2 < K2 ; ++k2, L2_pt += ND) {                      

			int bin ;                                                       
			int acc = 0 ;                                         
            sum1 = 0;
            sum2 = 0;

			for(bin = 0 ; bin < ND ; ++bin) {                               
				int delta =                                         
					((int) L1_pt[bin]) -                              
					((int) L2_pt[bin]) ;                              
				acc += delta*delta ;

                if ( acc > second_best ) {
                    break;
                }
			}                                                               

			/* Filter the best and second best matching point. */           
			if(acc < best) {                                                
				second_best = best ;                                          
				best = acc ;                                                  
				bestk = k2 ;        
			} else if(acc < second_best) {                                  
				second_best = acc ;                                           
			}                                                               
		}                                                                 

		L2_pt = saveptr;                         

		/* Lowe's method: accept the match only if unique. */             
		if(thresh * (float) best < (float) second_best &&                 
			bestk != -1 ) {                                                 
				pairs_iterator->k1 = k1 ;                                       
				pairs_iterator->k2 = bestk ;                                    
				pairs_iterator->score = best ;                                  
				pairs_iterator++ ;  
				(*nMatches)++;
		}
	}                                                                   

	Pair* pairs_end = pairs_iterator ;
	//double* M_pt = (double*)calloc((pairs_end-pairs_begin)*2,sizeof(double));
	double* M_pt = (double*)calloc((*nMatches)*2,sizeof(double));
	//double* M_start = M_pt;

	for(pairs_iterator = pairs_begin ;
		pairs_iterator < pairs_end  ;
		++pairs_iterator) {
			*M_pt++ = pairs_iterator->k1 ;
			*M_pt++ = pairs_iterator->k2 ;
	}
	M_pt -= (*nMatches)*2 ;
	memcpy(MATCHES,M_pt,(*nMatches) * 2 * sizeof(double));
	free(pairs_begin) ;
	free(M_pt);

	return;
}

typedef unsigned char uchar;
int main( int argc, char *argv[] )
{
    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " image1.png image2.png" << std::endl;
        return 1;
    }

    const char *image1 = argv[1];
    const char *image2 = argv[2];


    bloom_parameters parameters;

    // How many elements roughly do we expect to insert?
    parameters.projected_element_count = 12000;

    // Maximum tolerable false positive probability? (0,1)
    parameters.false_positive_probability = 0.01; // 1 in 100

    // Simple randomizer (optional)
    parameters.random_seed = 0xA5A5A5A5;

    if (!parameters)
    {
      std::cout << "Error - Invalid set of bloom filter parameters!" << std::endl;
      return 1;
    }

    parameters.compute_optimal_parameters();

    std::cout << parameters.optimal_parameters.number_of_hashes << "\n";
    std::cout << parameters.optimal_parameters.table_size << "\n";

    //Instantiate Bloom Filter
    bloom_filter filter1(parameters);
    bloom_filter filter2(parameters);

    unsigned modulus = hash_search::prime_generator( 247483647 );

	// Load template image:
	IplImage* Timage = cvLoadImage(image1,0);

	double*            TFrames = (double*)calloc ( ALLOC_FRAMES , sizeof(double) ) ;
	vl_uint8*          TDescr  = (vl_uint8*)calloc ( ALLOC_DESCS, sizeof(vl_uint8) ) ;
	int                Tnframes = 0;

    if ( TFrames == NULL ) {
        fprintf( stderr, " Could not alloc memory for frames\n" );
        exit(-1);
    }
    if ( TDescr == NULL ) {
        fprintf( stderr, " Could not alloc memory for descriptors\n" );
        exit(-1);
    }

	VLSIFT(Timage, TDescr, TFrames, &Tnframes);
	TFrames = (double*)realloc (TFrames, 4 * sizeof(double) * Tnframes) ; // = Y X Scale Angle
	TDescr  = (vl_uint8*)realloc (TDescr,  128 * sizeof(vl_uint8) * Tnframes) ;
	
	Timage = cvLoadImage(image1);
    desaturateImage( Timage );
	for(int i=0;i<Tnframes;i++){
		cvCircle(Timage,                
		cvPoint(TFrames[0+i*4], TFrames[1+i*4]), TFrames[2+i*4],   
		cvScalar(255, 0, 0, 0),  
		1, 8, 0);  
	}
    cvSaveImage("FrameT.png", Timage );

	// Load query frame:
	IplImage* Qimage = cvLoadImage(image2,0);

	double*            QFrames = (double*)calloc ( ALLOC_FRAMES , sizeof(double) ) ;
	vl_uint8*          QDescr  = (vl_uint8*)calloc ( ALLOC_DESCS, sizeof(vl_uint8) ) ;
	int                Qnframes = 0;
	VLSIFT(  Qimage, QDescr, QFrames, &Qnframes);
	QFrames = (double*)realloc (QFrames, 4 * sizeof(double) * Qnframes) ;
	QDescr  = (vl_uint8*)realloc (QDescr,  128 * sizeof(vl_uint8) * Qnframes) ;
	
	Qimage = cvLoadImage(image2);
    desaturateImage( Qimage );
	for(int i=0;i<Qnframes;i++){
	cvCircle(Qimage,                
	cvPoint(QFrames[0+i*4], QFrames[1+i*4]), QFrames[2+i*4],   
	cvScalar(255, 0, 0, 0),  
	1, 8, 0);  
	}
    cvSaveImage("FrameQ.png", Qimage );

    clock_t start = clock(), diff;

	// Match process:
	int matchesFound=0;
	double* MATCHES = (double*)calloc( 2*12000, sizeof(double) ) ;
    // 2.3 practically gets rid of false matches
    // false positives 

	VLMATCH(TDescr,QDescr, Tnframes, Qnframes, 2.4, &matchesFound, MATCHES );

    diff = clock() - start;

	//Display matches for user checking:
    printf( "Matches found: %d\n", matchesFound );

    int msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Time taken brute force match %d seconds %d milliseconds\n", msec/1000, msec%1000);

    start = clock();
    memset( MATCHES, 0x00, 2*12000 * sizeof(double) );
    FLANNMATCH( TDescr,QDescr, Tnframes, Qnframes, 2.4, &matchesFound, MATCHES );

    diff = clock() - start;

	//Display matches for user checking:
    printf( "Matches found: %d\n", matchesFound );

    msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Time taken flann match %d seconds %d milliseconds\n", msec/1000, msec%1000);

    // load image again
	Timage = cvLoadImage(image1);
    desaturateImage( Timage );
	for(int i=0;i<Tnframes;i++){
		for(int m=0;m<matchesFound;m++){
			if(i == MATCHES[m*2]){
				cvCircle(Timage,                
					cvPoint(TFrames[0+i*4], TFrames[1+i*4]), TFrames[2+i*4],   
					cvScalar(255, 0, 0, 0),  
					1, 8, 0); 
			}
		}
	}
    cvSaveImage("matchT.png", Timage );

    // load image again to clean it up
	Qimage = cvLoadImage(image2);
    desaturateImage( Qimage );
	for(int i=0;i<Qnframes;i++){
		for(int m=0;m<matchesFound;m++){
			if(i == MATCHES[m*2+1]){
				cvCircle(Qimage,                
					cvPoint(QFrames[0+i*4], QFrames[1+i*4]), QFrames[2+i*4],   
					cvScalar(255, 0, 0, 0),  
					1, 8, 0); 
			}
		}
	}
    cvSaveImage("matchQ.png", Qimage );

    IplImage *stitch;
    stitch = cvCreateImage(cvSize(Qimage->width*2,Qimage->height), Qimage->depth, Qimage->nChannels);

    int x_offset = Qimage->width;

    cvSetImageROI(stitch, cvRect(0*x_offset, 0, Qimage->width, Qimage->height));
    cvCopy(Timage, stitch);
    cvSetImageROI(stitch, cvRect(1*x_offset, 0, Qimage->width, Qimage->height));
    cvCopy(Qimage,stitch);
    cvResetImageROI(stitch);

    for(int m=0;m<matchesFound;m++){
    	for(int q=0;q<Qnframes;q++){
 			if(q == MATCHES[m*2+1]){
            	for(int t=0;t<Tnframes;t++){
			        if(t == MATCHES[m*2]){
                        cvLine( stitch,
                            cvPoint(TFrames[0+t*4], TFrames[1+t*4]),
                            cvPoint(QFrames[0+q*4]+x_offset, QFrames[1+q*4]),
                            cvScalar(255, 0, 0, 0),
                            1, 8, 0 );
                    }
                }
			}
		}
	}

    cvSaveImage("stitch.png", stitch );

	return(0);
}

void desaturateImage( IplImage *img )
{
    // BGR to HSV
    cvCvtColor(img, img, CV_BGR2HSV);    
    for (int i=0; i < img->height ; i++)
    {
        uchar *data = (uchar *)(img->imageData + i * img->widthStep);
          for(int j=0; j < img->width; j++)
          {
                data[3*j+1] = 64;
          }
    }
    // HSV back to BGR
    cvCvtColor(img, img, CV_HSV2BGR);
}

/*
    pick a hashsize, lets say 32 bits
    let V = [0] * 32 # (ie 32 zeros)
    break the phrase up into features

    irb(main):003:0> 'the cat sat on the mat'.shingles
    => #<Set: {"th", "he", "e ", " c", "ca", "at", "t ",
        " s", "sa", " o", "on", "n ", " t", " m", "ma"}>

    hash each feature using a normal 32-bit hash algorithm

    "th".hash = -502157718
    "he".hash = -369049682
    ...

    for each hash
    if biti of hash is set then add 1 to V[i]
    if biti of hash is not set then take 1 from V[i]
    simhash biti is 1 if V[i] > 0 and 0 otherwise
*/
int calculate_simhash( std::vector<int> hashes )
{
    int v[32];
    
    for ( int i = 0; i < 32; i++ ) {
        v[i] = 0;
    }

    for ( int i = 0; i < hashes.size(); i++ ) {
        int hash = hashes[i];
        for ( int j = 0; j < 32; j++ ) {
            int mask =  1 << j;
            if ((hash & mask) != 0) {
                v[j]++;
            } else {
                v[j]--;
            }
        }
    }

    int result = 0;

    for ( int j = 0; j < 32; j++ ) {
        if ( v[j] > 0 ) {
            result |= (1<<j);
        }
    }

    return result;
}

