#!/usr/bin/python

import os
import glob
import argparse
from PIL import Image, ExifTags
import pyproj
import math
import cameraintrinsics_pb2

RADIUS = 40
MAX_ALT = 2000
# 10 km?
MAXDIST = 10000

VERSION = "geotagger 0.1"
DESCRIPTION = """\
Python convenience module to process a series of images and 
populate it with geotags and extract focal lengths and other data."""

CCD_WIDTHS = {
     "Asahi Optical Co.,Ltd.  PENTAX Optio330RS" : 7.176, # 1/1.8"
     "Canon Canon DIGITAL IXUS 400"              : 7.176,  # 1/1.8"
     "Canon Canon DIGITAL IXUS 40"               : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS 430"              : 7.176,  # 1/1.8"
     "Canon Canon DIGITAL IXUS 500"              : 7.176,  # 1/1.8"
     "Canon Canon DIGITAL IXUS 50"               : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS 55"               : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS 60"               : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS 65"               : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS 700"              : 7.176,  # 1/1.8"
     "Canon Canon DIGITAL IXUS 750"              : 7.176,  # 1/1.8"
     "Canon Canon DIGITAL IXUS 800 IS"           : 5.76,   # 1/2.5"
     "Canon Canon DIGITAL IXUS II"               : 5.27,   # 1/2.7"
     "Canon Canon EOS 10D"                       : 22.7,
     "Canon Canon EOS-1D Mark II"                : 28.7,
     "Canon Canon EOS-1Ds Mark II"               : 35.95,   
     "Canon Canon EOS  20D"                      : 22.5,
     "Canon Canon EOS 20D"                       : 22.5,
     "Canon Canon EOS 300D DIGITAL"              : 22.66,
     "Canon Canon EOS 30D"                       : 22.5,
     "Canon Canon EOS 350D DIGITAL"              : 22.2,
     "Canon Canon EOS 400D DIGITAL"              : 22.2,
     "Canon Canon EOS 40D"                       : 22.2,
     "Canon Canon EOS 5D"                        : 35.8,
     "Canon Canon EOS DIGITAL REBEL"             : 22.66,
     "Canon Canon EOS DIGITAL REBEL XT"          : 22.2,
     "Canon Canon EOS DIGITAL REBEL XTi"         : 22.2,
     "Canon Canon EOS Kiss Digital"              : 22.66,
     "Canon Canon IXY DIGITAL 600"               : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A20"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A400"                : 4.54,   # 1/3.2"
     "Canon Canon PowerShot A40"                 : 5.27,   # 1/2.7"
     "Canon Canon PowerShot A510"                : 5.76,   # 1/2.5"
     "Canon Canon PowerShot A520"                : 5.76,   # 1/2.5"
     "Canon Canon PowerShot A530"                : 5.76,   # 1/2.5"
     "Canon Canon PowerShot A60"                 : 5.27,   # 1/2.7"
     "Canon Canon PowerShot A620"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A630"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A640"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A700"                : 5.76,   # 1/2.5"
     "Canon Canon PowerShot A70"                 : 5.27,   # 1/2.7"
     "Canon Canon PowerShot A710 IS"             : 5.76,   # 1/2.5"
     "Canon Canon PowerShot A75"                 : 5.27,   # 1/2.7"
     "Canon Canon PowerShot A80"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot A85"                 : 5.27,   # 1/2.7"
     "Canon Canon PowerShot A95"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G1"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G2"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G3"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G5"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G6"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G7"                  : 7.176,  # 1/1.8"
     "Canon Canon PowerShot G9"                  : 7.600,  # 1/1.7"
     "Canon Canon PowerShot Pro1"                : 8.8,    # 2/3"
     "Canon Canon PowerShot S110"                : 5.27,   # 1/2.7"
     "Canon Canon PowerShot S1 IS"               : 5.27,   # 1/2.7"
     "Canon Canon PowerShot S200"                : 5.27,   # 1/2.7"
     "Canon Canon PowerShot S2 IS"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot S30"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S3 IS"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot S400"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S40"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S410"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S45"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S500"                : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S50"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S60"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S70"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot S80"                 : 7.176,  # 1/1.8"
     "Canon Canon PowerShot SD1000"              : 5.75,   # 1/2.5"
     "Canon Canon PowerShot SD100"               : 5.27,   # 1/2.7"
     "Canon Canon PowerShot SD10"                : 5.75,   # 1/2.5"
     "Canon Canon PowerShot SD110"               : 5.27,   # 1/2.7"
     "Canon Canon PowerShot SD200"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD300"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD400"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD450"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD500"               : 7.176,  # 1/1.8"
     "Canon Canon PowerShot SD550"               : 7.176,  # 1/1.8"
     "Canon Canon PowerShot SD600"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD630"               : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD700 IS"            : 5.76,   # 1/2.5"
     "Canon Canon PowerShot SD750"               : 5.75,   # 1/2.5"
     "Canon Canon PowerShot SD800 IS"            : 5.76,   # 1/2.5"
     "Canon EOS 300D DIGITAL"                    : 22.66,
     "Canon EOS DIGITAL REBEL"                   : 22.66,
     "Canon PowerShot A510"                      : 5.76,   # 1/2.5" ???
     "Canon PowerShot S30"                       : 7.176,  # 1/1.8"
     "CASIO COMPUTER CO.,LTD. EX-S500"           : 5.76,   # 1/2.5"
     "CASIO COMPUTER CO.,LTD. EX-Z1000"          : 7.716, # 1/1.8"
     "CASIO COMPUTER CO.,LTD  EX-Z30"            : 5.76,   # 1/2.5 "
     "CASIO COMPUTER CO.,LTD. EX-Z600"           : 5.76,   # 1/2.5"
     "CASIO COMPUTER CO.,LTD. EX-Z60"            : 7.176, # 1/1.8"
     "CASIO COMPUTER CO.,LTD  EX-Z750"           : 7.176, # 1/1.8"
     "CASIO COMPUTER CO.,LTD. EX-Z850"           : 7.176,
     "EASTMAN KODAK COMPANY KODAK CX7330 ZOOM DIGITAL CAMERA" : 5.27, # 1/2.7"
     "EASTMAN KODAK COMPANY KODAK CX7530 ZOOM DIGITAL CAMERA" : 5.76, # 1/2.5"
     "EASTMAN KODAK COMPANY KODAK DX3900 ZOOM DIGITAL CAMERA" : 7.176, # 1/1.8"
     "EASTMAN KODAK COMPANY KODAK DX4900 ZOOM DIGITAL CAMERA" : 7.176, # 1/1.8"
     "EASTMAN KODAK COMPANY KODAK DX6340 ZOOM DIGITAL CAMERA" : 5.27, # 1/2.7"
     "EASTMAN KODAK COMPANY KODAK DX6490 ZOOM DIGITAL CAMERA" : 5.76, # 1/2.5"
     "EASTMAN KODAK COMPANY KODAK DX7630 ZOOM DIGITAL CAMERA" : 7.176, # 1/1.8"
     "EASTMAN KODAK COMPANY KODAK Z650 ZOOM DIGITAL CAMERA"   : 5.76, # 1/2.5"
     "EASTMAN KODAK COMPANY KODAK Z700 ZOOM DIGITAL CAMERA"   : 5.76, # 1/2.5"
     "EASTMAN KODAK COMPANY KODAK Z740 ZOOM DIGITAL CAMERA"   : 5.76, # 1/2.5"
     "EASTMAN KODAK COMPANY KODAK Z740 ZOOM DIGITAL CAMERA"   : 5.76, # 1/2.5"?
     "FUJIFILM FinePix2600Zoom"                  : 5.27,   # 1/2.7"
     "FUJIFILM FinePix40i"                       : 7.600,  # 1/1.7" 
     "FUJIFILM FinePix A310"                     : 5.27,   # 1/2.7"
     "FUJIFILM FinePix A330"                     : 5.27,   # 1/2.7"
     "FUJIFILM FinePix A600"                     : 7.600,  # 1/1.7"
     "FUJIFILM FinePix E500"                     : 5.76,   # 1/2.5" 
     "FUJIFILM FinePix E510"                     : 5.76,   # 1/2.5"
     "FUJIFILM FinePix E550"                     : 7.600,  # 1/1.7" 
     "FUJIFILM FinePix E900"                     : 7.78,   # 1/1.6"
     "FUJIFILM FinePix F10"                      : 7.600, # 1/1.7"
     "FUJIFILM FinePix F30"                      : 7.600,  # 1/1.7"
     "FUJIFILM FinePix F450"                     : 5.76,   # 1/2.5"
     "FUJIFILM FinePix F601 ZOOM"                : 7.600,  # 1/1.7"
     "FUJIFILM FinePix S3Pro"                    : 23.0,
     "FUJIFILM FinePix S5000"                    : 5.27,   # 1/2.7"
     "FUJIFILM FinePix S5200"                    : 5.76,   # 1/2.5"
     "FUJIFILM FinePix S5500"                    : 5.27,   # 1/2.7"
     "FUJIFILM FinePix S6500fd"                  : 7.600,  # 1/1.7"
     "FUJIFILM FinePix S7000"                    : 7.600,  # 1/1.7"
     "FUJIFILM FinePix Z2"                       : 5.76,   # 1/2.5"
     "Hewlett-Packard hp 635 Digital Camera"     : 4.54, # 1/3.2"
     "Hewlett-Packard hp PhotoSmart 43x series"  : 5.27,  # 1/2.7"
     "Hewlett-Packard HP PhotoSmart 618 (V1.1)"  : 5.27,  # 1/2.7"
     "Hewlett-Packard HP PhotoSmart C945 (V01.61)" : 7.176, # 1/1.8"
     "Hewlett-Packard HP PhotoSmart R707 (V01.00)" : 7.176, # 1/1.8"
     "KONICA MILOLTA  DYNAX 5D"                  : 23.5,
     "Konica Minolta Camera, Inc. DiMAGE A2"     : 8.80, # 2/3"
     "KONICA MINOLTA CAMERA, Inc. DiMAGE G400"   : 5.76, # 1/2.5"
     "Konica Minolta Camera, Inc. DiMAGE Z2"     : 5.76, # 1/2.5"
     "KONICA MINOLTA DiMAGE A200"                : 8.80,   # 2/3"
     "KONICA MINOLTA DiMAGE X1"                  : 7.176,  # 1/1.8"
     "KONICA MINOLTA  DYNAX 5D"                  : 23.5,
     "Minolta Co., Ltd. DiMAGE F100"             : 7.176,  # 1/2.7"
     "Minolta Co., Ltd. DiMAGE Xi"               : 5.27,   # 1/2.7"
     "Minolta Co., Ltd. DiMAGE Xt"               : 5.27,   # 1/2.7"
     "Minolta Co., Ltd. DiMAGE Z1"               : 5.27, # 1/2.7" 
     "NIKON COOLPIX L3"                          : 5.76,   # 1/2.5"
     "NIKON COOLPIX P2"                          : 7.176,  # 1/1.8"
     "NIKON COOLPIX S4"                          : 5.76,   # 1/2.5"
     "NIKON COOLPIX S7c"                         : 5.76,   # 1/2.5"
     "NIKON CORPORATION NIKON D100"              : 23.7,
     "NIKON CORPORATION NIKON D1"                : 23.7,
     "NIKON CORPORATION NIKON D1H"               : 23.7,
     "NIKON CORPORATION NIKON D200"              : 23.6,
     "NIKON CORPORATION NIKON D2H"               : 23.3,
     "NIKON CORPORATION NIKON D2X"               : 23.7,
     "NIKON CORPORATION NIKON D40"               : 23.7,
     "NIKON CORPORATION NIKON D50"               : 23.7,
     "NIKON CORPORATION NIKON D60"               : 23.6,
     "NIKON CORPORATION NIKON D70"               : 23.7,
     "NIKON CORPORATION NIKON D70s"              : 23.7,
     "NIKON CORPORATION NIKON D80"               : 23.6,
     "NIKON E2500"                               : 5.27,   # 1/2.7"
     "NIKON E2500"                               : 5.27,   # 1/2.7"
     "NIKON E3100"                               : 5.27,   # 1/2.7"
     "NIKON E3200"                               : 5.27,
     "NIKON E3700"                               : 5.27,   # 1/2.7"
     "NIKON E4200"                               : 7.176,  # 1/1.8"
     "NIKON E4300"                               : 7.18,
     "NIKON E4500"                               : 7.176,  # 1/1.8"
     "NIKON E4600"                               : 5.76,   # 1/2.5"
     "NIKON E5000"                               : 8.80,   # 2/3"
     "NIKON E5200"                               : 7.176,  # 1/1.8"
     "NIKON E5400"                               : 7.176,  # 1/1.8"
     "NIKON E5600"                               : 5.76,   # 1/2.5"
     "NIKON E5700"                               : 8.80,   # 2/3"
     "NIKON E5900"                               : 7.176,  # 1/1.8"
     "NIKON E7600"                               : 7.176,  # 1/1.8"
     "NIKON E775"                                : 5.27,   # 1/2.7"
     "NIKON E7900"                               : 7.176,  # 1/1.8"
     "NIKON E7900"                               : 7.176,  # 1/1.8"
     "NIKON E8800"                               : 8.80,   # 2/3"
     "NIKON E990"                                : 7.176,  # 1/1.8"
     "NIKON E995"                                : 7.176,  # 1/1.8"
     "NIKON S1"                                  : 5.76,   # 1/2.5"
     "Nokia N80"                                 : 5.27,   # 1/2.7"
     "Nokia N80"                                 : 5.27,   # 1/2.7"
     "Nokia N93"                                 : 4.536,  # 1/3.1"
     "Nokia N95"                                 : 5.7,    # 1/2.7"
     "OLYMPUS CORPORATION     C-5000Z"           : 7.176,  # 1/1.8"
     "OLYMPUS CORPORATION C5060WZ"               : 7.176, # 1/1.8"
     "OLYMPUS CORPORATION C750UZ"                : 5.27,   # 1/2.7"
     "OLYMPUS CORPORATION C765UZ"                : 5.76,   # 1//2.5"
     "OLYMPUS CORPORATION C8080WZ"               : 8.80,   # 2/3"
     "OLYMPUS CORPORATION X250,D560Z,C350Z"      : 5.76, # 1/2.5" 
     "OLYMPUS CORPORATION     X-3,C-60Z"         : 7.176, # 1.8"
     "OLYMPUS CORPORATION X400,D580Z,C460Z"      : 5.27,  # 1/2.7"
     "OLYMPUS IMAGING CORP.   E-500"             : 17.3,  # 4/3?
     "OLYMPUS IMAGING CORP.   FE115,X715"        : 5.76, # 1/2.5"
     "OLYMPUS IMAGING CORP. SP310"               : 7.176, # 1/1.8"
     "OLYMPUS IMAGING CORP.   SP510UZ"           : 5.75,   # 1/2.5"
     "OLYMPUS IMAGING CORP.   SP550UZ"           : 5.76, # 1/2.5"
     "OLYMPUS IMAGING CORP.   uD600,S600"        : 5.75, # 1/2.5" 
     "OLYMPUS_IMAGING_CORP.   X450,D535Z,C370Z"  : 5.27, # 1/2.7" 
     "OLYMPUS IMAGING CORP. X550,D545Z,C480Z"    : 5.76, # 1/2.5" 
     "OLYMPUS OPTICAL CO.,LTD C2040Z"            : 6.40,  # 1/2"
     "OLYMPUS OPTICAL CO.,LTD C211Z"             : 5.27,   # 1/2.7"
     "OLYMPUS OPTICAL CO.,LTD C2Z,D520Z,C220Z"   : 4.54, # 1/3.2"
     "OLYMPUS OPTICAL CO.,LTD C3000Z"            : 7.176, # 1/1.8"
     "OLYMPUS OPTICAL CO.,LTD C300Z,D550Z"       : 5.4,
     "OLYMPUS OPTICAL CO.,LTD C4100Z,C4000Z"     : 7.176,  # 1/1.8" 
     "OLYMPUS OPTICAL CO.,LTD C750UZ"            : 5.27,  # 1/2.7"
     "OLYMPUS OPTICAL CO.,LTD X-2,C-50Z"         : 7.176, # 1/1.8"
     "OLYMPUS SP550UZ"                           : 5.76,  # 1/2.5"
     "OLYMPUS X100,D540Z,C310Z"                  : 5.27,   # 1/2.7"
     "Panasonic DMC-FX01"                        : 5.76,   # 1/2.5"
     "Panasonic DMC-FX07"                        : 5.75,   # 1/2.5"
     "Panasonic DMC-FX9"                         : 5.76,   # 1/2.5"
     "Panasonic DMC-FZ20"                        : 5.760,  # 1/2.5"
     "Panasonic DMC-FZ2"                         : 4.54,   # 1/3.2"
     "Panasonic DMC-FZ30"                        : 7.176,  # 1/1.8"
     "Panasonic DMC-FZ50"                        : 7.176,  # 1/1.8"
     "Panasonic DMC-FZ5"                         : 5.760,  # 1/2.5"
     "Panasonic DMC-FZ7"                         : 5.76,   # 1/2.5"
     "Panasonic DMC-LC1"                         : 8.80,   # 2/3"
     "Panasonic DMC-LC33"                        : 5.760,  # 1/2.5"
     "Panasonic DMC-LX1"                         : 8.50,   # 1/6.5"
     "Panasonic DMC-LZ2"                         : 5.76,   # 1/2.5"
     "Panasonic DMC-TZ1"                         : 5.75,   # 1/2.5"
     "Panasonic DMC-TZ3"                         : 5.68,   # 1/2.35"
     "PENTAX Corporation  PENTAX *ist DL"        : 23.5,
     "PENTAX Corporation  PENTAX *ist DS2"       : 23.5,
     "PENTAX Corporation  PENTAX *ist DS"        : 23.5,
     "PENTAX Corporation  PENTAX K100D"          : 23.5,
     "PENTAX Corporation PENTAX Optio 450"       : 7.176, # 1/1.8"
     "PENTAX Corporation PENTAX Optio 550"       : 7.176, # 1/1.8"
     "PENTAX Corporation PENTAX Optio E10"       : 5.76, # 1/2.5"
     "PENTAX Corporation PENTAX Optio S40"       : 5.76, # 1/2.5"
     "PENTAX Corporation  PENTAX Optio S4"       : 5.76, # 1/2.5" 
     "PENTAX Corporation PENTAX Optio S50"       : 5.76, # 1/2.5"
     "PENTAX Corporation  PENTAX Optio S5i"      : 5.76, # 1/2.5" 
     "PENTAX Corporation  PENTAX Optio S5z"      : 5.76, # 1/2.5" 
     "PENTAX Corporation  PENTAX Optio SV"       : 5.76, # 1/2.5" 
     "PENTAX Corporation PENTAX Optio WP"        : 5.75, # 1/2.5" 
     "RICOH CaplioG3 modelM"                     : 5.27,   # 1/2.7"
     "RICOH       Caplio GX"                     : 7.176,  # 1/1.8"
     "RICOH       Caplio R30"                    : 5.75,   # 1/2.5"
     "Samsung  Digimax 301"                      : 5.27,   # 1/2.7"
     "Samsung Techwin <Digimax i5, Samsung #1>"  : 5.76,   # 1/2.5"
     "SAMSUNG TECHWIN Pro 815"                   : 8.80,   # 2/3"
     "SONY DSC-F828"                             : 8.80,   # 2/3"
     "SONY DSC-N12"                              : 7.176,  # 1/1.8"
     "SONY DSC-P100"                             : 7.176,  # 1/1.8"
     "SONY DSC-P10"                              : 7.176,  # 1/1.8"
     "SONY DSC-P12"                              : 7.176,  # 1/1.8"
     "SONY DSC-P150"                             : 7.176,  # 1/1.8"
     "SONY DSC-P200"                             : 7.176,   # 1/1.8");
     "SONY DSC-P52"                              : 5.27,   # 1/2.7"
     "SONY DSC-P72"                              : 5.27,   # 1/2.7"
     "SONY DSC-P73"                              : 5.27,
     "SONY DSC-P8"                               : 5.27,   # 1/2.7"
     "SONY DSC-R1"                               : 21.5,
     "SONY DSC-S40"                              : 5.27,   # 1/2.7"
     "SONY DSC-S600"                             : 5.760,  # 1/2.5"
     "SONY DSC-T9"                               : 7.18,
     "SONY DSC-V1"                               : 7.176,  # 1/1.8"
     "SONY DSC-W1"                               : 7.176,  # 1/1.8"
     "SONY DSC-W30"                              : 5.760,  # 1/2.5"
     "SONY DSC-W50"                              : 5.75,   # 1/2.5"
     "SONY DSC-W5"                               : 7.176,  # 1/1.8"
     "SONY DSC-W7"                               : 7.176,  # 1/1.8"
     "SONY DSC-W80"                              : 5.75,   # 1/2.5"
}

def get_images( imagedir ):
    """Searches the present directory for JPEG images."""
    images = sorted(glob.glob(os.path.join(imagedir,"*.[jJ][pP][gG]")))
    if len(images) == 0:
        error_str = ("Error: No images supplied!  "
                     "No JPEG files found in directory %s!"%(imagedir))
        raise Exception(error_str)
    return images

def extract_focal_length(image, scale=1.0):
    """Extracts (pixel) focal length from images where available.
    The functions returns a dictionary of image, focal length pairs.
    If no focal length is extracted for an image, the second pair is None.
    """
    ret = None
    #print "[Extracting EXIF tags from image {0}]".format(image)

    tags = {}
    with open(image, 'rb') as fp:
        img = Image.open(fp)
        if hasattr(img, '_getexif'):
            exifinfo = img._getexif()
            if exifinfo is not None:
                for tag, value in exifinfo.items():
                    tags[ExifTags.TAGS.get(tag, tag)] = value

    # Extract Focal Length
    focalN, focalD = tags.get('FocalLength', (0, 1))
    focal_length = float(focalN)/float(focalD)

    # Extract Resolution
    img_width = tags.get('ExifImageWidth', 0)
    img_height = tags.get('ExifImageHeight', 0)
    if img_width < img_height:
        img_width,img_height = img_height,img_width

    # Extract CCD Width (Prefer Lookup Table)
    ccd_width = 1.0
    make_model = tags.get('Make', '') + ' ' + tags.get('Model', '')
    if CCD_WIDTHS.has_key(make_model.strip()):
        ccd_width = CCD_WIDTHS[make_model.strip()]
    else:
        fplaneN, fplaneD = tags.get('FocalPlaneXResolution', (0, 1))
        if fplaneN != 0:
            ccd_width = 25.4*float(img_width)*float(fplaneD)/float(fplaneN)
            #print "  [Using CCD width from EXIF tags]"
        else:
            ccd_width = 0

    #print "  [EXIF focal length = {0}mm]".format(focal_length)
    #print "  [EXIF CCD width = {0}mm]".format(ccd_width)
    #print "  [EXIF resolution = {0} x {1}]".format(img_width, img_height)
    #if ccd_width == 0:
        #print "  [No CCD width available for camera {0}]".format(make_model)

    if (img_width==0 or img_height==0 or focalN==0 or ccd_width==0):
        #print "  [Could not determine pixel focal length]"
        return ret, None, None

    # Compute Focal Length in Pixels
    ret = float(img_width) * (focal_length / ccd_width) * scale
    #print "  [Focal length (pixels) = {%f}]"%(ret)

    return ret, img_width, img_height

def get_exif_data(image):
    """Returns a dictionary from the exif data of an PIL Image item. Also converts the GPS Tags"""
    exif_data = {}
    fp = open(image, 'rb')
    img = Image.open(fp)
    info = img._getexif()
    if info:
        for tag, value in info.items():
            decoded = ExifTags.TAGS.get(tag, tag)
            if decoded == "GPSInfo":
                gps_data = {}
                for t in value:
                    sub_decoded = ExifTags.GPSTAGS.get(t, t)
                    gps_data[sub_decoded] = value[t]

                    exif_data[decoded] = gps_data
            else:
                exif_data[decoded] = value
     
    return exif_data
 
def _get_if_exist(data, key):
    if key in data:
        return data[key]
    return None

def _convert_to_degress(value):
    """Helper function to convert the GPS coordinates stored in the EXIF to degress in float format"""
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)
 
    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)
 
    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)
 
    return d + (m / 60.0) + (s / 3600.0)
 
def get_lat_lon_alt(exif_data):
    """Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)"""
    lat = None
    lon = None
    alt = None

    if "GPSInfo" in exif_data:	
        gps_info = exif_data["GPSInfo"]
    else:
        return (None, None, None)
     
    gps_latitude = _get_if_exist(gps_info, "GPSLatitude")
    gps_latitude_ref = _get_if_exist(gps_info, 'GPSLatitudeRef')
    gps_longitude = _get_if_exist(gps_info, 'GPSLongitude')
    gps_longitude_ref = _get_if_exist(gps_info, 'GPSLongitudeRef')
    gps_altitude = _get_if_exist(gps_info, 'GPSAltitude')
    gps_altitude_ref = gps_info.get('GPSAltitudeRef')
     
    if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
        lat = _convert_to_degress(gps_latitude)
        if gps_latitude_ref != "N":
            lat = 0 - lat
        lon = _convert_to_degress(gps_longitude)
        if gps_longitude_ref != "E":
            lon = 0 - lon

    if gps_altitude:
        alt = float(gps_altitude[0]) / float(gps_altitude[1])
        if gps_altitude_ref == 1:
            alt *=-1

    if (lat > 90) or (lat<-90):
        # invalid
        return None, None, None

    if ( lon>180) or (lon<-180):
        return None, None, None

    if (alt>MAX_ALT):
        alt = 0.0

    return (lat, lon, alt)

def convert_lat_lon_utm( lat, lon ):
    p = pyproj.Proj(proj='utm', zone=32, ellps='WGS84')
    x,y = p(lat, lon)
    return (x,y)

def calculate_distance( pos1, pos2 ):
    x1,y1 = pos1
    x2,y2 = pos2
    x = abs(x1-x2)
    y = abs(y1-y2)

    #print x1,x2,y1,y2,x, y

    return math.sqrt(x*x+y*y)

def calculate_3d_distance( pos1, pos2, alt1, alt2 ):
    x1,y1 = pos1
    x2,y2 = pos2
    x = abs(x1-x2)
    y = abs(y1-y2)
    z = abs(alt1-alt2)

    return math.sqrt(x*x+y*y+z*z)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--version', action='version', version=VERSION)
    parser.add_argument('dir', type=str, action='store', help="Location of images" )
    args = parser.parse_args()

    imagedata = []

    # Figure out which images we have
    images = get_images( args.dir )
    for image in images:
        (f,width,height) = extract_focal_length( image )
        exif_data = get_exif_data(image)
        (lat, lon, alt) = get_lat_lon_alt( exif_data )
        #if f == None: 
        #    print "No focal length found for %s"%(image)
        #else: 
        #    print "Focal length for %s is %f"%(image, f )

        if ( lat == None or lon == None or alt == None ):
            path, filename = os.path.split(image)
            x = -1
            y = -1
            filename, ext = os.path.splitext(filename)
            imagedata.append ( (filename, (f,width,height), (None, None, None), (x,y), []) )
        else:
            #print "Lat: %f, Lon: %f, Alt: %f"%(lat, lon, alt)
            x,y = convert_lat_lon_utm( lat, lon )
            #print "X: %f, Y: %f"%(x,y)
            path, filename = os.path.split(image)
            filename, ext = os.path.splitext(filename)
            imagedata.append ( (filename, (f,width,height), (lat,lon,alt), (x,y), []) )
   
    distmap = {}
    for i in xrange(len(imagedata)):
        total = 0
        for j in xrange(len(imagedata)):
            if j == i:
                continue
            pos1 = imagedata[i][3]
            pos2 = imagedata[j][3]
            alt1 = imagedata[i][2][2]
            alt2 = imagedata[j][2][2]
            if ( pos1[0] == -1 or pos2[0] == -1 ):
                continue

            dist = calculate_3d_distance( pos1, pos2, alt1, alt2 )
            total += dist

        path, filename = os.path.split(imagedata[i][0] )
        filename, ext = os.path.splitext(filename)
        distmap[ filename ] = total

    #sortedlist = sorted(distmap, key=lambda key: distmap[key])
    #middle_image = sortedlist[0]

    #pathfile = open( "%s/path.txt"%(args.dir), "w" )
    #pathfile.write( "__NUM_IMAGES__" )
    #pathfile.write( '\t' )
    #pathfile.write( '%d'%(len(imagedata)) )
    #pathfile.write( '\n' )

    # Now match images to others
    for i in xrange(len(imagedata)):
        sortedmap = {}
        for j in xrange(i+1, len(imagedata)):
            pos1 = imagedata[i][3]
            pos2 = imagedata[j][3]
            alt1 = imagedata[i][2][2]
            alt2 = imagedata[j][2][2]
            maxalt = max(alt1,alt2)

            if ( pos1[0] == -1 or pos2[0] == -1 ):
                print "Don't know case %s and %s, just adding it"%(imagedata[i][0], imagedata[j][0])
                # possible match.. append j
                path, filename = os.path.split(imagedata[j][0] )
                filename, ext = os.path.splitext(filename)
                # imagedata[i][4].append( filename )
                sortedmap[ filename ] = MAXDIST
            else:
                dist = calculate_distance( pos1, pos2 )

                if (( dist < maxalt ) or ( dist > MAXDIST )):
                    path, filename = os.path.split(imagedata[j][0] )
                    filename, ext = os.path.splitext(filename)
                    # imagedata[i][4].append( filename )
                    sortedmap[ filename ] = dist
        
        sortedlist = sorted(sortedmap, key=lambda key: sortedmap[key])
        sortedlist = sortedlist[:4]
        for filename in sortedlist:
            imagedata[i][4].append( filename )

    #pathfile.write( image )
    #pathfile.write( '\t' )
    #pathfile.write( k )
    #pathfile.write( ':' )
    #pathfile.write( '\n' )

    # And backwards...
    for i in reversed(xrange(len(imagedata))):
        sortedmap = {}
        for j in reversed(xrange(0, i)):
            pos1 = imagedata[i][3]
            pos2 = imagedata[j][3]
            alt1 = imagedata[i][2][2]
            alt2 = imagedata[j][2][2]
            maxalt = max(alt1,alt2)

            if ( pos1[0] == -1 or pos2[0] == -1 ):
                print "Don't know case %s and %s, just adding it"%(imagedata[i][0], imagedata[j][0])
                # possible match.. append j
                path, filename = os.path.split(imagedata[j][0] )
                filename, ext = os.path.splitext(filename)
                # imagedata[i][4].append( filename )
                sortedmap[ filename ] = MAXDIST
            else:
                dist = calculate_distance( pos1, pos2 )

                if (( dist < maxalt ) or ( dist > MAXDIST )):
                    path, filename = os.path.split(imagedata[j][0] )
                    filename, ext = os.path.splitext(filename)
                    # imagedata[i][4].append( filename )
                    sortedmap[ filename ] = MAXDIST

        sortedlist = sorted(sortedmap, key=lambda key: sortedmap[key])
        sortedlist = sortedlist[:4]
        for filename in sortedlist:
            imagedata[i][4].append( filename )

    for i in xrange(len(imagedata)):
        if ( len(imagedata[i][4]) == 1 ):
            # no matches with any other.. Attempt match with all
            print "%s had no matches, so letting verify with all in retrospect, except itself."%(imagedata[i][0])
            for j in xrange(1,len(imagedata)):
                if ( imagedata[i][0] == imagedata[j][0] ):
                    continue
                path, filename = os.path.split(imagedata[j][0] )
                filename, ext = os.path.splitext(filename)
                imagedata[i][4].append( filename )

    # Write instructions
    for i in xrange(len(imagedata)):
        filename = imagedata[i][0]
        instructionfile = os.path.join(args.dir, filename+'.instructions')
        f = open( instructionfile, 'w' )
        for j in xrange(len(imagedata[i][4])):
            f.write( '%s %s\n'%(filename, imagedata[i][4][j]) )
        f.close()
	print "Written: %s"%(instructionfile)
    
    # Write K
    for i in xrange(len(imagedata)):
        filename = imagedata[i][0]
        camfile = os.path.join(args.dir, filename+'.cam')
        f = open(camfile, "wb")
        cam = cameraintrinsics_pb2.CameraIntrinsics()
        cam.imageName1 = filename
        cam.fx = imagedata[i][1][0]
        cam.fy = imagedata[i][1][0]
        cam.width = imagedata[i][1][1] 
        cam.height = imagedata[i][1][2] 
        cam.cx = (0.5 * imagedata[i][1][1]) - 0.5
        cam.cy = (0.5 * imagedata[i][1][2]) - 0.5
        cam.k1 = -0.075837789917569245
        cam.k2 = 0.067434478711580731

        pos = imagedata[i][3]
        if ( pos[0] != -1 ):
            cam.posx = pos[0]
            cam.posy = pos[1]
            cam.alt = imagedata[i][2][2]
        else:
            cam.posx = -1
            cam.posy = -1
            cam.alt = 0.0

        f.write(cam.SerializeToString())
        f.close()


