#!/bin/bash

export LIBJARS=/home/gt/cloudsift/hadoop-2.3.0/lib/BoofCV.jar,/home/gt/cloudsift/hadoop-2.3.0/lib/GeoRegression.jar,/home/gt/cloudsift/hadoop-2.3.0/lib/DDogleg.jar,/home/gt/cloudsift/hadoop-2.3.0/lib/EJML.jar,/home/gt/cloudsift/hadoop-2.3.0/lib/xstream-1.4.4.jar

bin/hdfs dfs -rm -r output/epipolars
bin/hadoop jar /home/gt/cloudsfm/code/epipolar/target/epipolar-1.0-SNAPSHOT.jar -libjars ${LIBJARS} true output/matches output/epipolars 

