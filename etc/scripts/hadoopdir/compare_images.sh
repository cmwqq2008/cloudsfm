#!/bin/bash

FILE=""
LOCALDIR="/tmp/hadoop-images"
REMOTEDIR="input/images/*"
REMOTEMATCHDIR="input/matchinstructions"
REMOTECAMDIR="input/cameras"
REMOTENVIEWDIR="input/nview"

HADOOPDIR="/home/gt/cloudsift/hadoop-2.3.0"

# init
# look for empty dir 
if [ -d "$LOCALDIR" ]; then
    echo "There is a local dir"
    if [ "$(ls -A $LOCALDIR)" ]; then
        echo "$LOCALDIR is not empty."

        read -p "Do you wish to continue on what's in there? (y/n)" yn
        case $yn in
            [Yy]* ) ;;
            [Nn]* ) rm -rf $LOCALDIR; mkdir -p $LOCALDIR; $HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEDIR $LOCALDIR;;
            * ) echo "Please answer y or n.";;
        esac
    else
      echo "Local dir is empty"
      rm -rf $LOCALDIR
      mkdir -p $LOCALDIR
      $HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEDIR $LOCALDIR
    fi
else
    mkdir -p $LOCALDIR
    $HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEDIR $LOCALDIR
fi

$HADOOPDIR/bin/hdfs dfs -rm -r $REMOTEMATCHDIR
$HADOOPDIR/bin/hdfs dfs -rm -r $REMOTECAMDIR
$HADOOPDIR/bin/hdfs dfs -rm -r $REMOTENVIEWDIR
$HADOOPDIR/bin/hdfs dfs -mkdir $REMOTEMATCHDIR
$HADOOPDIR/bin/hdfs dfs -mkdir $REMOTECAMDIR
$HADOOPDIR/bin/hdfs dfs -mkdir $REMOTENVIEWDIR

python compare_exif.py $LOCALDIR

$HADOOPDIR/bin/hdfs dfs -copyFromLocal $LOCALDIR/*.instructions $REMOTEMATCHDIR
$HADOOPDIR/bin/hdfs dfs -copyFromLocal $LOCALDIR/*.cam $REMOTECAMDIR
$HADOOPDIR/bin/hdfs dfs -copyFromLocal $LOCALDIR/path.txt $REMOTENVIEWDIR

