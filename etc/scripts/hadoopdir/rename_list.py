#!/usr/bin/python

import os
import glob
import argparse

VERSION = "geotagger 0.1"
DESCRIPTION = """\
Python convenience module to rename a list of files."""

def rename_files( pmvsdir, filetype ):
    images = sorted(glob.glob(os.path.join(pmvsdir,filetype)))
    for image in images:
        # IMG_1391_geotag.jpg-m-00000.jpg
        names = image.split("-m-")
        os.rename( image, names[0] )

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--version', action='version', version=VERSION)
    parser.add_argument('dir', type=str, action='store', help="Location of images" )
    args = parser.parse_args()

    # Figure out which images we have
    images = rename_files( "%svisualize"%(args.dir), "*.jpg" )
    images = rename_files( "%stxt"%(args.dir), "*.txt" )

    renamefile = open( "%srename.txt-m-00000.txt"%(args.dir), "r" )
    listfile = open( "%slist.txt"%(args.dir), "w" )
    for line in renamefile:
        line = line.strip()
        files = line.split("\t")
        os.rename( "%s%s"%(args.dir,files[0]), "%s%s"%(args.dir, files[1]) )
        listfile.write( "%s\n"%( files[1] ) )

    listfile.close()

