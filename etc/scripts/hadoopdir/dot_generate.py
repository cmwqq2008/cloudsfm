#!/usr/bin/python

import argparse
import copy
import itertools
import cameraintrinsics_pb2

VERSION = "dot_generator 0.1"
DESCRIPTION = """\
Makes a dot file of the edge matches"""

def permutationG(input, path, total, average):
    for s in xrange(len(input)):
        key = None
        highest = 0
        for k in path:
            try:
                v = edges[ k ]
            except KeyError as e:
                continue

	        # this node is on the path, so creates reachables
            if len(v) == 0:
                continue
            for k2,v2 in v:
                if k2 in path:
                    continue
                if v2 > highest:
                    highest = v2
                    key = k2

        if key == None:
            return path, total

        path.append( key )
        total += highest

    return path, total

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--version', action='version', version=VERSION)
    parser.add_argument('file', type=str, action='store', help="Location of edge file" )
    parser.add_argument('dotfile', type=str, action='store', help="Location of dot file" )
    parser.add_argument('pathfile', type=str, action='store', help="Location of dot file" )
    parser.add_argument('camdir', type=str, action='store', help="Location of cam files" )
    args = parser.parse_args()

    edgefile = open( args.file, 'r' )
    dotfile = open( args.dotfile, 'w' )
    pathfile = open( args.pathfile, 'w' )    

    dotfile.write( 'digraph {\n' )

    bestpath = []
    path = []
    highs = {}
    edges = {}
    best = []
    imagecount = {}

    for line in edgefile:
        line = line.strip()
        imagenums = line.split( '\t' )
        (image1, image2) = imagenums[0].split(':')
        (matches, inliers) = imagenums[1].split(':')
        
        inliers = int(inliers)

        try:
            s = imagecount[ image1 ]
        except KeyError, e:
            imagecount[ image1 ] = "s"
        try:
            s = imagecount[ image2 ]
        except KeyError, e:
            imagecount[ image2 ] = "s"

        # Record highest inliers for each image
        t = 0
        try:
            t = highs[ image1 ]
        except KeyError, e:
            best.append( image1 )

        if inliers > t:
            t = inliers

        highs[ image1 ] = int(t) 

        # record all edges with inlier.size()
        t = []
        try:
            t = edges[ image1 ]
        except KeyError, e:
            pass

        t.append( ( image2, inliers ) )            
        edges[ image1 ] = t

        dotfile.write( "%s -> %s [label=\"%s/%s\"];\n"%(image1, image2, matches, inliers) )

    # Find best 30% to start from.
    numedges = len(edges)
    numbest = max( 1, numedges * 0.3 )

    while ( len(best) > numbest ):
        lowest = 99999999
        lowestname = None
        for imagename in best:
            if highs[ imagename ] < lowest:
                lowest = highs[ imagename ]
                lowestname = imagename

        best.remove( lowestname )

    dotfile.write( '}\n' )
    dotfile.close()

    print numbest, best

    bestpath = []
    bestval = 0
    permutation = []
    for myimg in edges.iterkeys():
        permutation.append( myimg )

    for image in best:
        path, val = permutationG( permutation, [image], 0, 0 )
        if val > bestval:
            bestval = val
            bestpath = copy.deepcopy( path )

    pathfile.write( "__NUM_IMAGES__" )
    pathfile.write( '\t' )
    pathfile.write( '%d'%(len(imagecount)) )
    pathfile.write( '\n' )

    print "bestpath: ", bestpath

    newpath = []
    first = True
    numImages = 0
    camOrder = []

    for image in bestpath:
        newpath.append( image )
        camOrder.append( image + '.cam' )

        if first:
            first = False
            continue

        numImages += 1
        pathfile.write( image )
        pathfile.write( '\t' )

        for k in newpath:
            try:
                v = edges[ k ]
            except KeyError as e:
                continue

	        # this node is on the path, so creates reachables
            if len(v) == 0:
                continue
            for k2,v2 in v:
                if k2 == image:
                    pathfile.write( k )
                    pathfile.write( ':' )

        pathfile.write( '\n' )

    pathfile.flush()
    pathfile.close()

    print "Written %d images to nview"%(numImages)

    camdata = []
    total = 0
    for i in xrange(len(camOrder)):
        cam = cameraintrinsics_pb2.CameraIntrinsics()
        camfile = open( args.camdir + "/" + camOrder[ i ], 'r' )
        cam.ParseFromString( camfile.read() )
        camdata.append( cam )
        total = total + 1

    # Calculate pcd
    f = open( '/tmp/pmvs/cloud1.pcd', 'w' )
    f.write( "# .PCD v.7 - Point Cloud Data file format\n" )
    f.write( "VERSION .7\n" )
    f.write( "FIELDS x y z\n" )
    f.write( "SIZE 4 4 4\n" )
    f.write( "TYPE F F F\n" )
    f.write( "COUNT 1 1 1\n" )
    f.write( "WIDTH %d\n"%(total) )
    f.write( "HEIGHT 1\n" )
    f.write( "VIEWPOINT 0 0 0 1 0 0 0\n" )
    f.write( "POINTS %d\n"%(total) )
    f.write( "DATA ascii\n" )

    for i in xrange(len(camOrder)):
        cam = camdata[i]
        f.write( "%f %f %f\n"%( cam.posx, cam.posy, cam.alt ) )


