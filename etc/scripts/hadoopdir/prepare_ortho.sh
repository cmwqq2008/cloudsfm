#!/bin/bash

FILE=""
LOCALDIR="/tmp/ortho"
LOCALIMAGEDIR="/tmp/ortho/images"
LOCALCAMDIR="/tmp/ortho/cams"
REMOTEEPIDIR="output/epipolars"
REMOTEPATH="input/nview/path.txt"
REMOTEIMAGEPATH="output/reconstructions/bundler/visualize/*.jpg"
REMOTECAMPATH="output/reconstructions/*.bin"
HADOOPDIR="/home/gt/cloudsift/hadoop-2.3.0"

rm -rf $LOCALDIR
mkdir -p $LOCALIMAGEDIR
mkdir -p $LOCALCAMDIR

$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEEPIDIR/*.bin $LOCALDIR 
$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEPATH $LOCALDIR 
$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEIMAGEPATH $LOCALIMAGEDIR
$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTECAMPATH $LOCALCAMDIR


