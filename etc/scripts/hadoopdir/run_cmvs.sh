#!/bin/bash

FILE=""
LOCALDIR="/tmp/pmvs"
LOCALPCLDIR="/tmp/pmvs"
REMOTEPATH="output/reconstructions/bundler/"
REMOTEPCLPATH="output/reconstructions/cloud*"
HADOOPDIR="/home/gt/cloudsift/hadoop-2.3.0"

rm -rf $LOCALDIR
mkdir -p $LOCALDIR

$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEPATH $LOCALDIR
$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTEPCLPATH $LOCALPCLDIR

mkdir -p $LOCALDIR/bundler/models

# ---------------

mv $LOCALDIR/bundler/bundler.out-m-00000.txt $LOCALDIR/bundler/bundle.rd.out
mv $LOCALPCLDIR/cloud2.pcd-m-00000.txt $LOCALPCLDIR/cloud2.pcd

python rename_list.py $LOCALDIR/bundler/

CMVS_DIR=/home/gt/cloudsift/RunSFM/cmvs/program/main
$CMVS_DIR/cmvs /tmp/pmvs/bundler/
$CMVS_DIR/genOption /tmp/pmvs/bundler/ 1 2 0.7 7 3 4
#$CMVS_DIR/genOption /tmp/pmvs/ 
$CMVS_DIR/pmvs2 /tmp/pmvs/bundler/ option-0000

