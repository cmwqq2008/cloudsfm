#!/bin/bash

FILE=""
LOCALDIR="/tmp/hadoop-dot"
REMOTEDIR="output/epipolars"
REMOTENVIEWDIR="input/nview"
REMOTECAMDIR="input/cameras"
HADOOPDIR="/home/gt/cloudsift/hadoop-2.3.0"

rm -rf $LOCALDIR
mkdir -p $LOCALDIR
$HADOOPDIR/bin/hdfs dfs -cat $REMOTEDIR/edges* > $LOCALDIR/edges.txt
$HADOOPDIR/bin/hdfs dfs -copyToLocal $REMOTECAMDIR/*.cam $LOCALDIR
python dot_generate.py $LOCALDIR/edges.txt $LOCALDIR/edges.dot $LOCALDIR/path.txt $LOCALDIR

# dot -Tpng -o$LOCALDIR/edges.png $LOCALDIR/edges.dot

$HADOOPDIR/bin/hdfs dfs -rm -r $REMOTENVIEWDIR
$HADOOPDIR/bin/hdfs dfs -mkdir $REMOTENVIEWDIR

$HADOOPDIR/bin/hdfs dfs -copyFromLocal $LOCALDIR/path.txt $REMOTENVIEWDIR

