package br.com.cridea;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import br.com.cridea.DescriptorProto.Descriptors;
import br.com.cridea.KeyProto.Keys;
import br.com.cridea.KeyProto.Keys.Key;
import br.com.cridea.MatchesProto.Matches;

public class MatcherMapper extends Mapper<LongWritable, Text, Text, BytesWritable> {
	
	private MultipleOutputs<Text, BytesWritable> mos;
	private byte[] myDescriptors;
	private Keys myFrames;
	
	private double threshold = 2.3;
	private NativeMatcher matcher;
	private Configuration conf;
	private String lastDescriptor;
	private String inputDir;
	private static final int NUM_FEATURES = 128;
	private static final int MIN_MATCHES = 28; 
	
	static enum Matcher { NUMMATCHES, NUMATTEMPTS, IMAGESMATCHING, IMAGESNOTMATCHING }
	
	@Override
	protected void setup(Context context)
		throws IOException, InterruptedException 
	{
		conf = context.getConfiguration();
		String descriptor = context.getConfiguration().get( "matcher.threshold" );
		if ( descriptor == null ) {
			throw new IOException( "matcher.threshold not set" );
		}
		System.out.println( "Matcher threshold is: " + descriptor );

		threshold = Double.valueOf( context.getConfiguration().get( "matcher.threshold" ) ).doubleValue();	
		matcher = new NativeMatcher();
		
		inputDir = conf.get("matcher.frames.dir");
		if ( inputDir == null ) {
			throw new IOException( "matcher.frames.dir not set" );
		}
		
	    mos = new MultipleOutputs(context);
	}
	
	public void map(LongWritable key, Text value, Context context) 
		throws IOException, InterruptedException 
	{
		String realPath = null;
		Descriptors temp = null;
		Keys otherFrames = null;
		InputStream in = null;
		
		String[] imageNames = value.toString().split(" ");
		
		if ( imageNames[0].equals( imageNames[1])) {
			// Do not compare images against themselves
			return;
		}
		
		if (( lastDescriptor == null ) || (! lastDescriptor.equals( imageNames[0] ))) {
			realPath = generatePath( "descriptors", imageNames[0] );
	        in = openBinaryFile( conf, realPath );
			temp = Descriptors.parseFrom( in );
			in.close();
			myDescriptors = new byte[ temp.getDescriptorCount() * NUM_FEATURES ];
			for ( int i = 0; i < temp.getDescriptorCount(); i++ ) {
		        System.arraycopy( temp.getDescriptor(i).getData().toByteArray(), 0, myDescriptors, i * NUM_FEATURES, NUM_FEATURES );
			}
			lastDescriptor = imageNames[0];
			myFrames = null;
		}

		realPath = generatePath( "descriptors", imageNames[1] );
        in = openBinaryFile( conf, realPath );
        temp = Descriptors.parseFrom( in );
        in.close();
		byte[] otherDescriptor = new byte[ temp.getDescriptorCount() * NUM_FEATURES ];
		for ( int i = 0; i < temp.getDescriptorCount(); i++ ) {
	        System.arraycopy( temp.getDescriptor(i).getData().toByteArray(), 0, otherDescriptor, i * NUM_FEATURES, NUM_FEATURES );
		}
		temp = null;
		
		/** Running the matching algorithm **/
    	System.out.println("Testing " + imageNames[0] + " against file: " + imageNames[1] );
		context.getCounter(Matcher.NUMATTEMPTS).increment( 1 );
    	System.out.println("Using native method");
		int[] matches = matcher.runMatch( myDescriptors, otherDescriptor, threshold );
		
		Map<Integer,Integer> uniqueMatch1 = new HashMap<Integer,Integer>();
		Map<Integer,Integer> uniqueMatch2 = new HashMap<Integer,Integer>();
		for ( int i = 0; i < matches.length; i+=2 ) {
        	Integer num = uniqueMatch1.get( new Integer( matches[ i ] ));
        	if  ( num == null ) { 
        		num = new Integer( 1 );
        	} else {
        		num = new Integer( num.intValue() + 1 );
        	}
        	uniqueMatch1.put( new Integer( matches[ i ] ), num);
        	num = uniqueMatch2.get( new Integer( matches[ i + 1 ] ));
        	if  ( num == null ) { 
        		num = new Integer( 1 );
        	} else {
        		num = new Integer( num.intValue() + 1 );
        	}
        	uniqueMatch2.put( new Integer( matches[ i + 1 ] ), num);
		}
		
		if ( matches.length > MIN_MATCHES ) {
			otherDescriptor = null;
			if ( myFrames == null ) {
				realPath = generatePath( "frames", imageNames[0] );
		        in = openBinaryFile( conf, realPath );
		        myFrames = Keys.parseFrom( in );
				in.close();
			}
			realPath = generatePath( "frames", imageNames[1] );
	        in = openBinaryFile( conf, realPath );
	        otherFrames = Keys.parseFrom( in );
			in.close();
		
	        br.com.cridea.MatchesProto.Matches.Builder matchesBuilder = Matches.newBuilder();
	        matchesBuilder.setImageName1( imageNames[0] );
	        matchesBuilder.setImageName2( imageNames[1] );
			
	        int matched = 0;
			for ( int i = 0; i < matches.length; i+=2 ) {
	        	br.com.cridea.MatchesProto.Matches.Match.Builder matchBuilder = 
	            		br.com.cridea.MatchesProto.Matches.Match.newBuilder();
	        	
        		matched = uniqueMatch1.get(new Integer( matches[ i ] ));
        		if ( matched > 1 ) {
        			// do not add poor matches.
        			continue;
        		}
        		matched = uniqueMatch2.get(new Integer( matches[ i+1 ] ));
        		if ( matched > 1 ) {
        			// do not add poor matches.
        			continue;
        		}
	        	Key myKey = myFrames.getKey( matches[ i ] );
	        	Key otherKey = otherFrames.getKey( matches[ i + 1 ] );
	        	
	        	if (( myKey.getX() < 4 ) || ( myKey.getX() > 3996 ) || ( myKey.getY() < 4 ) || ( myKey.getY() > 2996 )) {
	        		System.out.println("Weird. Still invalid point in matcher: " + 
	        				myKey.getX() + " " + myKey.getY() );
	        	}
	        	
	        	matchBuilder.setX1( myKey.getX() );
	        	matchBuilder.setY1( myKey.getY() );
	        	matchBuilder.setX2( otherKey.getX() );
	        	matchBuilder.setY2( otherKey.getY() );
	        	matchBuilder.setIdx1( matches[ i ] );
	        	matchBuilder.setIdx2( matches[ i + 1 ] );
	        	matchesBuilder.addMatch( matchBuilder.build() );
			}
	        Matches matchesObj = matchesBuilder.build();
	        
	        if ( matchesObj.getMatchCount() < MIN_MATCHES ) {
	        	context.getCounter(Matcher.IMAGESNOTMATCHING).increment( 1 );
	        	return;
	        }

			mos.write("matches", new Text(imageNames[0]), new BytesWritable( matchesObj.toByteArray() ), imageNames[0] );

	        context.getCounter(Matcher.NUMMATCHES).increment( matchesObj.getMatchCount() );
			context.getCounter(Matcher.IMAGESMATCHING).increment( 1 );
		} else {
			context.getCounter(Matcher.IMAGESNOTMATCHING).increment( 1 );
		}
	}
	
	private InputStream openBinaryFile( Configuration conf, String filename ) 
		throws IOException 
	{
        final Path file = new Path( filename );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream filein = fs.open(file);	
        return filein;
	}
	
	private String generatePath( String subdir, String filename ) {
		return inputDir + File.separator + subdir + File.separator + filename + "-r-00000.bin";
	}
	
	@Override
	protected void cleanup(org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		super.cleanup(context);
        mos.close();
	}
}

