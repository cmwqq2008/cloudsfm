package br.com.cridea;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Matcher extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		int res = ToolRunner.run(conf, new Matcher(), args);
		System.exit(res);
	}
	
	public final int run(final String[] args) throws Exception {
		Configuration conf = getConf();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

		if (otherArgs.length != 4) {
			System.err.println("Usage: matcher <matcher-threshold> <frames-dir> <in> <out>");
			System.exit(2);
		}

	    System.out.println("Setting matcher.threshold to " + otherArgs[0] );
	    System.out.println("Setting matcher.frames.dir to " + otherArgs[1] );

	    // set these *before* they're inserted into the job.
	    // makes a copy?
	    conf.set( "matcher.threshold", otherArgs[0] );
	    conf.set("matcher.frames.dir", otherArgs[1] );

	    Job job = new Job(conf, "matcher");
	    job.setJarByClass(Matcher.class);

	    job.setMapperClass(MatcherMapper.class);
//	    job.setCombinerClass(MatcherReducer.class);
//	    job.setReducerClass(MatcherReducer.class);
    	job.setNumReduceTasks( 0 );
    	
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(BytesWritable.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(BytesWritable.class);
	    job.setOutputFormatClass(MatcherOutputFormat.class);

	    LazyOutputFormat.setOutputFormatClass(job, MatcherOutputFormat.class); 
	    MultipleOutputs.addNamedOutput(job, "matches", MatcherOutputFormat.class, Text.class, BytesWritable.class);

	    FileInputFormat.addInputPath(job, new Path(otherArgs[2]) );
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[3]));
	    if (job.waitForCompletion(true)) { 
	    	return 0;
	    } else {
	    	return -1;
	    }
	}
}
