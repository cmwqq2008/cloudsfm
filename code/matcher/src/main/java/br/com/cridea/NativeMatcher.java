package br.com.cridea;

public class NativeMatcher {
	static {
		System.loadLibrary("cloudsfm");
	}
	
	public native int[] runMatch( byte[] d1, byte[] d2, double threshold );
}
