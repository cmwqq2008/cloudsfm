/** @internal
 ** @file     sift.c
 ** @author   Andrea Vedaldi
 ** @brief    Scale Invariant Feature Transform (SIFT) - Driver
 **/

/*
Copyright (C) 2007-12 Andrea Vedaldi and Brian Fulkerson.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#define VL_SIFT_DRIVER_VERSION 0.1

#include <vl/generic-driver.h>
#include <vl/generic.h>
#include <vl/stringop.h>
#include <vl/pgm.h>
#include <vl/sift.h>
#include <vl/getopt_long.h>
#include <jpeglib.h>
#include <jerror.h>

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <algorithm>
#include <limits>
#include <string>
#include <vector>
#include <flann/flann.hpp>
 
#include  "stdint.h"  // <--- to prevent uint64_t errors!

#include "br_com_cridea_NativeSifter.h"
#include "br_com_cridea_NativeMatcher.h"
#include "br_com_cridea_NativeSBA.h"

#define V3DLIB_ENABLE_SUITESPARSE

#include <Math/v3d_linear.h>
#include <Base/v3d_vrmlio.h>
#include <Geometry/v3d_metricbundle.h>

using namespace V3D;
using namespace std;

/* unit quaternion from vector part */
#define _MK_QUAT_FRM_VEC(q, v){                                     \
  (q)[1]=(v)[0]; (q)[2]=(v)[1]; (q)[3]=(v)[2];                      \
  (q)[0]=sqrt(1.0 - (q)[1]*(q)[1] - (q)[2]*(q)[2]- (q)[3]*(q)[3]);  \
}

/* ----------------------------------------------------------------- */
/** @brief Keypoint ordering
 ** @internal
 **/
int
korder (void const* a, void const* b) {
  double x = ((double*) a) [2] - ((double*) b) [2] ;
  if (x < 0) return -1 ;
  if (x > 0) return +1 ;
  return 0 ;
}

inline void transpose_descriptor (vl_sift_pix* dst, vl_sift_pix* src)
{
	int const BO = 8 ;  /* number of orientation bins */
	int const BP = 4 ;  /* number of spatial bins     */
	int i, j, t ;

	for (j = 0 ; j < BP ; ++j) {
		int jp = BP - 1 - j ;
		for (i = 0 ; i < BP ; ++i) {
			int o  = BO * i + BP*BO * j  ;
			int op = BO * i + BP*BO * jp ;
			dst [op] = src[o] ;
			for (t = 1 ; t < BO ; ++t)
				dst [BO - t + op] = src [t + o] ;
		}
	}
}

static void write_double( string& data, double x );

/* ---------------------------------------------------------------- */
/** @brief SIFT driver entry point
 **/
/*
 * Class:     br_com_cridea_NativeSifter
 * Method:    runSift
 * Signature: ([BII)[[D
 */
JNIEXPORT jobjectArray JNICALL Java_br_com_cridea_NativeSifter_runSift
  (JNIEnv *env, jobject obj, jbyteArray image, jint width, jint height)
{
  /* algorithm parameters */
  double   edge_thresh  = 8 ;
  double   peak_thresh  = 1 ;
  double   norm_thresh = 0;
  double   magnif       = -1 ;
  double   window_size = -1;
  int      O = 4, S = 5, omin = 0;
	vl_bool            force_orientations = 0 ;
	vl_bool            floatDescriptors = 0 ;

      VlSiftKeypoint const *keys = 0 ;

  vl_bool  err    = VL_ERR_OK ;
  char     err_msg [1024] ;
  int      n ;
  int      exit_code          = 0 ;

    jsize len = env->GetArrayLength(image);
    jbyte *data = env->GetByteArrayElements(image, 0);

    #define ERRF(msg, arg) {                                        \
        err = VL_ERR_BAD_ARG ;                                      \
        snprintf(err_msg, sizeof(err_msg), msg, arg) ;              \
        break ;                                                     \
      }

    #define ERR(msg) {                                              \
        err = VL_ERR_BAD_ARG ;                                      \
        snprintf(err_msg, sizeof(err_msg), msg) ;                   \
        break ;                                                     \
    }

      /* -----------------------------------------------------------------
       *                                                     Parse options
       * -------------------------------------------------------------- */

      /*
         if --output is not specified, specifying --frames or --descriptors
         prevent the aggregate outout file to be produced.
      */

    #define PRNFO(name,fm)                                                  \
        printf("sift: " name) ;                                             \
        printf("%3s ",  (fm).active ? "yes" : "no") ;                       \
        printf("%-6s ", vl_string_protocol_name ((fm).protocol)) ;          \
        printf("%-10s\n", (fm).pattern) ;

      /* ------------------------------------------------------------------
       *                                         Process one image per time
       * --------------------------------------------------------------- */
    char             basename [1024] ;

    vl_sift_pix     *fdata = 0 ;

    VlSiftFilt      *filt = 0 ;
    vl_size          q ;
    int              i ;
    vl_bool          first ;
    unsigned int    npixels;

    double           *ikeys = 0 ;
    int              nikeys = 0, ikeys_size = 0 ;

    /* ...............................................................
     *                                                       Read data
     * ............................................................ */

    npixels = width * height;

    printf ("sift: image is %d by %d pixels\n", width, height) ;

    fdata = (vl_sift_pix *)malloc(npixels * sizeof (vl_sift_pix)) ;

    if (!fdata) {
       err = VL_ERR_ALLOC ;
       snprintf(err_msg, sizeof(err_msg), "Could not allocate enough memory.") ;
    }

    /* convert data type */
    for (q = 0 ; q < len; ++q) {
        fdata [q] = (unsigned char)data [q] ;
        if ( fdata[q] < 0 ) {
            fprintf( stderr, "data < 0: oops! %f\n", fdata[q] );
        }
    }

    env->ReleaseByteArrayElements( image, data, 0 );

    /* ...............................................................
     *                                     Optionally source keypoints
     * ............................................................ */

    #define WERR(inputfile,op)                                           \
    if (err == VL_ERR_OVERFLOW) {                               \
      snprintf(err_msg, sizeof(err_msg),                        \
               "Output file name too long.") ;                  \
                                                   \
    } else if (err) {                                           \
      snprintf(err_msg, sizeof(err_msg),                        \
               "Could not open '%s' for " #op, inputfile) ;          \
                                                  \
    }

    /* ...............................................................
     *                                               Open output files
     * ............................................................ */

    /* ...............................................................
     *                                                     Make filter
     * ............................................................ */

    filt = vl_sift_new (width, height, O, S, omin) ;

    if (edge_thresh >= 0) vl_sift_set_edge_thresh (filt, edge_thresh) ;
    if (peak_thresh >= 0) vl_sift_set_peak_thresh (filt, peak_thresh) ;
    if (magnif      >= 0) vl_sift_set_magnif      (filt, magnif) ;
    if (window_size >= 0) vl_sift_set_window_size (filt, window_size) ;
    if (norm_thresh >= 0) vl_sift_set_norm_thresh (filt, norm_thresh) ;

    if (!filt) {
      snprintf (err_msg, sizeof(err_msg),
                "Could not create SIFT filter.") ;
      err = VL_ERR_ALLOC ;
    }

      printf ("sift: filter settings:\n") ;
      printf ("sift:   octaves      (O)     = %d\n",
              vl_sift_get_noctaves     (filt)) ;
      printf ("sift:   levels       (S)     = %d\n",
              vl_sift_get_nlevels      (filt)) ;
      printf ("sift:   first octave (o_min) = %d\n",
              vl_sift_get_octave_first (filt)) ;
      printf ("sift:   edge thresh           = %g\n",
              vl_sift_get_edge_thresh  (filt)) ;
      printf ("sift:   peak thresh           = %g\n",
              vl_sift_get_peak_thresh  (filt)) ;
      printf("vl_sift:   norm thresh           = %g\n",
	          vl_sift_get_norm_thresh   (filt)) ;
      printf ("sift:   magnif                = %g\n",
              vl_sift_get_magnif       (filt)) ;
      printf ("sift: will source frames? %s\n",
              ikeys ? "yes" : "no") ;
	  printf("vl_sift:   window size           = %g\n",
		      vl_sift_get_window_size   (filt)) ;
	  printf("vl_sift:   float descriptor      = %d\n",
		floatDescriptors) ;
      printf("vl_sift: will force orientations? %s\n",
		force_orientations ? "yes" : "no") ;
    /* ...............................................................
     *                                             Process each octave
     * ............................................................ */
    i     = 0 ;
    first = 1 ;

    std::vector<double> frames;
    std::vector<double> descriptors;

    while (1) {
      int                   nkeys ;

      /* calculate the GSS for the next octave .................... */
      if (first) {
        first = 0 ;
        err = vl_sift_process_first_octave (filt, fdata) ;
      } else {
        err = vl_sift_process_next_octave  (filt) ;
      }

      if (err) {
        err = VL_ERR_OK ;
        break ;
      }

      printf("sift: GSS octave %d computed\n", vl_sift_get_octave_index (filt));

      /* run detector ............................................. */
        vl_sift_detect (filt) ;

        keys  = vl_sift_get_keypoints     (filt) ;
        nkeys = vl_sift_get_nkeypoints (filt) ;
        i     = 0 ;

        printf ("sift: detected %d (unoriented) keypoints\n", nkeys) ;

      /* for each keypoint ........................................ */
      for (; i < nkeys ; ++i) {
        double                angles [4] ;
        int                   nangles ;
        VlSiftKeypoint        ik ;
        VlSiftKeypoint const *k ;

          k = keys + i ;
          nangles = vl_sift_calc_keypoint_orientations
            (filt, angles, k) ;

        /* for each orientation ................................... */
        for (q = 0 ; q < (unsigned) nangles ; ++q) {

          vl_sift_pix descr [128] ;
          vl_sift_pix rbuf [128] ;

          /* compute descriptor (if necessary) */
          vl_sift_calc_keypoint_descriptor
              (filt, descr, k, angles [q]) ;
          transpose_descriptor (rbuf, descr) ;

            int l ;
            for (l = 0 ; l < 128 ; ++l) {
              float x = 512.0 * rbuf[l] ;
              x = (x < 255.0) ? x : 255.0 ;
              descriptors.push_back( x );
            }

            frames.push_back( k->x );
            frames.push_back( k->y );
            frames.push_back( k->sigma );
            frames.push_back( angles[q] );

          //write_double( frames, k->x );
         // write_double( frames, k->y );
          //write_double( frames, k->sigma );
          //write_double( frames, angles[q] );
        }
      }
    }

    /* ...............................................................
     *                                                       Finish up
     * ............................................................ */

    /* release input keys buffer */
    if (ikeys) {
      free (ikeys) ;
      ikeys_size = nikeys = 0 ;
      ikeys = 0 ;
    }

    /* release filter */
    if (filt) {
      vl_sift_delete (filt) ;
      filt = 0 ;
    }

    /* release image data */
    if (fdata) {
      free (fdata) ;
      fdata = 0 ;
    }  

    printf( "sift: dumping %ld frames\n", frames.size() );
    printf( "sift: dumping %ld descriptors\n", descriptors.size() );

    jdoubleArray dFrames = env->NewDoubleArray(frames.size());
    env->SetDoubleArrayRegion( dFrames,0, frames.size(), &frames[0] );

    jdoubleArray dDescriptors = env->NewDoubleArray( descriptors.size() );
    env->SetDoubleArrayRegion( dDescriptors,0, descriptors.size(), &descriptors[0] );

    jclass byteArrCls = env->FindClass("[D");
    jobjectArray arrOfDoubleArrays = env->NewObjectArray( 2, byteArrCls, NULL);
    env->SetObjectArrayElement(arrOfDoubleArrays, 0, dFrames);
    env->SetObjectArrayElement(arrOfDoubleArrays, 1, dDescriptors);
    env->DeleteLocalRef( dFrames );
    env->DeleteLocalRef( dDescriptors );

    return arrOfDoubleArrays;
}

/*
 * Class:     br_com_cridea_NativeMatcher
 * Method:    runMatch
 * Signature: ([B[BD)[I
 */
JNIEXPORT jintArray JNICALL Java_br_com_cridea_NativeMatcher_runMatch
  (JNIEnv *env, jobject obj, jbyteArray d1, jbyteArray d2, jdouble threshold)
{
	const long maxval = 0x7fffffff ;
    const int ND = 128;

    jsize d1len = env->GetArrayLength(d1) / ND;
    jbyte *b1 = env->GetByteArrayElements(d1, 0);
    jsize d2len = env->GetArrayLength(d2) / ND;
    jbyte *b2 = env->GetByteArrayElements(d2, 0);

    vector<int> matches;

	int k1, k2 ;
    jbyte *p1;
    jbyte *p2;
    jbyte *saveptr;
    long best = maxval;
    long second_best = maxval;
    long bestk = -1;
    int bin = 0;
    long acc = 0;
    int delta = 0;

    saveptr = b2;
    p1 = b1;
    p2 = b2;

    printf( "matcher: Starting a loop now %d to %d\n", d1len, d2len );
    printf( "and threshold %f\n", threshold );
    fflush( stdout );

    int nn = 2;

    std::vector <float> data(d1len*128);
    for(int j=0; j < d1len; j++) {
        for(int k=0; k < 128; k++) {
            data[j*128 + k] = b1[j*128 + k];
        }
    }

    flann::Matrix<float> dataset(&data[0], d1len, 128);
    flann::Index<flann::L2<float> > index(dataset, flann::KDTreeIndexParams(4));
    index.buildIndex();

    std::vector <float> queries_data(d2len*128);
    for(int k=0; k < d2len; k++) {
        for(int l=0; l < 128; l++) {
            queries_data[k*128 + l] = b2[k*128 + l];
        }
    }

    std::vector <int> indices_data(d2len*nn);
    std::vector <float> dists_data(d2len*nn);

    flann::Matrix<float> queries(&queries_data[0], d2len, 128);
    flann::Matrix<int> indices(&indices_data[0], d2len, nn);
    flann::Matrix<float> dists(&dists_data[0], d2len, nn);

    index.knnSearch(queries, indices, dists, nn, flann::SearchParams(128));

    for(int k=0; k < d2len; k++) {
        float dist1 = dists_data[k*2];
        float dist2 = dists_data[k*2 + 1];

        if ( dist1*threshold < dist2 ) {
            matches.push_back( indices_data[k*2] );
            matches.push_back( k );
        }
    }

    jintArray dArray = env->NewIntArray( matches.size() );
    jint *inArray = env->GetIntArrayElements( dArray, NULL);
    for ( int i = 0; i < matches.size(); i++ ) {
        inArray[i] = matches[i];
    }
    env->ReleaseIntArrayElements( dArray, inArray, 0);

    return dArray;
}

namespace
{
inline void
showErrorStatistics(double const f0,
					StdDistortionFunction const& distortion,
					vector<CameraMatrix> const& cams,
					vector<Vector3d> const& Xs,
					vector<Vector2d> const& measurements,
					vector<int> const& correspondingView,
					vector<int> const& correspondingPoint)
{
	int const K = measurements.size();
	
	double meanReprojectionError = 0.0;
	for (int k = 0; k < K; ++k)
	{
		int const i = correspondingView[k];
		int const j = correspondingPoint[k];
		Vector2d p = cams[i].projectPoint(distortion, Xs[j]);
		
		double reprojectionError = norm_L2(f0 * (p - measurements[k]));
		meanReprojectionError += reprojectionError;
		
			/*cout << "i=" << i << " j=" << j << " k=" << k << "\n";
			 displayVector(Xs[j]);
			 displayVector(f0*p);
			 displayVector(f0*measurements[k]);
			 displayMatrix(cams[i].getRotation());
			 displayVector(cams[i].getTranslation());
			 cout << "##################### error = " << reprojectionError << "\n";
			 if(reprojectionError > 2)
                 cout << "!\n";
           */
	}
	cout << "mean reprojection error (in pixels): " << meanReprojectionError/K << endl;
}
}
/*
 * Class:     br_com_cridea_NativeSBA
 * Method:    runSBA
 * Signature: ([D[D[D[D[D)V
 */
JNIEXPORT void JNICALL Java_br_com_cridea_NativeSBA_runSBA
  (JNIEnv *env, jobject obj, jdoubleArray cams, jdoubleArray pts, jdoubleArray intrinsics, jdoubleArray projs, jdoubleArray distorts)
{
    jsize numcams = env->GetArrayLength(cams) / 12;
    jdouble *dcams = env->GetDoubleArrayElements(cams, 0);
    jsize ptslen = env->GetArrayLength(pts) / 3;
    jdouble *dpts = env->GetDoubleArrayElements(pts, 0);
    jdouble *dintrinsics = env->GetDoubleArrayElements(intrinsics, 0);
    jdouble *dprojs = env->GetDoubleArrayElements(projs, 0);
    jsize projlen = env->GetArrayLength(projs) / 4;

    // This array must always have some values, or it crashes when releasing it later...
    jdouble *ddistorts = env->GetDoubleArrayElements(distorts, 0);
    jsize distortlen = env->GetArrayLength(distorts);

    fprintf( stdout, "%d cams, %d points, %d projections, %d distort params\n", numcams, ptslen, projlen, distortlen );

    /** Conversions here **/
	int N = numcams, M = ptslen, K = projlen;

	cout << "N (cams) = " << N << " M (points) = " << M << " K (measurements) = " << K << endl;
	
	StdDistortionFunction distortion;
	
    distortion.k1 = ddistorts[0];
    distortion.k2 = ddistorts[1];
    distortion.p1 = ddistorts[2];
    distortion.p2 = ddistorts[3];

    //conver camera intrinsics to BA datastructs
    Matrix3x3d KMat;
    makeIdentityMatrix(KMat);
    KMat[0][0] = dintrinsics[0]; //fx
    KMat[1][1] = dintrinsics[4]; //fy
    KMat[0][1] = dintrinsics[1]; //skew
    KMat[0][2] = dintrinsics[2]; //ppx
    KMat[1][2] = dintrinsics[5]; //ppy

    double const f0 = KMat[0][0];
    cout << "intrinsic before bundle = "; displayMatrix(KMat);
    Matrix3x3d Knorm = KMat;
    // Normalize the intrinsic to have unit focal length.
    scaleMatrixIP(1.0/f0, Knorm);
    Knorm[2][2] = 1.0;

	vector<int> pointIdFwdMap(M);
	map<int, int> pointIdBwdMap;
	
	//conver 3D point cloud to BA datastructs
	vector<Vector3d > Xs(M);
	for (int j = 0; j < M; ++j) {
		int pointId = j;
		Xs[j][0] = dpts[j*3];
		Xs[j][1] = dpts[j*3+1];
		Xs[j][2] = dpts[j*3+2];
		pointIdFwdMap[j] = pointId;
		pointIdBwdMap.insert(make_pair(pointId, j));
	}
	cout << "Read the 3D points." << endl;
	
	vector<int> camIdFwdMap(N,-1);
	map<int, int> camIdBwdMap;
	
	//convert cameras to BA datastructs
	vector<CameraMatrix> mycams(N);
	for (int i = 0; i < N; ++i) {
		int camId = i;
		Matrix3x3d R;
		Vector3d T;
		
		//Matx34d& P = Pmats[i];
		
		R[0][0] = dcams[i*12]; R[0][1] = dcams[i*12+1]; R[0][2] = dcams[i*12+2]; T[0] = dcams[i*12+3];
		R[1][0] = dcams[i*12+4]; R[1][1] = dcams[i*12+5]; R[1][2] = dcams[i*12+6]; T[1] = dcams[i*12+7];
		R[2][0] = dcams[i*12+8]; R[2][1] = dcams[i*12+9]; R[2][2] = dcams[i*12+10]; T[2] = dcams[i*12+11];
		
		camIdFwdMap[i] = camId;
		camIdBwdMap.insert(make_pair(camId, i));
		
		mycams[i].setRotation(R);
		mycams[i].setTranslation(T);
		mycams[i].setIntrinsic(Knorm);
	}
	cout << "Read the cameras." << endl;

	vector<Vector2d > measurements;
	vector<int> correspondingView;
	vector<int> correspondingPoint;
	
	measurements.reserve(K);
	correspondingView.reserve(K);
	correspondingPoint.reserve(K);
	
	//convert 2D measurements to BA datastructs
	for (unsigned int i=0; i<projlen; i++) {
		Vector3d p, np;

		p[0] = dprojs[i*4+2];
		p[1] = dprojs[i*4+3];
		p[2] = 1.0;
		
		// Normalize the measurements to match the unit focal length.
		scaleVectorIP(1.0/f0, p);
		measurements.push_back(Vector2d(p[0], p[1]));
		correspondingView.push_back((int)dprojs[i*4+1]);
		correspondingPoint.push_back((int)dprojs[i*4]);

        //fprintf( stdout, "New point %f, %f for view %d and point id %d\n", p[0], p[1], (int)dprojs[i*4+1], (int)dprojs[i*4] );
	}

	K = measurements.size();
	
	cout << "Read " << K << " valid 2D measurements." << endl;
	
	showErrorStatistics(f0, distortion, mycams, Xs, measurements, correspondingView, correspondingPoint);

    env->ReleaseDoubleArrayElements(projs, dprojs, 0);

    /** Calling here **/
    V3D::optimizerVerbosenessLevel = 1;
	double const inlierThreshold = 2.0 / fabs(f0);
	
	Matrix3x3d K0 = mycams[0].getIntrinsic();
	cout << "K0 = "; displayMatrix(K0);

	bool good_adjustment = false;
	{
		ScopedBundleExtrinsicNormalizer extNorm(mycams, Xs);
		ScopedBundleIntrinsicNormalizer intNorm(mycams,measurements,correspondingView);
		CommonInternalsMetricBundleOptimizer opt(V3D::FULL_BUNDLE_RADIAL, inlierThreshold, K0, distortion, mycams, Xs,
												 measurements, correspondingView, correspondingPoint);
//		StdMetricBundleOptimizer opt(inlierThreshold,cams,Xs,measurements,correspondingView,correspondingPoint);
		
		opt.tau = 1e-3;
		opt.maxIterations = 50;
		opt.minimize();
		
		cout << "optimizer status = " << opt.status << endl;
		
		good_adjustment = (opt.status != 2);
	}
	
	cout << "refined K = "; displayMatrix(K0);
	
	for (int i = 0; i < N; ++i) mycams[i].setIntrinsic(K0);
	
	Matrix3x3d Knew = K0;
	scaleMatrixIP(f0, Knew);
	Knew[2][2] = 1.0;
	cout << "Knew = "; displayMatrix(Knew);
	
	showErrorStatistics(f0, distortion, mycams, Xs, measurements, correspondingView, correspondingPoint);
	
    /** converting it back here... **/

	if(good_adjustment) { //good adjustment?
	
		//extract 3D points

		for (unsigned int j = 0; j < M; ++j) {
			dpts[j*3] = Xs[j][0];
			dpts[j*3+1] = Xs[j][1];
			dpts[j*3+2] = Xs[j][2];
		}

		//extract adjusted cameras
		for (int i = 0; i < N; ++i) {
			Matrix3x3d R = mycams[i].getRotation();
			Vector3d T = mycams[i].getTranslation();

            for ( int j = 0; j < 3; j++ ) {
                for ( int k = 0; k < 3; k++ ) {
                    dcams[ i*12+j*4+k ] = R[j][k];
                }
            }
            dcams[ i*12+3 ] = T[0];
            dcams[ i*12+7 ] = T[1];
            dcams[ i*12+11 ] = T[2];
		}

        dintrinsics[ 0 ] = Knew[0][0];
        dintrinsics[ 4 ] = Knew[1][1];
        dintrinsics[ 1 ] = Knew[0][1];
        dintrinsics[ 2 ] = Knew[0][2];
        dintrinsics[ 5 ] = Knew[1][2];

        ddistorts[ 0 ] = distortion.k1;
        ddistorts[ 1 ] = distortion.k2;
        ddistorts[ 2 ] = distortion.p1;
        ddistorts[ 3 ] = distortion.p2;

        cout << "Distortion (k1k2p1p2):" << distortion.k1 << " " << distortion.k2 << " "<< distortion.p1 << " "<< distortion.p2 << endl;
	}

    env->ReleaseDoubleArrayElements(cams, dcams, 0);
    env->ReleaseDoubleArrayElements(pts, dpts, 0);
    env->ReleaseDoubleArrayElements(intrinsics, dintrinsics, 0);
    env->ReleaseDoubleArrayElements(distorts, ddistorts, 0);
}

inline void swap_host_big_endianness_8 (void *dst, void* src)
{
  char *dst_ = (char*) dst ;
  char *src_ = (char*) src ;
#if defined(SIFTBIG_ENDIAN)
    dst_ [0] = src_ [0] ;
    dst_ [1] = src_ [1] ;
    dst_ [2] = src_ [2] ;
    dst_ [3] = src_ [3] ;
    dst_ [4] = src_ [4] ;
    dst_ [5] = src_ [5] ;
    dst_ [6] = src_ [6] ;
    dst_ [7] = src_ [7] ;
#else
    dst_ [0] = src_ [7] ;
    dst_ [1] = src_ [6] ;
    dst_ [2] = src_ [5] ;
    dst_ [3] = src_ [4] ;
    dst_ [4] = src_ [3] ;
    dst_ [5] = src_ [2] ;
    dst_ [6] = src_ [1] ;
    dst_ [7] = src_ [0] ;
#endif
}

static void write_double( string& data, double x )
{
    double y ;

    swap_host_big_endianness_8 (&y, &x) ;
    data.append( reinterpret_cast< const char * >(&y), 8 );
   // data.append( 4, &y );
}


