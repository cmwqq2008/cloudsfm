package br.com.cridea;

import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class RawOutputFormat extends FileOutputFormat<Text, BytesWritable> {

    @Override
    public RecordWriter<Text, BytesWritable> getRecordWriter(TaskAttemptContext taskAttemptContext) 
    	throws IOException, InterruptedException 
    {
        Configuration conf = taskAttemptContext.getConfiguration();
        String extension = ".jpg";
        Path file = getDefaultWorkFile(taskAttemptContext, extension);
        FileSystem fs = file.getFileSystem(conf);
        FSDataOutputStream fileOut = fs.create(file, false);
        return new ByteRecordWriter(fileOut);
    }
   
    protected class ByteRecordWriter extends RecordWriter<Text, BytesWritable> {
        private DataOutputStream out;
 
        public ByteRecordWriter(DataOutputStream out) {
            this.out = out;
        }
       
        @Override
        public void write(Text key, BytesWritable value) throws IOException {
            out.write( value.getBytes(), 0, value.getLength());
        }
 
        @Override
        public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
            out.close();
        }
    }
}
