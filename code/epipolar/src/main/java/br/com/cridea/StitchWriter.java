package br.com.cridea;

import georegression.struct.homo.Homography2D_F64;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.transform.homo.HomographyPointOps_F64;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import boofcv.alg.distort.ImageDistort;
import boofcv.alg.distort.PixelTransformHomography_F32;
import boofcv.alg.distort.impl.DistortSupport;
import boofcv.alg.interpolate.impl.ImplBilinearPixel_F32;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.gui.feature.VisualizeFeatures;
import boofcv.struct.geo.AssociatedPair;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.MultiSpectral;
import br.com.cridea.MatchesProto.Matches;

public class StitchWriter {
	
	protected List<Point2D_F64> leftPts,rightPts;
	List<Integer> selected = new ArrayList<Integer>();
	private int assocLeft[],assocRight[];
	private Color colors[];
	protected BufferedImage leftImage,rightImage;
	private BufferedImage fundamental;
	protected double scaleLeft,scaleRight;
	private int width, height;
	protected boolean selectedIsLeft;

	public StitchWriter() {
		leftPts = null;
		rightPts = null;
		selected = new ArrayList<Integer>();
		assocLeft = null;
		assocRight = null;
		colors = null;
		leftImage = null;
		rightImage = null;
		fundamental = null;
	}
	
	public void writeStitches( Context context, Matches matches, List<AssociatedPair> inliers, MultipleOutputs<Text, BytesWritable> mos, Homography2D_F64 H ) 
		throws IOException, InterruptedException
	{
		BufferedImage A = loadImage( context.getConfiguration(), "input/images/" + matches.getImageName1() + ".JPG" );
		BufferedImage B = loadImage( context.getConfiguration(), "input/images/" + matches.getImageName2() + ".JPG" );
		BufferedImage stitched = renderStitching( A, B, H );
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(stitched, "jpg", baos);
		baos.flush();
		byte[] s = baos.toByteArray();
		baos.close();
		
		setAssociation(inliers);
		setImages(A,B);
		Graphics2D g2 = fundamental.createGraphics();
		renderFundamental(g2);
		
		baos = new ByteArrayOutputStream();
		ImageIO.write(fundamental, "jpg", baos);
		baos.flush();
		byte[] f = baos.toByteArray();
		baos.close();
		
		mos.write("stitches", new Text( matches.getImageName1() + ":" + matches.getImageName2() ), new BytesWritable( s ), "stitches/" + matches.getImageName1()+"-"+matches.getImageName2() );
		mos.write("fundamentals", new Text( matches.getImageName1() + ":" + matches.getImageName2() ), new BytesWritable( f ), "fundamentals/" + matches.getImageName1()+"-"+matches.getImageName2() );
	}
	
	private BufferedImage loadImage( Configuration conf, String filename )
			throws IOException
		{
	        final Path file = new Path( filename );
	        FileSystem fs = file.getFileSystem(conf);
	        FSDataInputStream in = fs.open(file);	
	    	byte[] newImage = new byte[ in.available() ];
	        in.readFully( newImage );
	        ByteArrayInputStream bis = new ByteArrayInputStream( newImage );
			BufferedImage originalImage = ImageIO.read( bis );
			return originalImage;
		}
	
	/**
	 * Renders and displays the stitched together images
	 */
	public static BufferedImage renderStitching( BufferedImage imageA, BufferedImage imageB , Homography2D_F64 fromAtoB )
	{
		// specify size of output image
		double scale = 0.5;
		int outputWidth = imageA.getWidth();
		int outputHeight = imageA.getHeight();
 
		// Convert into a BoofCV color format
		MultiSpectral<ImageFloat32> colorA =
				ConvertBufferedImage.convertFromMulti(imageA, null,true, ImageFloat32.class);
		MultiSpectral<ImageFloat32> colorB =
				ConvertBufferedImage.convertFromMulti(imageB, null,true, ImageFloat32.class);
 
		// Where the output images are rendered into
		MultiSpectral<ImageFloat32> work = new MultiSpectral<ImageFloat32>(ImageFloat32.class,outputWidth,outputHeight,3);
 
		// Adjust the transform so that the whole image can appear inside of it
		Homography2D_F64 fromAToWork = new Homography2D_F64(scale,0,colorA.width/4,0,scale,colorA.height/4,0,0,1);
		Homography2D_F64 fromWorkToA = fromAToWork.invert(null);
 
		// Used to render the results onto an image
		PixelTransformHomography_F32 model = new PixelTransformHomography_F32();
		ImageDistort<MultiSpectral<ImageFloat32>> distort;
		distort = DistortSupport.createDistortMS(ImageFloat32.class, model, new ImplBilinearPixel_F32(), null);
 
		// Render first image
		model.set(fromWorkToA);
		distort.apply(colorA,work);
 
		// Render second image
		Homography2D_F64 fromWorkToB = fromWorkToA.concat(fromAtoB,null);
		model.set(fromWorkToB);
		distort.apply(colorB,work);
 
		// Convert the rendered image into a BufferedImage
		BufferedImage output = new BufferedImage(work.width,work.height,imageA.getType());
		ConvertBufferedImage.convertTo(work,output,true);
 
		Graphics2D g2 = output.createGraphics();
 
		// draw lines around the distorted image to make it easier to see
		Homography2D_F64 fromBtoWork = fromWorkToB.invert(null);
		Point2D_I32 corners[] = new Point2D_I32[4];
		corners[0] = renderPoint(0,0,fromBtoWork);
		corners[1] = renderPoint(colorB.width,0,fromBtoWork);
		corners[2] = renderPoint(colorB.width,colorB.height,fromBtoWork);
		corners[3] = renderPoint(0,colorB.height,fromBtoWork);
 
		g2.setColor(Color.ORANGE);
		g2.setStroke(new BasicStroke(4));
		g2.drawLine(corners[0].x,corners[0].y,corners[1].x,corners[1].y);
		g2.drawLine(corners[1].x,corners[1].y,corners[2].x,corners[2].y);
		g2.drawLine(corners[2].x,corners[2].y,corners[3].x,corners[3].y);
		g2.drawLine(corners[3].x,corners[3].y,corners[0].x,corners[0].y);
		
		return output;
	}
	
	private static Point2D_I32 renderPoint( int x0 , int y0 , Homography2D_F64 fromBtoWork )
	{
		Point2D_F64 result = new Point2D_F64();
		HomographyPointOps_F64.transform(fromBtoWork, new Point2D_F64(x0, y0), result);
		return new Point2D_I32((int)result.x,(int)result.y);
	}
	
	public synchronized void setAssociation( List<AssociatedPair> matches ) 
	{
		List<Point2D_F64> leftPts = new ArrayList<Point2D_F64>();
		List<Point2D_F64> rightPts = new ArrayList<Point2D_F64>();

		for( AssociatedPair p : matches ) {
			leftPts.add(p.p1);
			rightPts.add(p.p2);
		}

		setLocation(leftPts,rightPts);

		assocLeft = new int[ leftPts.size() ];
		assocRight = new int[ rightPts.size() ];

		for( int i = 0; i < assocLeft.length; i++ ) {
			assocLeft[i] = i;
			assocRight[i] = i;
		}

		Random rand = new Random(234);
		colors = new Color[ leftPts.size() ];
		for( int i = 0; i < colors.length; i++ ) {
			colors[i] = new Color(rand.nextInt() | 0xFF000000 );
		}
	}
	
	public synchronized void setImages(BufferedImage leftImage , BufferedImage rightImage ) 
	{
		this.leftImage = leftImage;
		this.rightImage = rightImage;

		width = leftImage.getWidth() + rightImage.getWidth();
		height = Math.max(leftImage.getHeight(),rightImage.getHeight());
		fundamental = new BufferedImage(width,height,leftImage.getType());
	}
	
	public void setLocation( List<Point2D_F64> leftPts , List<Point2D_F64> rightPts) 
	{
		this.leftPts = leftPts;
		this.rightPts = rightPts;
		selected.clear();
	}
	
	public synchronized void renderFundamental(Graphics g) 
	{
		if( leftImage == null || rightImage == null )
			return;

		computeScales();

		Graphics2D g2 = (Graphics2D)g;

		// location in the current frame, taking in account the scale of each image
		int x1 = (int)(scaleLeft*leftImage.getWidth());
		int x2 = x1;
		int x3 = x2+(int)(scaleRight*rightImage.getWidth());
		int y1 = (int)(scaleLeft*leftImage.getHeight());
		int y2 = (int)(scaleRight*rightImage.getHeight());

		// draw the background images
		g2.drawImage(leftImage,0,0,x1,y1,0,0,leftImage.getWidth(),leftImage.getHeight(),null);
		g2.drawImage(rightImage,x2,0,x3,y2,0,0,rightImage.getWidth(),rightImage.getHeight(),null);

		drawFeatures(g2,scaleLeft,0,0,scaleRight,x2,0);
	}
	
	private void computeScales() 
	{
		int width = (this.width)/2;

		// compute the scale factor for each image
		scaleLeft = scaleRight = 1;
		if( leftImage.getWidth() > width || leftImage.getHeight() > height ) {
			double scaleX = (double)width/(double)leftImage.getWidth();
			double scaleY = (double)height/(double)leftImage.getHeight();
			scaleLeft = Math.min(scaleX,scaleY);
		}
		if( rightImage.getWidth() > width || rightImage.getHeight() > height ) {
			double scaleX = (double)width/(double)rightImage.getWidth();
			double scaleY = (double)height/(double)rightImage.getHeight();
			scaleRight = Math.min(scaleX,scaleY);
		}
	}
	
	protected void drawFeatures(Graphics2D g2 ,
			double scaleLeft, int leftX, int leftY,
			double scaleRight, int rightX, int rightY) 
	{
		if( selected.isEmpty() )
			drawAllFeatures(g2, scaleLeft,scaleRight,rightX);
		else {

			for( int selectedIndex : selected ) {
				// draw just an individual feature pair
				Point2D_F64 l,r;
				Color color;

				if( selectedIsLeft ) {
					l = leftPts.get(selectedIndex);
					if( assocLeft[selectedIndex] < 0 ) {
						r = null; color = null;
					} else {
						r = rightPts.get(assocLeft[selectedIndex]);
						color = colors[selectedIndex];
					}
				} else {
					r = rightPts.get(selectedIndex);
					if( assocRight[selectedIndex] < 0 ) {
						l = null; color = null;
					} else {
						l = leftPts.get(assocRight[selectedIndex]);
						color = colors[assocRight[selectedIndex]];
					}
				}

				if( color == null ) // clicking on something with no association is annoying
					drawAllFeatures(g2, scaleLeft,scaleRight,rightX);
				else
					drawAssociation(g2, scaleLeft,scaleRight,rightX, l, r, color);
			}
		}
	}
	
	private void drawAllFeatures(Graphics2D g2, double scaleLeft , double scaleRight , int rightX) {
		if( assocLeft == null || rightPts == null || leftPts == null )
			return;

		for( int i = 0; i < assocLeft.length; i++ ) {
			if( assocLeft[i] == -1 )
				continue;

			Point2D_F64 l = leftPts.get(i);
			Point2D_F64 r = rightPts.get(assocLeft[i]);

			Color color = colors[i];

			drawAssociation(g2, scaleLeft,scaleRight,rightX, l, r, color);
		}
	}

	private void drawAssociation(Graphics2D g2, double scaleLeft , double scaleRight , int rightX, Point2D_F64 l, Point2D_F64 r, Color color) {
		if( r == null ) {
			int x1 = (int)(scaleLeft*l.x);
			int y1 = (int)(scaleLeft*l.y);
			VisualizeFeatures.drawPoint(g2,x1,y1,Color.RED);
		} else if( l == null ) {
			int x2 = (int)(scaleRight*r.x) + rightX;
			int y2 = (int)(scaleRight*r.y);
			VisualizeFeatures.drawPoint(g2,x2,y2,Color.RED);
		} else {
			int x1 = (int)(scaleLeft*l.x);
			int y1 = (int)(scaleLeft*l.y);
			VisualizeFeatures.drawPoint(g2,x1,y1,color);

			int x2 = (int)(scaleRight*r.x) + rightX;
			int y2 = (int)(scaleRight*r.y);
			VisualizeFeatures.drawPoint(g2,x2,y2,color);

			g2.setColor(color);
			g2.drawLine(x1,y1,x2,y2);
		}
	}
	
}
