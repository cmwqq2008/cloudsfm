package br.com.cridea;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Epipolar extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		int res = ToolRunner.run(conf, new Epipolar(), args);
		System.exit(res);
	}
	
	public final int run(final String[] args) throws Exception {
		Configuration conf = getConf();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

		if (otherArgs.length != 3) {
			System.err.println("Usage: epipolar <output-match-jpgs> <in> <out>");
			System.exit(2);
		}
		
		if ( otherArgs[0].equals( "true" ) ) {
			conf.set( "epipolar.output", "true" );
		} else {
			conf.set( "epipolar.output", "false" );
		}
		
	    // set these *before* they're inserted into the job.
	    // makes a copy?
//	    conf.set( "matcher.threshold", otherArgs[0] );

	    Job job = new Job(conf, "epipolars");
	    job.setJarByClass(Epipolar.class);

	    job.setMapperClass(EpipolarMapper.class);
//	    job.setReducerClass(EpipolarReducer.class);
	    job.setNumReduceTasks( 0 );
	    
	    job.setInputFormatClass(MatchesInputFormat.class);

	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(BytesWritable.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(BytesWritable.class);
	    LazyOutputFormat.setOutputFormatClass(job, MatchesOutputFormat.class); 
	    MultipleOutputs.addNamedOutput(job, "epipolars", MatchesOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "stitches", RawOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "fundamentals", RawOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job,  "edges", TextOutputFormat.class, Text.class, Text.class );
	    FileInputFormat.addInputPath(job, new Path(otherArgs[1]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));
	    if (job.waitForCompletion(true)) { 
	    	return 0;
	    } else {
	    	return -1;
	    }
	}
}
