package br.com.cridea;

import java.io.IOException;

import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class EpipolarReducer extends Reducer<Text, BytesWritable, Text, BytesWritable> {
	public void reduce(Text key, Iterable<BytesWritable> values, Context context) 
		throws IOException, InterruptedException 
	{
	      for (BytesWritable val : values) {
	    	  context.write( key, val );
	      }
	}
}
