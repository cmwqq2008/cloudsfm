package br.com.cridea;


import georegression.fitting.homography.ModelManagerHomography2D_F64;
import georegression.fitting.se.ModelManagerSe3_F64;
import georegression.struct.homo.Homography2D_F64;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Vector3D_F64;
import georegression.struct.se.Se3_F64;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.ddogleg.fitting.modelset.DistanceFromModel;
import org.ddogleg.fitting.modelset.ModelFitter;
import org.ddogleg.fitting.modelset.ModelGenerator;
import org.ddogleg.fitting.modelset.ModelManager;
import org.ddogleg.fitting.modelset.ModelMatcher;
import org.ddogleg.fitting.modelset.ransac.Ransac;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import boofcv.abst.geo.Estimate1ofEpipolar;
import boofcv.abst.geo.TriangulateTwoViewsCalibrated;
import boofcv.abst.geo.fitting.DistanceFromModelResidual;
import boofcv.abst.geo.fitting.GenerateEpipolarMatrix;
import boofcv.abst.geo.fitting.ModelManagerEpipolarMatrix;
import boofcv.alg.distort.LensDistortionOps;
import boofcv.alg.geo.DecomposeHomography;
import boofcv.alg.geo.PerspectiveOps;
import boofcv.alg.geo.PositiveDepthConstraintCheck;
import boofcv.alg.geo.f.FundamentalResidualSampson;
import boofcv.alg.sfm.robust.DistanceHomographySq;
import boofcv.alg.sfm.robust.DistanceSe3SymmetricSq;
import boofcv.alg.sfm.robust.GenerateHomographyLinear;
import boofcv.alg.sfm.robust.Se3FromEssentialGenerator;
import boofcv.factory.geo.EnumEpipolar;
import boofcv.factory.geo.EpipolarError;
import boofcv.factory.geo.FactoryMultiView;
import boofcv.factory.geo.FactoryTriangulate;
import boofcv.struct.calib.IntrinsicParameters;
import boofcv.struct.distort.PointTransform_F64;
import boofcv.struct.geo.AssociatedPair;
import br.com.cridea.CameraIntrinsicsProto.CameraIntrinsics;
import br.com.cridea.MatchesProto.Matches;
import br.com.cridea.MatchesProto.Matches.Match;
import br.com.cridea.TransformProto.Transforms;
import br.com.cridea.TransformProto.Transforms.CoordPair;

public class EpipolarMapper extends Mapper<Text, BytesWritable, Text, BytesWritable> {

	private MultipleOutputs<Text, BytesWritable> mos;
	private boolean output = false;
	private double Pquality = 0.0;
	
	static enum Epipolar { FUNDMATRIXOK, FUNDMATRIXFAIL, HOMOGRAPHYFAIL, HOMOGRAPHYOK, ATTEMPTS, NUMCOORDS }
	
	@Override
	protected void setup(Context context)
		throws IOException, InterruptedException 
	{
		Configuration conf = context.getConfiguration();
		
		if ( conf.get( "epipolar.output" ).equals( "true" ) ) {
			output = true;
		} else {
			output = false;
		}
	
	    mos = new MultipleOutputs(context);
	}

	public void map(Text key, BytesWritable value, Context context) 
		throws IOException, InterruptedException 
	{
		Matches matches = Matches.parseFrom( value.getBytes() );
		List<AssociatedPair> pairs = new ArrayList<AssociatedPair>();
		
		context.getCounter(Epipolar.ATTEMPTS).increment( 1 );

		for ( int i = 0; i < matches.getMatchCount(); i++ ) {
			Match match = matches.getMatch( i );
			AssociatedPair pair = new AssociatedPair();
			pair.p1 = new Point2D_F64();
			pair.p2 = new Point2D_F64();
			pair.p1.x = match.getX1();
			pair.p1.y = match.getY1();
			pair.p2.x = match.getX2();
			pair.p2.y = match.getY2();
			
			pairs.add( pair );
		}			
		
		if ( pairs.size() < 8 ) {
			System.out.println("bad");
			context.getCounter(Epipolar.FUNDMATRIXFAIL).increment( 1 );
			return;
		}

		List<AssociatedPair> inliers = new ArrayList<AssociatedPair>();
		DenseMatrix64F F = robustFundamental( pairs, inliers );

		if (( F == null ) || ( inliers.size() < 16 )) {
			if ( F == null ) {
				System.out.println("F==null");
			} else {
				System.out.println("inliers.size() < 16, initial matches: " + matches.getMatchCount() );
			}
			context.getCounter(Epipolar.FUNDMATRIXFAIL).increment( 1 );
			return;
		} else {
			System.out.println("Adding one matrix");
			context.getCounter(Epipolar.FUNDMATRIXOK).increment( 1 );
			
			ModelManager<Homography2D_F64> manager = new ModelManagerHomography2D_F64();
			GenerateHomographyLinear modelFitter = new GenerateHomographyLinear(true);
			DistanceHomographySq distance = new DistanceHomographySq();

			ModelMatcher<Homography2D_F64,AssociatedPair> modelMatcher =
					new Ransac<Homography2D_F64,AssociatedPair>(123,manager,modelFitter,distance,4000,9);

			if( !modelMatcher.process(inliers) ) {
				context.getCounter(Epipolar.HOMOGRAPHYFAIL).increment( 1 );
				return;
			}

			System.out.println( matches.getImageName1() + ":" + matches.getImageName2() + " with quality: " + modelMatcher.getFitQuality() );
			
			/** MAGIC NUMBER! **/
			if ( modelMatcher.getFitQuality() < 13.0 ) {
				context.getCounter(Epipolar.HOMOGRAPHYFAIL).increment( 1 );
				return;
			}

			List<AssociatedPair> hInliers = modelMatcher.getMatchSet();
			// Reprocess again with homography inliers to improve solution.
			modelMatcher = new Ransac<Homography2D_F64,AssociatedPair>(123,manager,modelFitter,distance,128,6);
			if( !modelMatcher.process(hInliers) ) {
				context.getCounter(Epipolar.HOMOGRAPHYFAIL).increment( 1 );
				return;
			}
			
			context.getCounter(Epipolar.HOMOGRAPHYOK).increment( 1 );
			Homography2D_F64 H = modelMatcher.getModelParameters().copy();

//		    matrix_product(3, 3, 3, 3, K2_inv, H, tmp);
//		    matrix_product(3, 3, 3, 3, tmp, K1, Hnorm);
			IntrinsicParameters intrinsics1 = loadCameraIntrinsics( context.getConfiguration(), matches.getImageName1() );
			IntrinsicParameters intrinsics2 = loadCameraIntrinsics( context.getConfiguration(), matches.getImageName2() );
			
			DenseMatrix64F K1 = PerspectiveOps.calibrationMatrix( intrinsics1, null );
			DenseMatrix64F K2 = PerspectiveOps.calibrationMatrix( intrinsics2, null );
			DenseMatrix64F K2inv = K2.copy();
			DenseMatrix64F temp = new DenseMatrix64F( 3, 3 );

			double[][] mat = {{H.a11,H.a12,H.a13},{H.a21,H.a22,H.a23},{H.a31,H.a32,H.a33}};
			DenseMatrix64F densMat = new DenseMatrix64F(mat);
			
			CommonOps.invert( K2inv );
			CommonOps.mult( K2inv, densMat, temp );
			CommonOps.mult( temp,  K1,  densMat );
			
			DecomposeHomography dec = new DecomposeHomography();
			dec.decompose(densMat);
			System.out.println("decomposed:");
			for(Se3_F64 sol : dec.getSolutionsSE()) {
				System.out.println("SolSE: x = "+sol.getX()+", y = "+sol.getY()+", z = "+sol.getZ());
			}
			for(Vector3D_F64 sol : dec.getSolutionsN()) {
				System.out.println("SolN: x = "+sol.getX()+", y = "+sol.getY()+", z = "+sol.getZ());
			}
			
			Se3_F64 bestPose = selectBestPose( dec.getSolutionsSE(), hInliers );
			
			br.com.cridea.TransformProto.Transforms.Builder transformBuilder = Transforms.newBuilder();
			transformBuilder.setImageName1( matches.getImageName1() );
			transformBuilder.setImageName2( matches.getImageName2() );

			for ( int i = 0; i < 3; i++ ) {
				for ( int j = 0; j < 3; j++ ) {
					transformBuilder.addF( F.get( i, j ));
				}
			}
			for ( int i = 0; i < 3; i++ ) {
				for ( int j = 0; j < 3; j++ ) {
					transformBuilder.addH( H.get( i, j ));
				}
			}			

			transformBuilder.setNumF( 9 );
			transformBuilder.setNumH( 9 );
			transformBuilder.setNumP( 12 );
			transformBuilder.setNumR( 9 );
			transformBuilder.setNumT( 3 );
			transformBuilder.setFNumInliers( inliers.size() );
			transformBuilder.setHquality( modelMatcher.getFitQuality() );
			
			List<AssociatedPair> outliers = new ArrayList<AssociatedPair>();
			for ( AssociatedPair pair: pairs ) {
				outliers.add( pair );
			}
			outliers.removeAll( inliers );
			if ( output ) {
				// Writes fundamentals and stitches to file.
				StitchWriter writer = new StitchWriter();
				writer.writeStitches( context, matches, outliers, mos, H );
			}
			
			List<AssociatedPair> normCoords = convertToNormalizedCoordinates( pairs, intrinsics1, intrinsics2 );
			
			CameraIntrinsics i1 = loadCameraIntrinsicsProto( context.getConfiguration(), matches.getImageName1() );
			CameraIntrinsics i2 = loadCameraIntrinsicsProto( context.getConfiguration(), matches.getImageName2() );
			
			double d = 1.0f;
			if ( Math.abs( i1.getPosx() + 1 ) > 0.00001 ) {
				double dx = i1.getPosx() - i2.getPosx();
				double dy = i1.getPosy() - i2.getPosy();
				double dz = i1.getAlt() - i2.getAlt();
				d = Math.sqrt( dx*dx+dy*dy+dz*dz );
			}

			Se3_F64 P = estimateCameraMotion( intrinsics2, normCoords, inliers, d);

			normCoords = convertToNormalizedCoordinates( pairs, intrinsics1, intrinsics2 );

			transformBuilder.setPquality( Pquality );
			for ( int i = 0; i < 3; i++ ) {
				for ( int j = 0; j < 3; j++ ) {
					transformBuilder.addP( P.R.get( i, j ) );
				}
				transformBuilder.addP( P.T.getIndex( i ));
			}
			
			for ( int i = 0; i < 3; i++ ) {
				for ( int j = 0; j < 3; j++ ) {
					transformBuilder.addR( bestPose.R.get( i, j ) );
				}
				transformBuilder.addT( bestPose.T.getIndex( i ));
			}

			BufferedImage image1 = loadImage( context.getConfiguration(), matches.getImageName1() );
			int idx = 0;

			for ( AssociatedPair pair: normCoords ) {
				AssociatedPair inlierPair = pairs.get( idx );
				Match match = matches.getMatch( idx );
				br.com.cridea.TransformProto.Transforms.CoordPair.Builder pairBuilder = CoordPair.newBuilder();
				pairBuilder.setX1( pair.p1.x );
				pairBuilder.setY1( pair.p1.y );
				pairBuilder.setX2( pair.p2.x );
				pairBuilder.setY2( pair.p2.y );
				pairBuilder.setIndex1( match.getIdx1() );
				pairBuilder.setIndex2( match.getIdx2() );
				pairBuilder.setColor( image1.getRGB( (int)inlierPair.p1.x, (int)inlierPair.p1.y ) );

				transformBuilder.addCoords( pairBuilder.build() );
				idx++;
			}

			context.getCounter(Epipolar.NUMCOORDS).increment( normCoords.size() );

			Transforms t = transformBuilder.build();
			mos.write("epipolars", new Text( matches.getImageName1() + ":" + matches.getImageName2() ), new BytesWritable( t.toByteArray() ), matches.getImageName1() );
			mos.write("edges", new Text( matches.getImageName1() + ":" + matches.getImageName2() ), 
				new Text( new Integer( pairs.size() ).toString() + ":" + new Integer( inliers.size() ).toString() ), "edges" );
		}
	}

	public static DenseMatrix64F robustFundamental( List<AssociatedPair> matches, List<AssociatedPair> inliers ) 
	{
		// used to create and copy new instances of the fit model
		ModelManager<DenseMatrix64F> managerF = new ModelManagerEpipolarMatrix();
		// Select which linear algorithm is to be used.  Try playing with the number of remove ambiguity points
		Estimate1ofEpipolar estimateF = FactoryMultiView.computeFundamental_1(EnumEpipolar.FUNDAMENTAL_8_LINEAR, 2);
		// Wrapper so that this estimator can be used by the robust estimator
		GenerateEpipolarMatrix generateF = new GenerateEpipolarMatrix(estimateF);

		// How the error is measured
		DistanceFromModelResidual<DenseMatrix64F,AssociatedPair> errorMetric =
				new DistanceFromModelResidual<DenseMatrix64F,AssociatedPair>(new FundamentalResidualSampson());

		// Use RANSAC to estimate the Fundamental matrix
		ModelMatcher<DenseMatrix64F,AssociatedPair> robustF =
			new Ransac<DenseMatrix64F, AssociatedPair>(5684,managerF,generateF,errorMetric,6000,0.1);

		// Estimate the fundamental matrix while removing outliers
		if( !robustF.process(matches) ) {
			System.out.println("Could not find a fundamental matrix");
			return null;
		}

		// save the set of features that were used to compute the fundamental matrix
		inliers.addAll(robustF.getMatchSet());

		// Improve the estimate of the fundamental matrix using non-linear optimization
		DenseMatrix64F F = new DenseMatrix64F(3,3);
		ModelFitter<DenseMatrix64F,AssociatedPair> refine =
				FactoryMultiView.refineFundamental(1e-8, 4000, EpipolarError.SAMPSON);
		if( !refine.fitModel(inliers, robustF.getModelParameters(), F) ) {
			System.out.println( "Refining and refitting the model failed");
			return null;
		}

		// Return the solution
		return F;
	}

	protected void cleanup(org.apache.hadoop.mapreduce.Mapper<Text,BytesWritable,Text,BytesWritable>.Context context) 
		throws IOException ,InterruptedException 
	{
		mos.close();
	};
	
	private CameraIntrinsics loadCameraIntrinsicsProto( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/cameras/" + filename + ".cam" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	CameraIntrinsics intrinsics = CameraIntrinsics.parseFrom( in );
    	return intrinsics;
	}
	
	private IntrinsicParameters loadCameraIntrinsics( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/cameras/" + filename + ".cam" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	CameraIntrinsics intrinsics = CameraIntrinsics.parseFrom( in );
    	IntrinsicParameters params = new IntrinsicParameters();
    	
    	// fx 0 cx
    	//  0 fy cy
    	//  0 0  1
    	
    	params.cx = intrinsics.getCx();
    	params.cy = intrinsics.getCy();
    	params.flipY = false;
    	params.fx = intrinsics.getFx();
    	params.fy = intrinsics.getFy();
    	params.height = intrinsics.getHeight();
    	params.radial = new double[2];
    	params.radial[0] = intrinsics.getK1();
    	params.radial[1] = intrinsics.getK2();
    	params.width = intrinsics.getWidth();
    	
    	return params;
	}
	
	/**
	 * Estimates the camera motion robustly using RANSAC and a set of associated points.
	 *
	 * @param intrinsic   Intrinsic camera parameters
	 * @param matchedNorm set of matched point features in normalized image coordinates
	 * @param inliers     OUTPUT: Set of inlier features from RANSAC
	 * @return Found camera motion.  Note translation has an arbitrary scale
	 */
	public Se3_F64 estimateCameraMotion(IntrinsicParameters intrinsic, List<AssociatedPair> matchedNorm, List<AssociatedPair> inliers, double d )
	{
		Estimate1ofEpipolar essentialAlg = FactoryMultiView.computeFundamental_1(EnumEpipolar.FUNDAMENTAL_8_LINEAR, 2);
		TriangulateTwoViewsCalibrated triangulate = FactoryTriangulate.twoGeometric();
		ModelManager<Se3_F64> manager = new ModelManagerSe3_F64();
		ModelGenerator<Se3_F64, AssociatedPair> generateEpipolarMotion =
				new Se3FromEssentialGenerator(essentialAlg, triangulate);
 
		DistanceFromModel<Se3_F64, AssociatedPair> distanceSe3 =
			new DistanceSe3SymmetricSq(triangulate,
				intrinsic.fx, intrinsic.fy, intrinsic.skew,
				intrinsic.fx, intrinsic.fy, intrinsic.skew);
 
		// 1/2 a pixel tolerance for RANSAC inliers
		double ransacTOL = 0.5 * 0.5 * 2.0;
 
		ModelMatcher<Se3_F64, AssociatedPair> epipolarMotion =
			new Ransac<Se3_F64, AssociatedPair>(2323, manager, generateEpipolarMotion, distanceSe3,
					200, ransacTOL);
 
		if (!epipolarMotion.process(matchedNorm))
			throw new RuntimeException("Motion estimation failed");
 
		// save inlier set for debugging purposes
		inliers.addAll(epipolarMotion.getMatchSet());
		
		System.out.println("matchedNorm: " + matchedNorm.size());
		System.out.println("Best fits: " + inliers.size());
		
		Pquality = epipolarMotion.getFitQuality();
		Se3_F64 P = epipolarMotion.getModelParameters();

		return P;
	}
 
	/**
	 * Convert a set of associated point features from pixel coordinates into normalized image coordinates.
	 */
	public static List<AssociatedPair> convertToNormalizedCoordinates(List<AssociatedPair> matchedFeatures, IntrinsicParameters intrinsic1, IntrinsicParameters intrinsic2) {
 
		PointTransform_F64 tran1 = LensDistortionOps.transformRadialToNorm_F64(intrinsic1);
		PointTransform_F64 tran2 = LensDistortionOps.transformRadialToNorm_F64(intrinsic2);
//		PointTransform_F64 tran3 = LensDistortionOps.transformNormToRadial_F64(intrinsic1);
//		PointTransform_F64 tran4 = LensDistortionOps.transformNormToRadial_F64(intrinsic2);
		
		List<AssociatedPair> calibratedFeatures = new ArrayList<AssociatedPair>();
 
//		Point2D_F64 x = new Point2D_F64();
//		Point2D_F64 z = new Point2D_F64();
		
		for (AssociatedPair p : matchedFeatures) {
			AssociatedPair c = new AssociatedPair();
 		
			tran1.compute(p.p1.x, p.p1.y, c.p1);
			tran2.compute(p.p2.x, p.p2.y, c.p2);
			
//			String s = String.format( "Point %f,%f becomes %f,%f\n", p.p1.x, p.p1.y, c.p1.x, c.p1.y );
//			System.out.println( s );
//			s = String.format( "Point %f,%f becomes %f,%f\n", p.p2.x, p.p2.y, c.p2.x, c.p2.y );
//			System.out.println( s );
//
//			tran3.compute(c.p1.x, c.p1.y, x);
//			tran4.compute(c.p2.x, c.p2.y, z);
//			
//			s = String.format( "Point %f,%f becomes %f,%f\n", c.p1.x, c.p1.y, x.x, x.y );
//			System.out.println( s );
//			s = String.format( "Point %f,%f becomes %f,%f\n", c.p2.x, c.p2.y, z.x, z.y );
//			System.out.println( s );
			
			calibratedFeatures.add(c);
		}
 
		return calibratedFeatures;
	}
	
	private BufferedImage loadImage( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/images/" + filename + ".JPG" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	byte[] newImage = new byte[ in.available() ];
        in.readFully( newImage );
        ByteArrayInputStream bis = new ByteArrayInputStream( newImage );
		return ImageIO.read( bis );
	}
	
	private Se3_F64 selectBestPose(List<Se3_F64> motions, List<AssociatedPair> observations) {
		int bestCount = -1;
		Se3_F64 bestModel = null;

		TriangulateTwoViewsCalibrated triangulate = FactoryTriangulate.twoGeometric();
		PositiveDepthConstraintCheck depthChecker = new PositiveDepthConstraintCheck( triangulate );
		
		for( Se3_F64 s : motions ) {
			int count = 0;
			for( AssociatedPair p : observations ) {
				if( depthChecker.checkConstraint(p.p1,p.p2,s) ) {
					count++;
				}
			}
			
			if( count > bestCount ) {
				bestCount = count;
				bestModel = s;
			}
		}
		
		return bestModel;
	}
}
