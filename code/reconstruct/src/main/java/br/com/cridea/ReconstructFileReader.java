package br.com.cridea;


import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class ReconstructFileReader extends RecordReader<Text, BytesWritable>{
 
	private FSDataInputStream in;
	private Text key;
	private BytesWritable value;
	private long end;

    @Override
    public Text getCurrentKey() throws IOException,InterruptedException {
        return key;
    }
 
    @Override
    public BytesWritable getCurrentValue() throws IOException, InterruptedException {
        return value;
    }
 
    @Override
    public float getProgress() throws IOException, InterruptedException {
        return 1.0f;
    }
 
    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)
    			throws IOException, InterruptedException 
    {
        FileSplit split = (FileSplit) genericSplit;
        
        end = split.getLength();

        final Path file = split.getPath();
        Configuration conf = context.getConfiguration();
        FileSystem fs = file.getFileSystem(conf);
        in = fs.open(split.getPath());
    }
 
    @Override
    public boolean nextKeyValue() 
    	throws IOException, InterruptedException 
    {
    	if ( in == null ) {
    		return false;
    	}

    	if ( in.available() == 0 ) {
    		return false;
    	}
    	
        int length = in.readInt();
        int matchlength = in.readInt();
        byte[] keystr = new byte[length];
        in.readFully( keystr );
        
        byte[] b = new byte[ matchlength ];
        in.readFully( b );
        
        key = new Text( keystr );
        value = new BytesWritable( b );
        
        if (( in.available() == 0 ) || ( in.getPos() >= end )) {
        	in.close();
        	in = null;
        }
        
        return true;
    }

	@Override
	public void close() throws IOException {
		if ( in != null ) {
			in.close();
		}
	}
}