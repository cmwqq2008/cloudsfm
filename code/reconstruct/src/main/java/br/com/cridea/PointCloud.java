package br.com.cridea;

import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point3D_F64;
import georegression.struct.se.Se3_F64;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import boofcv.struct.calib.IntrinsicParameters;

public class PointCloud {
	
	public static class Point {
		public Point3D_F64 point = new Point3D_F64();
		public int[] indices;
		public int color;
		public int views;
		public Map<Integer, Point2D_F64> points2d = new HashMap<Integer, Point2D_F64>();
		
		public Point( Point3D_F64 point, int numidx, int color ) {
			super();
			this.point = point;
			indices = new int[ numidx ];
			for ( int i = 0; i < numidx; i++ ) {
				indices[i] = -1;
			}
			this.views = 0;
			this.color = color;
		}
	}

	public List<Point> points = new ArrayList<Point>();
	public Map<String,Integer> imgIndex = new HashMap<String,Integer>();
	public Map<Integer,IntrinsicParameters> intrinsics = new HashMap<Integer,IntrinsicParameters>();
	public int lastIdx = 0;
	public Map<Integer,Se3_F64> Pmats = new HashMap<Integer,Se3_F64>();

	public PointCloud() {
		super();
	}
}
