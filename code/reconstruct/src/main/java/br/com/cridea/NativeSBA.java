package br.com.cridea;

public class NativeSBA {
	static {
		System.loadLibrary("cloudsfm");
	}
	
	public native void runSBA( double[] cams, double[] points, double[] intrinsics, double projections[], double distortions[] );
}
