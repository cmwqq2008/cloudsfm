package br.com.cridea;

import georegression.struct.point.Point2D_F64;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper.Context;

import boofcv.alg.distort.LensDistortionOps;
import boofcv.gui.feature.VisualizeFeatures;
import boofcv.struct.calib.IntrinsicParameters;
import boofcv.struct.distort.PointTransform_F64;
import boofcv.struct.geo.Point2D3D;

public class OutlierWriter {
	
	protected List<Point2D_F64> leftPts;
	private int assocLeft[];
	private Color colors[];
	protected BufferedImage leftImage;
	private BufferedImage fundamental;
	protected double scaleLeft;
	private int width, height;
	private PointTransform_F64 tran1;
	
	public OutlierWriter() {
		leftPts = null;
		assocLeft = null;
		colors = null;
		leftImage = null;
		fundamental = null;
	}
	
	public void writeOutliers( Context context, IntrinsicParameters intrinsics, List<Point2D3D> outliers, String imageName ) 
		throws IOException, InterruptedException
	{
		BufferedImage A = loadImage( context.getConfiguration(), "input/images/" + imageName + ".JPG" );
		
		tran1 = LensDistortionOps.transformNormToRadial_F64( intrinsics );
		
		setAssociation(outliers);
		setImages(A);
		Graphics2D g2 = fundamental.createGraphics();
		renderFundamental(g2);
		
		File f = new File( "/tmp/" + imageName + ".jpg" );
		FileOutputStream baos = new FileOutputStream( f );
		ImageIO.write(fundamental, "jpg", baos);
		baos.flush();
		baos.close();	
	}
	
	private BufferedImage loadImage( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( filename );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	byte[] newImage = new byte[ in.available() ];
        in.readFully( newImage );
        ByteArrayInputStream bis = new ByteArrayInputStream( newImage );
		BufferedImage originalImage = ImageIO.read( bis );
		return originalImage;
	}
	
	public synchronized void setAssociation( List<Point2D3D> outliers ) 
	{
		List<Point2D_F64> leftPts = new ArrayList<Point2D_F64>();

		for( Point2D3D p : outliers ) {
			Point2D_F64 out = new Point2D_F64();
			tran1.compute(p.observation.x, p.observation.y, out );
			leftPts.add( out );
		}

		setLocation(leftPts);

		assocLeft = new int[ leftPts.size() ];

		for( int i = 0; i < assocLeft.length; i++ ) {
			assocLeft[i] = i;
		}

		Random rand = new Random(234);
		colors = new Color[ leftPts.size() ];
		for( int i = 0; i < colors.length; i++ ) {
			colors[i] = new Color(rand.nextInt() | 0xFF000000 );
		}
	}
	
	public synchronized void setImages(BufferedImage leftImage ) 
	{
		this.leftImage = leftImage;

		width = leftImage.getWidth();
		height = leftImage.getHeight();
		fundamental = new BufferedImage(width,height,leftImage.getType());
	}
	
	public void setLocation( List<Point2D_F64> leftPts) 
	{
		this.leftPts = leftPts;
	}
	
	public synchronized void renderFundamental(Graphics g) 
	{
		if( leftImage == null )
			return;

		computeScales();

		Graphics2D g2 = (Graphics2D)g;

		// location in the current frame, taking in account the scale of each image
		int x1 = (int)(scaleLeft*leftImage.getWidth());
		int y1 = (int)(scaleLeft*leftImage.getHeight());

		// draw the background images
		g2.drawImage(leftImage,0,0,x1,y1,0,0,leftImage.getWidth(),leftImage.getHeight(),null);

		drawFeatures(g2,scaleLeft,0,0);
	}
	
	private void computeScales() 
	{
		// compute the scale factor for each image
		scaleLeft = 1;
	}
	
	protected void drawFeatures(Graphics2D g2 ,
			double scaleLeft, int leftX, int leftY) 
	{
		drawAllFeatures(g2, scaleLeft);
	}
	
	private void drawAllFeatures(Graphics2D g2, double scaleLeft) {
		if( assocLeft == null ||leftPts == null )
			return;

		for( int i = 0; i < assocLeft.length; i++ ) {
			if( assocLeft[i] == -1 )
				continue;

			Point2D_F64 l = leftPts.get(i);

			Color color = colors[i];

			drawAssociation(g2, scaleLeft, l, color);
		}
	}

	private void drawAssociation(Graphics2D g2, double scaleLeft , Point2D_F64 l, Color color) {
		int x1 = (int)(scaleLeft*l.x);
		int y1 = (int)(scaleLeft*l.y);
		VisualizeFeatures.drawPoint(g2,x1,y1,color);
		System.out.println("Added " + x1 + " " + y1 + " to image.");
	}	
}
