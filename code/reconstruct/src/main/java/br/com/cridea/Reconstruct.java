package br.com.cridea;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Reconstruct extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		int res = ToolRunner.run(conf, new Reconstruct(), args);
		System.exit(res);
	}
	
	public final int run(final String[] args) throws Exception {
		Configuration conf = getConf();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

		if (otherArgs.length != 2) {
			System.err.println("Usage: reconstruct <in> <out>");
			System.exit(2);
		}

//		if ( otherArgs[0].equals( "true" ) ) {
//			conf.set( "epipolar.output", "true" );
//		} else {
//			conf.set( "epipolar.output", "false" );
//		}

	    // set these *before* they're inserted into the job.
	    // makes a copy?

	    Job job = new Job(conf, "reconstruct");
	    job.setJarByClass(Reconstruct.class);

	    job.setMapperClass(ReconstructMapper.class);
	    job.setInputFormatClass(ReconstructInputFormat.class);

	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(BytesWritable.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(BytesWritable.class);
	    LazyOutputFormat.setOutputFormatClass(job, ReconstructOutputFormat.class); 
	    MultipleOutputs.addNamedOutput(job, "cameras", ReconstructOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "ply", RawTxtOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "bundler", RawTxtOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "pcd", RawTxtOutputFormat.class, Text.class, BytesWritable.class);	    
	    MultipleOutputs.addNamedOutput(job, "txt", RawTxtOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "visualize", RawJpgOutputFormat.class, Text.class, BytesWritable.class);	 
	    MultipleOutputs.addNamedOutput(job, "rename", RawTxtOutputFormat.class, Text.class, BytesWritable.class);

	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    if (job.waitForCompletion(true)) { 
	    	return 0;
	    } else {
	    	return -1;
	    }
	}
}
