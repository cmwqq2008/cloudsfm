package br.com.cridea;

import georegression.fitting.se.ModelManagerSe3_F64;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point3D_F64;
import georegression.struct.point.Vector3D_F64;
import georegression.struct.se.Se3_F64;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.ddogleg.fitting.modelset.DistanceFromModel;
import org.ddogleg.fitting.modelset.ModelFitter;
import org.ddogleg.fitting.modelset.ModelManager;
import org.ddogleg.fitting.modelset.ModelMatcher;
import org.ddogleg.fitting.modelset.ransac.Ransac;
import org.ejml.alg.dense.linsol.svd.SolvePseudoInverseSvd;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import boofcv.abst.geo.Estimate1ofPnP;
import boofcv.abst.geo.RefineTriangulationCalibrated;
import boofcv.abst.geo.fitting.GenerateMotionPnP;
import boofcv.alg.distort.DistortImageOps;
import boofcv.alg.distort.ImageDistort;
import boofcv.alg.distort.LensDistortionOps;
import boofcv.alg.geo.PerspectiveOps;
import boofcv.alg.geo.pose.PnPDistanceReprojectionSq;
import boofcv.alg.geo.pose.PnPLepetitEPnP;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.core.image.border.BorderType;
import boofcv.factory.geo.FactoryMultiView;
import boofcv.factory.geo.FactoryTriangulate;
import boofcv.struct.calib.IntrinsicParameters;
import boofcv.struct.distort.PointTransform_F64;
import boofcv.struct.geo.Point2D3D;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.MultiSpectral;
import br.com.cridea.CameraIntrinsicsProto.CameraIntrinsics;
import br.com.cridea.CameraProto.Camera;
import br.com.cridea.PointCloud.Point;
import br.com.cridea.TransformProto.Transforms;
import br.com.cridea.TransformProto.Transforms.CoordPair;


public class ReconstructMapper extends Mapper<LongWritable, Text, Text, BytesWritable> {

	private MultipleOutputs<Text, BytesWritable> mos;
	private Map<String,String> files = new HashMap<String,String>();
	private PointCloud pc;
	private Integer numImages;
	private boolean first = false;
//	private int iteration = 0;
	
//	private final static int MAX_ITERATION_FOR_BA = 1;
	
	static enum Reconstruct { RECONSTRUCTOK }
	
	@Override
	protected void setup(Context context)
		throws IOException, InterruptedException 
	{
		Configuration conf = context.getConfiguration();
		final Path dir = new Path( "output/epipolars" );
		FileSystem fs = dir.getFileSystem(context.getConfiguration());
		RemoteIterator<LocatedFileStatus> ritr = fs.listFiles( dir, false );
		while ( ritr.hasNext() ) {
			Path p = ritr.next().getPath();
			String name = p.getName();
			if ( name.endsWith( "bin" )) {
				String path = name.substring(0, name.length() - 12);
				files.put( path, name );
			}
		}

		mos = new MultipleOutputs(context);
        pc = new PointCloud();
	}
	
	public void map(LongWritable key, Text value, Context context) 
		throws IOException, InterruptedException 
	{
		String[] sourcesdest = value.toString().split("\t");
		
		if ( sourcesdest[0].equals( "__NUM_IMAGES__" )) {
			numImages = Integer.parseInt( sourcesdest[1] );
			first = true;
			return;
		}
		
		IntrinsicParameters intrinsics2 = loadCameraIntrinsicsFile( context.getConfiguration(), sourcesdest[0] );
		DenseMatrix64F K2 = PerspectiveOps.calibrationMatrix( intrinsics2, null );
		String[] sources = sourcesdest[1].split(":");
		List<Transforms> list = new ArrayList<Transforms>();
		
		for ( int i = 0; i < sources.length; i++ ) {
			String srcfilename = files.get( sources[i] );
			
			Transforms transforms = loadEpipolars( context.getConfiguration(), "output/epipolars/" + srcfilename, sourcesdest[0] );
			list.add( transforms );
		}
		
		CameraIntrinsics i1 = loadCameraIntrinsics( context.getConfiguration(), list.get( 0 ).getImageName1() );
		CameraIntrinsics i2 = loadCameraIntrinsics( context.getConfiguration(), list.get( 0 ).getImageName2() );
		pc.intrinsics.put( pc.lastIdx, intrinsics2 );

		System.out.println("Mapping " + sourcesdest[0]);

		List<Point2D3D> points = new ArrayList<Point2D3D>();
		
		if ( pc.points.size() == 0 ) {
			addFirstPoints( pc, list, context, K2, intrinsics2, i1, i2 );
		} else {
			System.out.println("Cloud size: " + pc.points.size());
			
			PnPLepetitEPnP epnp = new PnPLepetitEPnP();
			epnp.setNumIterations( 10 );

			Map<String,Set<Integer>> toTriangulate = new HashMap<String,Set<Integer>>();

			for ( Transforms transforms: list ) {
				if (! pc.imgIndex.containsKey( transforms.getImageName1() )) {
					IntrinsicParameters ip = loadCameraIntrinsicsFile( context.getConfiguration(), transforms.getImageName1() );
					pc.intrinsics.put( new Integer( pc.lastIdx ), ip );
					System.out.println("Inserted ip for " + pc.lastIdx );
					pc.imgIndex.put( transforms.getImageName1(), new Integer( pc.lastIdx++ ) );
				}
				if (! pc.imgIndex.containsKey( transforms.getImageName2() )) {
					IntrinsicParameters ip = loadCameraIntrinsicsFile( context.getConfiguration(), transforms.getImageName2() );
					pc.intrinsics.put( pc.lastIdx, ip );
					System.out.println("Inserted ip for " + pc.lastIdx );
					pc.imgIndex.put( transforms.getImageName2(), new Integer( pc.lastIdx++ ) );
				}

				int idx1 = pc.imgIndex.get( transforms.getImageName1() );
				int idx2 = pc.imgIndex.get( transforms.getImageName2() );

				for ( int ti = 0; ti < transforms.getCoordsCount(); ti++ ) {
					CoordPair cp = transforms.getCoords( ti );
					int queryIdx = cp.getIndex1();
					boolean found = false;
					for ( int p = 0; p < pc.points.size(); p++ ) {
						Point mypoint = pc.points.get( p );
						if ( mypoint.indices[ idx1 ] == queryIdx ) {
							// This point had the same index for the source image
							if ( mypoint.indices[ idx2 ] != -1 ) {
								if ( mypoint.indices[idx2] != cp.getIndex2() ) {
									System.out.println("same 3d point registered to different points in view: " + transforms.getImageName2() );
								}
								continue;
							}
							Point2D3D p2d3d = new Point2D3D();
							p2d3d.setLocation( mypoint.point );
							p2d3d.setObservation( new Point2D_F64( cp.getX2(), cp.getY2() ) );
							points.add( p2d3d );
							
							// in multiple transforms the same point is found. This means it creates duplicates...
							// Should have a "status" list of points already added to avoid this.
							mypoint.indices[ idx2 ] = cp.getIndex2();
							mypoint.views++;
							
							mypoint.points2d.put( idx2, new Point2D_F64( cp.getX2(), cp.getY2() ));
							found = true;
						} 
					}
					if ( !found ) {
						if ( toTriangulate.containsKey( transforms.getImageName1() )) {
							Set<Integer> plist = toTriangulate.get( transforms.getImageName1() );
							plist.add( queryIdx );
						} else {
							Set<Integer> plist = new HashSet<Integer>();
							plist.add( queryIdx );
							toTriangulate.put( transforms.getImageName1(), plist );	
						}
					}
				}
			}

			Se3_F64 solutionModel = new Se3_F64();
			System.out.println("EstimatePnp " + sourcesdest[0] + ": with " + points.size() + " points." );

			List<Point2D3D> inliers = new ArrayList<Point2D3D>();
			if ( points.size() > 4 ) {
				solutionModel = robustPnp( points, inliers );
				points.removeAll( inliers );

				System.out.println("After filtering points, outliers: " + points.size() + ", cloud: " + pc.points.size() );

				List<Point> toRemove = new ArrayList<Point>();
				for ( int i = 0; i < points.size(); i++ ) {
					Point3D_F64 outlier = points.get( i ).location;
					for ( int p = 0; p < pc.points.size(); p++ ) {
						Point mypoint = pc.points.get( p );
						if ( mypoint.point.isIdentical( outlier, 1e-6 )) {
							toRemove.add( mypoint );
						}
					}
				}
				
				pc.points.removeAll( toRemove );
				System.out.println("Filtered point cloud: " + pc.points.size() );

				// sourcesdest == destination current image (image to be added)
				pc.Pmats.put( pc.imgIndex.get( sourcesdest[0] ), solutionModel );

				for ( Transforms transforms: list ) {
					int idx1 = pc.imgIndex.get( transforms.getImageName1() );
					int idx2 = pc.imgIndex.get( transforms.getImageName2() );

					List<Point2D_F64> pointsImg1 = new ArrayList<Point2D_F64>();
					List<Point2D_F64> pointsImg2 = new ArrayList<Point2D_F64>();
					List<Integer> colors = new ArrayList<Integer>();
					List<Integer> matchIdx = new ArrayList<Integer>();

					Set<Integer> plist = toTriangulate.get( transforms.getImageName1() );
					for ( int i = 0; i < transforms.getCoordsCount(); i++ ) {
						CoordPair cp = transforms.getCoords( i );
						if ( plist.contains( cp.getIndex1() ) ) {
							pointsImg1.add( new Point2D_F64( cp.getX1(), cp.getY1() ) );
							pointsImg2.add( new Point2D_F64( cp.getX2(), cp.getY2() ) );
							colors.add( cp.getColor() );
							matchIdx.add( i );
						}
					}

					List<Point3D_F64> pointCloud = new ArrayList<Point3D_F64>();
					Se3_F64 P = pc.Pmats.get( idx1 );
					Se3_F64 P1 = pc.Pmats.get( idx2 );
				
					List<Boolean> status = new ArrayList<Boolean>();
					double error = triangulatePoints( pointsImg1, pointsImg2, P, P1, K2, pointCloud, intrinsics2, status );
					System.out.println("Reconstruct " + transforms.getImageName1() + " to " + transforms.getImageName2() + ": error " + error );

					for ( int i = 0; i < pointCloud.size(); i++ ) {
						Point3D_F64 f = pointCloud.get( i );
						if ( status.get( i ) == Boolean.FALSE ) {
							continue;
						}
						Point p = new Point( f, numImages, colors.get( i ) );
						p.views = 2;
						
						CoordPair cp = transforms.getCoords( matchIdx.get( i ) );
						pc.points.add( p );
						p.indices[ idx1 ] = cp.getIndex1();
						p.indices[ idx2 ] = cp.getIndex2();
//						observations = new ArrayList<Point2D_F64>();
//						
//						observations.add( new Point2D_F64( cp.getX1(), cp.getY1() ) );
//						observations.add( new Point2D_F64( cp.getX2(), cp.getY2() ) );

						p.points2d.put( idx1, new Point2D_F64( cp.getX1(), cp.getY1() ) );
						p.points2d.put( idx2, new Point2D_F64( cp.getX2(), cp.getY2() ) );
						
//						p.point = refineTriangulation( observations, worldToCameras, p.point );
					}
				}
			}
		}

		int good = 0;
		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views > 2 ) {
				// Do not use views < 3.
				good++;
			}
		}

		if ( (pc.imgIndex.keySet().size() % 15 == 0 ) || ( pc.imgIndex.keySet().size() == numImages ))  {
			runSba( good );
		}
		if ( ( pc.imgIndex.keySet().size() == numImages ))  {
			dumpSparsePly( good );
			overwriteCameraIntrinsics();
			outputBundler( context.getConfiguration(), good );
			outputPCL( );
		}
	}
	
	public static Se3_F64 robustPnp( List<Point2D3D> points, List<Point2D3D> inliers ) 
	{	
		// used to create and copy new instances of the fit model
		ModelManager<Se3_F64> managerP = new ModelManagerSe3_F64();
		// Select which linear algorithm is to be used.  Try playing with the number of remove ambiguity points
		Estimate1ofPnP estimatePnP = FactoryMultiView.computePnPwithEPnP(10, 0.1);

		// Wrapper so that this estimator can be used by the robust estimator
		GenerateMotionPnP generateP = new GenerateMotionPnP(estimatePnP);

		// How the error is measured
		DistanceFromModel<Se3_F64,Point2D3D> errorMetric =
			new PnPDistanceReprojectionSq();

		// Use RANSAC to estimate the PNP matrix
		ModelMatcher<Se3_F64,Point2D3D> robustPnp =
			new Ransac<Se3_F64, Point2D3D>(123123,managerP,generateP,errorMetric,20000,0.00001);

		// Estimate the fundamental matrix while removing outliers
		if( !robustPnp.process(points) ) {
			System.out.println("Could not find a pnp solution");
			return null;
		}

		// save the set of features that were used to compute the fundamental matrix
		inliers.addAll(robustPnp.getMatchSet());

		System.out.println( inliers.size() + " inliers and " + points.size() + " points total" );
		
		// Improve the estimate of the fundamental matrix using non-linear optimization
		Se3_F64 P = new Se3_F64();
		ModelFitter<Se3_F64,Point2D3D> refine =
				FactoryMultiView.refinePnP( 1e-8, 200);
		
		if( !refine.fitModel(inliers, robustPnp.getModelParameters(), P) ) {
			System.out.println( "Refining and refitting the model failed");
			return null;
		}

		// Return the solution
		return P;
	}
	
	private void outputPCL() 
			throws IOException, InterruptedException 
	{
		// Print PCL file for understanding transformation
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		String line = null;
		
//		# .PCD v.7 - Point Cloud Data file format
//		VERSION .7
//		FIELDS x y z rgb
//		SIZE 4 4 4 4
//		TYPE F F F F
//		COUNT 1 1 1 1
//		WIDTH 213
//		HEIGHT 1
//		VIEWPOINT 0 0 0 1 0 0 0
//		POINTS 213
//		DATA ascii
//		0.93773 0.33763 0 4.2108e+06
//		0.90805 0.35641 0 4.2108e+06
//		0.81915 0.32 0 4.2108e+06
//		0.97192 0.278 0 4.2108e+06
//		0.944 0.29474 0 4.2108e+06
		
		bos.write( "# .PCD v.7 - Point Cloud Data file format\n".getBytes() );
		bos.write( "VERSION .7\n".getBytes() );
		bos.write( "FIELDS x y z\n".getBytes() );
		bos.write( "SIZE 4 4 4\n".getBytes() );
		bos.write( "TYPE F F F\n".getBytes() );
		bos.write( "COUNT 1 1 1\n".getBytes() );
		line = String.format( "WIDTH %d\n", pc.imgIndex.keySet().size() );
		bos.write( line.getBytes() );
		bos.write( "HEIGHT 1\n".getBytes() );
		bos.write( "VIEWPOINT 0 0 0 1 0 0 0\n".getBytes() );
		line = String.format( "POINTS %d\n", pc.imgIndex.keySet().size() );
		bos.write( line.getBytes() );
		bos.write( "DATA ascii\n".getBytes() );
		   	
		for ( int i = 0; i < pc.imgIndex.keySet().size(); i++ ) {
			Se3_F64 P = pc.Pmats.get( i );

			DenseMatrix64F T = new DenseMatrix64F( 3, 1 );
			T.data[ 0 ] = P.T.x;
			T.data[ 1 ] = P.T.y;
			T.data[ 2 ] = P.T.z;
						
			line = String.format( "%.12f %.12f %.12f\n", T.data[0], -T.data[1], -T.data[2] );
			bos.write( line.getBytes() );
		}
		mos.write("pcd", new Text( "pcd" ), new BytesWritable( bos.toByteArray() ), "cloud2.pcd" );
	}
	
	private void outputBundler( Configuration conf, int good ) throws IOException, InterruptedException {
		// Print bundle out file
		// only print ply at the end...
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		String line = null;
		
		bos.write( "# Bundle file v0.3\n".getBytes() );
		line = String.format( "%d %d\n", pc.imgIndex.keySet().size(), good );
		bos.write( line.getBytes() );
		
//		3251.32226563 0 0
//		0.0949476435781 -0.10939450562 0.989460468292
//		-0.995460927486 -0.0174259562045 0.093594878912
//		0.00700358347967 -0.993851006031 -0.110551908612
//		0.0752382576466 -0.101357340813 0.774130761623
		
//	    <f> <k1> <k2>   [the focal length, followed by two radial distortion coeffs]
//	    	    <R>             [a 3x3 matrix representing the camera rotation]
//	    	    <t>             [a 3-vector describing the camera translation]
    	
		for ( int i = 0; i < pc.imgIndex.keySet().size(); i++ ) {
			Se3_F64 P = pc.Pmats.get( i );

			IntrinsicParameters intrinsics = pc.intrinsics.get( i );
	    	DenseMatrix64F K = PerspectiveOps.calibrationMatrix( intrinsics, null );

			line = String.format( "%.12f %.12f %.12f\n", intrinsics.fx, 0.0, 0.0 ); //intrinsics.radial[0], intrinsics.radial[1] );
			bos.write( line.getBytes() );

			DenseMatrix64F R = P.R.copy();
			DenseMatrix64F T = new DenseMatrix64F( 3, 1 );
			T.data[ 0 ] = P.T.x;
			T.data[ 1 ] = P.T.y;
			T.data[ 2 ] = P.T.z;
			
			line = String.format( "%.12f %.12f %.12f\n", R.get( 0 ), R.get( 1 ), R.get( 2 ) );
			bos.write( line.getBytes() );
			line = String.format( "%.12f %.12f %.12f\n", -R.get( 3 ), -R.get( 4 ), -R.get( 5 ) );
			bos.write( line.getBytes() );
			line = String.format( "%.12f %.12f %.12f\n", -R.get( 6 ), -R.get( 7 ), -R.get( 8 ) );
			bos.write( line.getBytes() );
			
			line = String.format( "%.12f %.12f %.12f\n", T.data[0], -T.data[1], -T.data[2] );
			bos.write( line.getBytes() );
		}
		
//	    <position>      [a 3-vector describing the 3D position of the point]
//	    	    <color>         [a 3-vector describing the RGB color of the point]
//	    	    <view list>     [a list of views the point is visible in]
//		 The view list begins with the length of the list (i.e., the number of cameras the point is visible in). The list is then given as a list of quadruplets <camera> <key> <x> <y>,

//		-1.90900552273 6.60011148453 -1.54492008686
//		108 126 88
//		2 0 2 -1359.45068359 887.123413086 1 6 -1108.06518555 456.868652344
		
		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views > 2 ) {
				line = String.format( "%.12f %.12f %.12f\n", p.point.x, p.point.y, p.point.z );
				bos.write( line.getBytes() );
				
				int color = p.color;
				int r = (int)((color >> 16) & 0xFF);
				int g = (int)((color >> 8) & 0xFF);
				int b = (int)((color) & 0xFF);

				line = String.format( "%d %d %d\n", r, g, b );
				bos.write( line.getBytes() );
				
				line = String.format( "%d ", p.points2d.keySet().size() );
				bos.write( line.getBytes() );

				for ( Integer k: p.points2d.keySet() ) {
					Point2D_F64 p2d = p.points2d.get( k );
					
					IntrinsicParameters intrinsics = pc.intrinsics.get( k );
					PointTransform_F64 tran1 = LensDistortionOps.transformNormToRadial_F64( intrinsics );
				
					Point2D_F64 out = new Point2D_F64();
					tran1.compute(p2d.x, p2d.y, out );
					
					out.x = out.x - ((intrinsics.width-1) * 0.5);
					out.y = -( out.y - ((intrinsics.height-1) * 0.5));
					
					line = String.format( "%d %d %.12f %.12f ", k.intValue(), p.indices[ k ], out.x, out.y );
					bos.write( line.getBytes() );
				}
				bos.write( "\n".getBytes() );
			}
		}
//		bos.flush();
		mos.write("bundler", new Text( "bundler" ), new BytesWritable( bos.toByteArray() ), "bundler/bundler.out" );
		bos.close();
		
//		file = new File( basePath + "/list.txt" );
		bos = new ByteArrayOutputStream();
		ByteArrayOutputStream renamebos = new ByteArrayOutputStream();
		ByteArrayOutputStream renameCamBos = new ByteArrayOutputStream();

		String imgName = null;
		Map<Integer,String> tree = new TreeMap<Integer,String>();

		for ( Entry<String,Integer> e: pc.imgIndex.entrySet() ) {
			tree.put( e.getValue(), e.getKey() );
		}
		
		int idx = 0;
		for ( Entry<Integer,String> e: tree.entrySet() ) {
			imgName = e.getValue();
			System.out.println("Now working %s" + imgName);
			line = String.format( "visualize/%08d.jpg", idx );
			String realPath = String.format( "visualize/%s.jpg", imgName );
			bos.write( line.getBytes() );
			bos.write( "\n".getBytes() );

			renamebos.write( realPath.getBytes() );
			renamebos.write( "\t".getBytes() );
			renamebos.write( line.getBytes() );
			renamebos.write( "\n".getBytes() );
			
			// IMG_1387_geotag-m-00000.bin
			String binPath = String.format( "%s-m-00000.bin", imgName );
			String idxPath = String.format( "%08d.cam", idx );
			renameCamBos.write( binPath.getBytes() );
			renameCamBos.write( "\t".getBytes() );
			renameCamBos.write( idxPath.getBytes() );
			renameCamBos.write( "\n".getBytes() );
			
			BufferedImage img = loadImage( conf, imgName );
			
			IntrinsicParameters ip = pc.intrinsics.get( e.getKey() );
			MultiSpectral<ImageFloat32> ms = ConvertBufferedImage.convertFromMulti( img, null,true, ImageFloat32.class);
			MultiSpectral<ImageFloat32> output = new MultiSpectral<ImageFloat32>(ImageFloat32.class,
					ms.getWidth(),ms.getHeight(),ms.getNumBands());
			
			ImageDistort<ImageFloat32> imageDistort = LensDistortionOps.removeRadialImage( ip, BorderType.EXTENDED, ImageFloat32.class );
			DistortImageOps.distortMS(ms, output, imageDistort);
			
			img = ConvertBufferedImage.convertTo(output,null,true);
			
			ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
			ImageIO.write( img, "jpg", bos2 );
//			bos2.flush();
			line = String.format( "visualize/%08d.jpg", idx );
			mos.write("visualize", new Text( imgName ), new BytesWritable( bos2.toByteArray() ), "bundler/" + realPath );
			bos2.close();

			idx++;			
		}

		bos.flush();
		bos.close();
		
		renamebos.flush();
		renameCamBos.flush();
		mos.write( "rename", new Text( "rename" ), new BytesWritable( renamebos.toByteArray() ), "bundler/rename.txt" );
		mos.write( "rename", new Text( "rename2" ), new BytesWritable( renameCamBos.toByteArray() ), "rename.txt" );
		renamebos.close();
		renameCamBos.close();

		for ( int i = 0; i < pc.imgIndex.keySet().size(); i++ ) {
			Se3_F64 P = pc.Pmats.get( i );
			
			IntrinsicParameters intrinsics = pc.intrinsics.get( i );
	    	DenseMatrix64F K = PerspectiveOps.calibrationMatrix( intrinsics, null );
			
			String filename = String.format( "txt/%08d.txt", i );
			bos = new ByteArrayOutputStream();
			
	    	K.set( 0, -K.get( 0 ) );
	    	
			DenseMatrix64F R = P.R.copy();
			for ( int j = 3; j < 9; j++ ) {
				R.data[ j ] = -R.data[ j ];
			}
			Vector3D_F64 T = new Vector3D_F64();
			T.x = P.T.x;
			T.y = -P.T.y;
			T.z = -P.T.z;

			DenseMatrix64F Pfinal = PerspectiveOps.createCameraMatrix(R, T, K, null);    	
	    	CommonOps.scale( -1.0, Pfinal );
	    	
	    	bos.write( "CONTOUR\n".getBytes() );
			line = String.format( "%1.6f %1.6f %1.6f %1.6f\n", Pfinal.get( 0 ), Pfinal.get( 1 ), Pfinal.get( 2 ),  Pfinal.get( 3 ) );
			bos.write( line.getBytes() );
			line = String.format( "%1.6f %1.6f %1.6f %1.6f\n", Pfinal.get( 4 ), Pfinal.get( 5 ), Pfinal.get( 6 ),  Pfinal.get( 7 ) );
			bos.write( line.getBytes() );
			line = String.format( "%1.6f %1.6f %1.6f %1.6f\n", Pfinal.get( 8 ), Pfinal.get( 9 ), Pfinal.get( 10 ), Pfinal.get( 11 ) );
			bos.write( line.getBytes() );
			
//			bos.flush();
			mos.write("txt", new Text( line ), new BytesWritable( bos.toByteArray() ), "bundler/" + filename );	
			bos.close();
		}
	}

	private void addFirstPoints( PointCloud pc, List<Transforms> list, Context context, DenseMatrix64F K2, IntrinsicParameters intrinsics, CameraIntrinsics i1, CameraIntrinsics i2 ) 
		throws IOException
	{
		List<Point2D_F64> pointsImg1 = new ArrayList<Point2D_F64>();
		List<Point2D_F64> pointsImg2 = new ArrayList<Point2D_F64>();
		List<Integer> colors = new ArrayList<Integer>();
		
		assert( list.size() == 1 );
		Transforms transforms = list.get( 0 );
		
		for ( int i = 0; i < transforms.getCoordsCount(); i++ ) {
			CoordPair cp = transforms.getCoords( i );
			pointsImg1.add( new Point2D_F64( cp.getX1(), cp.getY1() ) );
			pointsImg2.add( new Point2D_F64( cp.getX2(), cp.getY2() ) );
			colors.add( cp.getColor() );
		}

		List<Point3D_F64> pointCloud = new ArrayList<Point3D_F64>();
		
		Se3_F64 P = new Se3_F64();
		Se3_F64 P1 = new Se3_F64();
		for ( int i = 0; i < 3; i++ ) {
			for ( int j = 0; j < 3; j++ ) {	
				P1.R.set( i, j, transforms.getP(i*4+j) );
			}
		}
		P1.T.setX( transforms.getP( 3 ));
		P1.T.setY( transforms.getP( 7 ));
		P1.T.setZ( transforms.getP( 11 ));

		P.R.set(0,0,1.0);
		P.R.set(1,1,1.0);
		P.R.set(2,2,1.0);
	
		List<Boolean> status = new ArrayList<Boolean>();
		double error = triangulatePoints( pointsImg1, pointsImg2, P, P1, K2, pointCloud, intrinsics, status );
		System.out.println("Reconstruct: mean error: " + error );
		
		if (! pc.imgIndex.containsKey( transforms.getImageName1() )) {
			IntrinsicParameters ip = loadCameraIntrinsicsFile( context.getConfiguration(), transforms.getImageName1() );
			pc.intrinsics.put( new Integer( pc.lastIdx ), ip );
			pc.imgIndex.put( transforms.getImageName1(), new Integer( pc.lastIdx++ ) );
		}
		if (! pc.imgIndex.containsKey( transforms.getImageName2() )) {
			IntrinsicParameters ip = loadCameraIntrinsicsFile( context.getConfiguration(), transforms.getImageName1() );
			pc.intrinsics.put( new Integer( pc.lastIdx ), ip );
			pc.imgIndex.put( transforms.getImageName2(), new Integer( pc.lastIdx++ ) );
		}

		int idx1 = pc.imgIndex.get( transforms.getImageName1() );
		int idx2 = pc.imgIndex.get( transforms.getImageName2() );
		
		for ( int i = 0; i < transforms.getCoordsCount(); i++ ) {
			Point p = new Point( pointCloud.get( i ), numImages, colors.get( i ) );
			Boolean mystat = status.get( i );
			if ( !mystat ) {
				continue;
			}
			CoordPair cp = transforms.getCoords( i );
			pc.points.add( p );

			p.views = 2;

			p.indices[ idx1 ] = cp.getIndex1();
			p.indices[ idx2 ] = cp.getIndex2();

			p.points2d.put( idx1, new Point2D_F64( cp.getX1(), cp.getY1() ) );
			p.points2d.put( idx2, new Point2D_F64( cp.getX2(), cp.getY2() ) );
		}
		
//		System.out.println(P);
//		System.out.println(P1);
		
		pc.Pmats.put( idx1, P );
		pc.Pmats.put( idx2, P1 );
	}

	protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,BytesWritable>.Context context) 
		throws IOException ,InterruptedException 
	{
		mos.close();
	};

	private DenseMatrix64F linearLSTriangulation( Point3D_F64 u, Se3_F64 P, Point3D_F64 u1, Se3_F64 P1, double wi, double wi1 )
	{
		DenseMatrix64F A = new DenseMatrix64F( 4, 3 );
		DenseMatrix64F B = new DenseMatrix64F( 4, 1 );
		DenseMatrix64F X = new DenseMatrix64F( 3, 1 );
		
		int i = 0;
		A.set(i++, (u.x*P.R.get( 2, 0 ) - P.R.get(0,0))/wi );
		A.set(i++, (u.x*P.R.get( 2, 1 ) - P.R.get(0,1))/wi );
		A.set(i++, (u.x*P.R.get( 2, 2 ) - P.R.get(0,2))/wi );
		A.set(i++, (u.y*P.R.get( 2, 0 ) - P.R.get(1,0))/wi );
		
		A.set(i++, (u.y*P.R.get( 2, 1 ) - P.R.get(1,1))/wi );
		A.set(i++, (u.y*P.R.get( 2, 2 ) - P.R.get(1,2))/wi );
		A.set(i++, (u1.x*P1.R.get( 2, 0 ) - P1.R.get(0,0))/wi1 );
		A.set(i++, (u1.x*P1.R.get( 2, 1 ) - P1.R.get(0,1))/wi1 );
		
		A.set(i++, (u1.x*P1.R.get( 2, 2 ) - P1.R.get(0,2))/wi1 );
		A.set(i++, (u1.y*P1.R.get( 2, 0 ) - P1.R.get(1,0))/wi1 );
		A.set(i++, (u1.y*P1.R.get( 2, 1 ) - P1.R.get(1,1))/wi1 );
		A.set(i++, (u1.y*P1.R.get( 2, 2 ) - P1.R.get(1,2))/wi1 );
		
		i = 0;
		B.set(i++,0, -(u.x*P.T.z - P.T.x)/wi );
		B.set(i++,0, -(u.y*P.T.z - P.T.y)/wi );
		B.set(i++,0, -(u1.x*P1.T.z - P1.T.x)/wi1 );
		B.set(i++,0, -(u1.y*P1.T.z - P1.T.y)/wi1 );
		
		//solve for X
		SolvePseudoInverseSvd svd = new SolvePseudoInverseSvd(3, 4);
		svd.setA( A );
		svd.solve( B, X );
		
		return X;
	}
	
	private DenseMatrix64F iterativeLinearLSTriangulation( Point3D_F64 u, Se3_F64 P, Point3D_F64 u1, Se3_F64 P1 )
	{
		DenseMatrix64F X = new DenseMatrix64F( 4, 1 );
		DenseMatrix64F P_r2 = new DenseMatrix64F( 1, 4 );
		DenseMatrix64F P1_r2 = new DenseMatrix64F( 1, 4 );
		DenseMatrix64F ret = new DenseMatrix64F(1,1);
		double EPSILON = 0.0001; 
		double wi = 1.0;
		double wi1 = 1.0;
		
		int idx = 0;
		for (int i=0; i<10; i++) { //Hartley suggests 10 iterations at most
			DenseMatrix64F X_ = linearLSTriangulation(u,P,u1,P1, wi, wi1);
			X.set( 0, X_.get( 0 ));
			X.set( 1, X_.get( 1 ));
			X.set( 2, X_.get( 2 ));
			X.set( 3, 1.0 );
			
			idx = 0;
			P_r2.set( idx, P.R.get( 2, idx ));
			idx++;
			P_r2.set( idx, P.R.get( 2, idx ));
			idx++;
			P_r2.set( idx, P.R.get( 2, idx ));
			idx++;
			P_r2.set( idx, P.T.z );

			idx = 0;
			P1_r2.set( idx, P1.R.get( 2, idx ));
			idx++;
			P1_r2.set( idx, P1.R.get( 2, idx ));
			idx++;
			P1_r2.set( idx, P1.R.get( 2, idx ));
			idx++;
			P1_r2.set( idx, P1.T.z );
			
			//recalculate weights
//			double p2x = Mat_<double>(Mat_<double>(P).row(2)*X)(0);
//			double p2x1 = Mat_<double>(Mat_<double>(P1).row(2)*X)(0);
			CommonOps.mult( P_r2, X, ret);
			double p2x = ret.get( 0 );
			CommonOps.mult( P1_r2, X, ret);
			double p2x1 = ret.get( 0 );

			//breaking point
			if ( Math.abs(wi - p2x) <= EPSILON && Math.abs(wi1 - p2x1) <= EPSILON) break;

			wi = p2x;
			wi1 = p2x1;			
		}
		return X;
	}
	
	private double triangulatePoints( List<Point2D_F64> pt_set1, List<Point2D_F64> pt_set2,
			Se3_F64 P, Se3_F64 P1, DenseMatrix64F K, List<Point3D_F64> pointCloud, IntrinsicParameters intrinsics2, List<Boolean> status)
	{
		List<Double> reproj_error = new ArrayList<Double>();
		DenseMatrix64F xPt_img = new DenseMatrix64F(3,1);
		DenseMatrix64F im = new DenseMatrix64F(3,4);
		PointTransform_F64 tran2 = LensDistortionOps.transformNormToRadial_F64( intrinsics2 );
		Point2D_F64 orig = new Point2D_F64();
		
		for ( int i = 0; i < pt_set1.size(); i++) {
			//convert to normalized homogeneous coordinates
			Point3D_F64 u = new Point3D_F64( pt_set1.get( i ).x, pt_set1.get( i ).y, 1.0 );
			Point3D_F64 u1 = new Point3D_F64( pt_set2.get( i ).x, pt_set2.get( i ).y, 1.0 );
			
			// triangulate
			DenseMatrix64F X = iterativeLinearLSTriangulation(u,P,u1,P1);
			
			//calculate reprojection error
			im = PerspectiveOps.createCameraMatrix(P1.R, P1.T, null, null);
			CommonOps.mult( im, X, xPt_img );

			tran2.compute( u1.x,  u1.y, orig );
			
			Point2D_F64 xPt_img_ = new Point2D_F64( xPt_img.get(0)/xPt_img.get(2), xPt_img.get(1)/xPt_img.get(2) );
			
			tran2.compute( xPt_img_.x, xPt_img_.y, xPt_img_ );
			
			double err = norm( xPt_img_, orig );
			pointCloud.add(new Point3D_F64(X.get(0),X.get(1),X.get(2)));
			
//			System.out.println("Error: " + xPt_img_ + " / " + orig + " / " + err ); 
			
			if ( err < 4.0 ) {
				status.add( true );
				reproj_error.add( err );
				// store 3D point
			} else {
				status.add( false );
			}
		}
		
		double me = 0.0f;
		for (int i = 0; i < reproj_error.size(); i++ ) {
			me += reproj_error.get( i ).doubleValue();
		}
		me /= reproj_error.size();
		return me;
	}
	
	private void runSba( int good ) {
		System.out.println("SBA started");
		
		NativeSBA sba = new NativeSBA();
		double[] cams = new double[ pc.lastIdx * 12 ];
		for ( int i = 0; i < (pc.lastIdx); i++ ) {
			Se3_F64 P = pc.Pmats.get( i );
			System.out.println(P);
			for ( int j = 0; j < 3; j++ ) {
				for ( int k = 0; k < 3; k++ ) {
					cams[ i*12 + j*4 + k ] = P.R.get( j, k );
				}
			}
			cams[ i*12 + 3] = P.T.x;
			cams[ i*12 + 7] = P.T.y;
			cams[ i*12 + 11] = P.T.z;				
		}
		double[] pts = new double[ good * 3 ];
		int idx = 0;
		int numprojs = 0;
		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views < 3 ) {
				continue;
			}
			pts[idx++] = p.point.x;
			pts[idx++] = p.point.y;
			pts[idx++] = p.point.z;
			numprojs += p.points2d.keySet().size();
		}
		double[] isics = new double[ 9 ];
		IntrinsicParameters ip = pc.intrinsics.get( 0 );
		DenseMatrix64F K = PerspectiveOps.calibrationMatrix( ip, null );
		for ( int j = 0; j < 9; j++ ) {
			isics[ j ] = K.get( j );
		}
		
		double[] projs = new double[ numprojs * 4 ];
		int m = 0;
		int l = 0;
		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views < 3 ) {
				continue;
			}
			for ( Integer k: p.points2d.keySet() ) {
				Point2D_F64 p2d = p.points2d.get( k );
				IntrinsicParameters intrinsics = pc.intrinsics.get( k );
				PointTransform_F64 tran1 = LensDistortionOps.transformNormToRadial_F64( intrinsics );
				Point2D_F64 out = new Point2D_F64();
				tran1.compute(p2d.x, p2d.y, out );
				projs[ m++ ] = l;
				projs[ m++ ] = k.doubleValue();
				projs[ m++ ] = out.x;
				projs[ m++ ] = out.y;
			}
			l++;
		}
		
		double distortions[] = new double[4];
		IntrinsicParameters intrinsics = pc.intrinsics.get( 0 );
		distortions[0] = intrinsics.getRadial()[0];
		distortions[1] = intrinsics.getRadial()[1];
		distortions[2] = 0.0;
		distortions[3] = 0.0;
		
		sba.runSBA(cams, pts, isics, projs, distortions);

		for ( int i = 0; i < (pc.lastIdx); i++ ) {
			Se3_F64 P = pc.Pmats.get( i );
			
			for ( int j = 0; j < 3; j++ ) {
				for ( int k = 0; k < 3; k++ ) {
					P.R.set( j, k, cams[ i*12 + j*4 + k ] );
				}
			}	
			P.T.x = cams[i*12+3];
			P.T.y = cams[i*12+7];
			P.T.z = cams[i*12+11];
			
			IntrinsicParameters newint = pc.intrinsics.get( i );
			newint.fx = isics[0];
			newint.skew = isics[1];
			newint.cx = isics[2];
			newint.fy = isics[4];
			newint.cy = isics[5];
			
			double[] radial = new double[2];
			radial[0] = distortions[0];
			radial[1] = distortions[1];
			newint.setRadial( radial );				
		}
		
		m = 0;
		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views > 2 ) {
				p.point.x = pts[m++];
				p.point.y = pts[m++];
				p.point.z = pts[m++];
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	private void dumpSparsePly( int good ) 
		throws IOException, InterruptedException 
	{
		// only print ply at the end...
//		File file = new File( "/tmp/final" + pc.imgIndex.keySet().size() + ".ply" );
//		BufferedOutputStream bos = new BufferedOutputStream( new FileOutputStream( file ));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		bos.write("ply\n".getBytes());
		bos.write("format ascii 1.0\n".getBytes());
		String s = new String( "element vertex " + good + "\n" );
		bos.write(s.getBytes());
		bos.write("property float x\n".getBytes());
		bos.write("property float y\n".getBytes());
		bos.write("property float z\n".getBytes());
		bos.write("property uchar red\n".getBytes());
		bos.write("property uchar green\n".getBytes());
		bos.write("property uchar blue\n".getBytes());
		bos.write("end_header\n".getBytes());

		for ( int i = 0; i < pc.points.size(); i++ ) {
			Point p = pc.points.get( i );
			if ( p.views > 2 ) {
				int color = p.color;
				int r = (int)((color >> 16) & 0xFF);
				int g = (int)((color >> 8) & 0xFF);
				int b = (int)((color) & 0xFF);
				s = new String( p.point.x + " " + p.point.y + " " + p.point.z + " " + r + " " + g + " " + b + "\n");
				bos.write( s.getBytes() );
			}
		}
//		bos.flush();
		mos.write("ply", new Text( "ply" ), new BytesWritable( bos.toByteArray() ), "sparse.ply" );
		bos.close();	
	}
	
	private Point3D_F64 refineTriangulation( List<Point2D_F64> observations, List<Se3_F64> worldToCameras, Point3D_F64 worldPt ) {
		RefineTriangulationCalibrated refine = FactoryTriangulate.refineSimple( 0.00001, 4000 );
		Point3D_F64 refinedPt = new Point3D_F64();
		refine.process( observations, worldToCameras, worldPt, refinedPt );
		return refinedPt;
	}

	private double norm( Point2D_F64 p1, Point2D_F64 p2 ) {
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		return Math.sqrt( dx*dx + dy*dy );
	}
	
	private CameraIntrinsics loadCameraIntrinsics( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/cameras/" + filename + ".cam" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	CameraIntrinsics intrinsics = CameraIntrinsics.parseFrom( in );
    	return intrinsics;
	}
	
	private IntrinsicParameters loadCameraIntrinsicsFile( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/cameras/" + filename + ".cam" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	CameraIntrinsics intrinsics = CameraIntrinsics.parseFrom( in );
    	
    	IntrinsicParameters params = new IntrinsicParameters();
    	
    	// fx 0 cx
    	//  0 fy cy
    	//  0 0  1
    	
    	params.cx = intrinsics.getCx();
    	params.cy = intrinsics.getCy();
    	params.flipY = false;
    	params.fx = intrinsics.getFx();
    	params.fy = intrinsics.getFy();
    	params.height = intrinsics.getHeight();
    	params.radial = new double[2];
    	params.radial[0] = intrinsics.getK1();
    	params.radial[1] = intrinsics.getK2();
    	params.width = intrinsics.getWidth();
    	return params;
	}
	
	private Transforms loadEpipolars( Configuration conf, String filename, String dest )
		throws IOException
	{
        final Path file = new Path( filename );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);

        while ( in.available() > 0 ) {
	        Integer keyLength = in.readInt();
	        int tLength = in.readInt();
	        byte[] key = new byte[ keyLength.intValue() ];
	        in.readFully(key);
	        byte[] tr = new byte[ tLength ];
	        in.readFully( tr );
	        Transforms transforms = Transforms.parseFrom( tr );
	        if ( transforms.getImageName2().equals( dest )) {
	        	return transforms;
	        }
        }
        return null;
	}
	
	public static DenseMatrix64F createCameraMatrix( DenseMatrix64F R , Vector3D_F64 T , DenseMatrix64F K , DenseMatrix64F ret ) {
		if( ret == null )
			ret = new DenseMatrix64F(3,4);
		
		CommonOps.insert(R,ret,0,0);
		
		ret.data[3] = T.x;
		ret.data[7] = T.y;
		ret.data[11] = T.z;
		
		if( K == null )
			return ret;
		
		DenseMatrix64F temp = new DenseMatrix64F(3,4);
		CommonOps.mult(ret,K,temp);
		
		ret.set(temp);
		
		return ret;
	}
	
	private void overwriteCameraIntrinsics() 
		throws IOException, InterruptedException
	{
		Map<Integer,String> tree = new TreeMap<Integer,String>();

		for ( Entry<String,Integer> e: pc.imgIndex.entrySet() ) {
			tree.put( e.getValue(), e.getKey() );
		}

		for ( Entry<Integer,String> e: tree.entrySet() ) {
			String imgName = e.getValue();
			Se3_F64 P = pc.Pmats.get( e.getKey() );
			IntrinsicParameters K = pc.intrinsics.get( e.getKey() );
			
			DenseMatrix64F Rt = P.R.copy();
			CommonOps.transpose( Rt );
			DenseMatrix64F T = new DenseMatrix64F( 3, 1 );
			T.data[ 0 ] = P.T.x;
			T.data[ 1 ] = P.T.y;
			T.data[ 2 ] = P.T.z;
			DenseMatrix64F C = new DenseMatrix64F( 3, 1 );
			CommonOps.mult( Rt, T,  C );
			CommonOps.scale( -1.0,  C );

			br.com.cridea.CameraProto.Camera.Builder cameraBuilder = Camera.newBuilder();
			
			cameraBuilder.setImageName( imgName );
			cameraBuilder.setAddorder( e.getKey() );
			cameraBuilder.setCx( K.cx );
			cameraBuilder.setCy( K.cy );
			cameraBuilder.setFx( K.fx );
			cameraBuilder.setFy( K.fy );
			cameraBuilder.setSkew( K.skew );
			cameraBuilder.setWidth( K.width );
			cameraBuilder.setHeight( K.height );
			cameraBuilder.setK1( K.radial[0] );
			cameraBuilder.setK2( K.radial[1] );
			
			cameraBuilder.setNumR( 9 );
			cameraBuilder.setNumT( 3 );
			for ( int i = 0; i < 9; i++ ) {
				cameraBuilder.addR( P.R.data[ i ] );
			}
			cameraBuilder.addT( P.T.x );
			cameraBuilder.addT( P.T.y );
			cameraBuilder.addT( P.T.z );

			cameraBuilder.setNumC( 3 );
			cameraBuilder.addC( C.data[0] );
			cameraBuilder.addC( C.data[1] );
			cameraBuilder.addC( C.data[2] );
			
			Camera t = cameraBuilder.build();
			mos.write("cameras", new Text( imgName ), new BytesWritable( t.toByteArray() ), imgName );
		}
	}
	
	private BufferedImage loadImage( Configuration conf, String filename )
		throws IOException
	{
        final Path file = new Path( "input/images/" + filename + ".JPG" );
        FileSystem fs = file.getFileSystem(conf);
        FSDataInputStream in = fs.open(file);	
    	byte[] newImage = new byte[ in.available() ];
        in.readFully( newImage );
        ByteArrayInputStream bis = new ByteArrayInputStream( newImage );
		return ImageIO.read( bis );
	}
}
