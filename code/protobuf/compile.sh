#!/bin/bash

PROTOC=/home/gt/cloudsift/compile/protobuf/protobuf-2.5.0/src/protoc

${PROTOC}  --java_out=../sifter/src/main/java keys.proto
${PROTOC}  --java_out=../sifter/src/main/java descriptors.proto
${PROTOC}  --java_out=../matcher/src/main/java keys.proto
${PROTOC}  --java_out=../matcher/src/main/java descriptors.proto
${PROTOC}  --java_out=../matcher/src/main/java matches.proto
${PROTOC}  --java_out=../epipolar/src/main/java matches.proto
${PROTOC}  --java_out=../epipolar/src/main/java transform.proto
${PROTOC}  --java_out=../epipolar/src/main/java cameraintrinsics.proto
${PROTOC}  --python_out=../../etc/scripts/hadoopdir cameraintrinsics.proto
${PROTOC}  --java_out=../reconstruct/src/main/java transform.proto
${PROTOC}  --java_out=../reconstruct/src/main/java cameraintrinsics.proto
${PROTOC}  --java_out=../reconstruct/src/main/java camera.proto
${PROTOC}  --java_out=../ortho/src/main/java transform.proto
${PROTOC}  --cpp_out=../../snippets/opencv-stitch/ camera.proto

