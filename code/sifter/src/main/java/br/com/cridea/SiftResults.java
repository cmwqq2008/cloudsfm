package br.com.cridea;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class SiftResults implements Writable {

	public String imageName;
	public int numFrames;
	public byte[] frames;
	public int numDescriptors;
	public byte[] descriptors;
	public int numBytesFrames;
	public int numBytesDescriptors;

	public SiftResults() {
		super();
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		numFrames = arg0.readInt();
		numDescriptors = arg0.readInt();		
		numBytesFrames = arg0.readInt();
		numBytesDescriptors = arg0.readInt();
		int len = arg0.readInt();
		byte[] imgName = new byte[ len ];
		arg0.readFully( imgName );
		imageName = new String( imgName );
		frames = new byte[ numBytesFrames ];
		descriptors = new byte[ numBytesDescriptors ];
		arg0.readFully( frames );
		arg0.readFully( descriptors );
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		arg0.writeInt( numFrames );
	    arg0.writeInt( numDescriptors );
		arg0.writeInt( numBytesFrames );
	    arg0.writeInt( numBytesDescriptors );
	    arg0.writeInt( imageName.length() );
	    arg0.write( imageName.getBytes() );
		arg0.write( frames );
	    arg0.write( descriptors );
	}
}
