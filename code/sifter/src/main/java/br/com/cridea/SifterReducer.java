package br.com.cridea;

import java.io.IOException;

import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class SifterReducer extends Reducer<Text, SiftResults, Text, SiftResults> {
	private MultipleOutputs<Text, BytesWritable> mos;
	
	static enum Features { FRAMES_BYTE_LEN, DESC_BYTE_LEN, IMAGES }
	
	public void reduce(Text key, Iterable<SiftResults> values, Context context) 
		throws IOException, InterruptedException 
	{
		String frameFileName = null;
		String descFileName = null;
		
	    for (SiftResults val : values) {
			frameFileName = generateFileName( "frames", key.toString() );
			descFileName = generateFileName( "descriptors", key.toString()  );

			mos.write("frames", key, new BytesWritable( val.frames ), frameFileName );
			mos.write("descriptors", key, new BytesWritable( val.descriptors ), descFileName );

			context.getCounter(Features.FRAMES_BYTE_LEN).increment(val.frames.length);
			context.getCounter(Features.DESC_BYTE_LEN).increment(val.descriptors.length);
	    }
	}

	@Override
	public void setup(Context context) {
	    mos = new MultipleOutputs(context);
	}

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }

    private String generateFileName( String subdir, String imageName ) {
    	String[] name = imageName.split( "\\." );
    	return subdir +"/"+name[0];
    }
}
