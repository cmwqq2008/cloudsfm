package br.com.cridea;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class SiftWritable implements Writable {

	public String imageName;
	public byte[] image;
	public int width;
	public int height;

	public SiftWritable() {
		super();
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
	    width = arg0.readInt();
	    height = arg0.readInt();
	    image = new byte[ width * height ];
	    arg0.readFully(image);
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		//write the size first
	    arg0.writeInt(width);
	    arg0.writeInt(height);
	    arg0.write( image );
	}
}
