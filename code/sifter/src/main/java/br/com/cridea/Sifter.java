package br.com.cridea;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Sifter extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		int res = ToolRunner.run(conf, new Sifter(), args);
		System.exit(res);
	}
	
	public final int run(final String[] args) throws Exception {
		Configuration conf = getConf();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
    
		if (otherArgs.length != 2) {
			System.err.println("Usage: sifter <in> <out>");
			System.exit(2);
		}

	    Job job = new Job(conf, "sifter");
	    job.setJarByClass(Sifter.class);
	    job.setMapperClass(SifterMapper.class);
//	    job.setCombinerClass(SifterReducer.class);
	    job.setReducerClass(SifterReducer.class);

//	    job.setMapOutputKeyClass(Text.class);
//	    job.setMapOutputValueClass(SiftWritable.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(SiftResults.class);
	    job.setInputFormatClass(ImageInputFormat.class);
	    LazyOutputFormat.setOutputFormatClass(job, SiftOutputFormat.class); 
	    MultipleOutputs.addNamedOutput(job, "frames", SiftOutputFormat.class, Text.class, BytesWritable.class);
	    MultipleOutputs.addNamedOutput(job, "descriptors", SiftOutputFormat.class, Text.class, BytesWritable.class);

	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    if (job.waitForCompletion(true)) { 
	    	return 1;
	    } else {
	    	return 0;
	    }
	}
}
