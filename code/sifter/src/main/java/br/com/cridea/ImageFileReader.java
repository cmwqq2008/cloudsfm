package br.com.cridea;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class ImageFileReader extends RecordReader<Text, SiftWritable>{
 
	private FSDataInputStream in;
	private Text key;
	private SiftWritable value;
	
    @Override
    public Text getCurrentKey() throws IOException,InterruptedException {
        return key;
    }
 
    @Override
    public SiftWritable getCurrentValue() throws IOException, InterruptedException {
        return value;
    }
 
    @Override
    public float getProgress() throws IOException, InterruptedException {
        return 1.0f;
    }
 
    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)
    			throws IOException, InterruptedException 
    {
        FileSplit split = (FileSplit) genericSplit;
        final Path file = split.getPath();
        Configuration conf = context.getConfiguration();
        FileSystem fs = file.getFileSystem(conf);
        in = fs.open(split.getPath());
      
        key = new Text( file.getName() );
    }
 
    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
    	if ( value != null ) {
    		return false;
    	}
    	
    	byte[] newImage = new byte[ in.available() ];
        in.readFully( newImage );
        ByteArrayInputStream bis = new ByteArrayInputStream( newImage );
		BufferedImage originalImage = ImageIO.read( bis );
		byte[] image = new byte[ originalImage.getWidth() * originalImage.getHeight() ];
	    
		WritableRaster raster = originalImage.getRaster();
        for(int y = 0; y < raster.getHeight(); y++) {
            for(int x = 0; x < raster.getWidth(); x++){
                Color c = new Color(originalImage.getRGB(x,y));
                int r = c.getRed();
                int g = c.getGreen();
                int b = c.getBlue();

                int l = (int) (.299 * r + .587 * g + .114 * b);
                image[ y * raster.getWidth() + x ] = (byte)l;
            }
        }

        value = new SiftWritable();
        value.imageName = key.toString();
        value.image = image;
        value.width = originalImage.getWidth();
        value.height = originalImage.getHeight();
        
//        InputStream ins = new ByteArrayInputStream(image);
//        BufferedImage grey = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY );
//        grey.getRaster().setDataElements(0, 0, originalImage.getWidth(), originalImage.getHeight(), image);        
//        ImageIO.write(grey, "jpg", new File( "/tmp/grey.jpg"));
        
        System.out.println("Printing from ImageFileReader");
        
        return true;
    }

	@Override
	public void close() throws IOException {
		if ( in != null ) {
			in.close();
		}
	}
}