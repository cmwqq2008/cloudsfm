package br.com.cridea;

public class NativeSifter {
	static {
		System.loadLibrary("cloudsfm");
	}
	
	public native double[][] runSift( byte[] image, int width, int height );
}
