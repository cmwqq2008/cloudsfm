package br.com.cridea;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class ImageInputFormat extends FileInputFormat<Text, SiftWritable> {
    @Override
    public RecordReader<Text, SiftWritable> createRecordReader(InputSplit split, TaskAttemptContext context) {
        return new ImageFileReader();
    }
    
    @Override
    protected boolean isSplitable(JobContext context, Path filename) {
    	return false;
    }
}