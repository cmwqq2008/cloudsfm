package br.com.cridea;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import br.com.cridea.DescriptorProto.Descriptors;
import br.com.cridea.KeyProto.Keys;

import com.google.protobuf.ByteString;

public class SifterMapper extends Mapper<Text, SiftWritable, Text, SiftResults> {
	
	private final static int NUM_FEATURES = 128;
	private final static int NUM_ATTRIBUTES = 4;
	
	static enum Features { NUMFRAMES, NUMDESCRIPTORS, IMAGES }
	
	public void map(Text key, SiftWritable value, Context context) 
		throws IOException, InterruptedException 
	{
		byte[] image = value.image;
		
		NativeSifter ns = new NativeSifter();
        double[][] framesAndDescriptors = ns.runSift( image, value.width, value.height );
               
        image = null;
        
        br.com.cridea.KeyProto.Keys.Builder keysBuilder = Keys.newBuilder().setImageName( value.imageName );
        double[] tmp = framesAndDescriptors[0];
        
        Set<Integer> outliers = new HashSet<Integer>();
        
        for ( int i = 0; i < tmp.length; i+= NUM_ATTRIBUTES ) {
        	
        	if (( tmp[i] < 4 ) || ( tmp[i] > ( value.width - 4 ))) {
        		// do not allow data too close to the borders
        		outliers.add( i / NUM_ATTRIBUTES );
        		System.out.println("Ignoring feature too close to the border " + i );
        		continue;
        	}
        	if (( tmp[i+1] < 4 ) || ( tmp[i+1] > ( value.width - 4 ))) {
        		// do not allow data too close to the borders
        		outliers.add( i / NUM_ATTRIBUTES );
        		System.out.println("Ignoring feature too close to the border " + i );
        		continue;
        	}
        	
        	br.com.cridea.KeyProto.Keys.Key.Builder keyBuilder = 
        		br.com.cridea.KeyProto.Keys.Key.newBuilder();
        	keyBuilder = keyBuilder.setX( tmp[i] );
        	keyBuilder = keyBuilder.setY( tmp[i+1] );
        	keyBuilder = keyBuilder.setSigma( tmp[i+2] );
        	keyBuilder = keyBuilder.setAngle( tmp[i+3] );
        	keysBuilder.addKey( keyBuilder.build() );
        }
        Keys keys = keysBuilder.build();
        
        br.com.cridea.DescriptorProto.Descriptors.Builder descsBuilder = Descriptors.newBuilder().setImageName( value.imageName );
        tmp = framesAndDescriptors[1];
    	ByteBuffer dataBuffer = ByteBuffer.allocateDirect(NUM_FEATURES);
    	dataBuffer.order(ByteOrder.nativeOrder());
    	
    	for ( int i = 0; i < tmp.length; i+= NUM_FEATURES ) {
        	br.com.cridea.DescriptorProto.Descriptors.Descriptor.Builder descBuilder = 
        		br.com.cridea.DescriptorProto.Descriptors.Descriptor.newBuilder();
        	
        	int check = i / NUM_FEATURES;
        	if ( outliers.contains( check )) {
        		continue;
        	}
        	
        	for ( int j = 0; j < NUM_FEATURES; j++ ) {
        		if ( tmp[i+j] < 255.0f ) {
        			dataBuffer.put((byte)tmp[i+j]);
        		} else {
        			dataBuffer.put((byte)0xFF);
        		}
        	}
        	dataBuffer.flip();
        	ByteString data = ByteString.copyFrom(dataBuffer);

        	descBuilder = descBuilder.setData( data );
        	descsBuilder.addDescriptor( descBuilder.build() );
        	
        	dataBuffer.position(0);
        }
        Descriptors descriptors = descsBuilder.build();
        
        SiftResults writable = new SiftResults();
        
        writable.imageName = value.imageName;
        writable.descriptors = descriptors.toByteArray(); 
        writable.frames = keys.toByteArray();
		writable.numBytesFrames = writable.frames.length;
		writable.numBytesDescriptors = writable.descriptors.length;
		writable.numFrames = keys.getKeyCount();
		writable.numDescriptors = descriptors.getDescriptorCount(); 
        
		context.getCounter(Features.IMAGES).increment(1);
		context.getCounter(Features.NUMFRAMES).increment(keys.getKeyCount());
		context.getCounter(Features.NUMDESCRIPTORS).increment(descriptors.getDescriptorCount());
        
        context.write( key, writable );
	}
}
