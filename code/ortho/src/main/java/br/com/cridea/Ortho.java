package br.com.cridea;

import georegression.struct.homo.Homography2D_F64;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.transform.homo.HomographyPointOps_F64;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import boofcv.alg.distort.ImageDistort;
import boofcv.alg.distort.PixelTransformHomography_F32;
import boofcv.alg.distort.impl.DistortSupport;
import boofcv.alg.interpolate.impl.ImplBilinearPixel_F32;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.MultiSpectral;
import br.com.cridea.TransformProto.Transforms;

public class Ortho {
 
	private static Point2D_I32 renderPoint( int x0 , int y0 , Homography2D_F64 fromBtoWork )
	{
		Point2D_F64 result = new Point2D_F64();
		HomographyPointOps_F64.transform(fromBtoWork, new Point2D_F64(x0, y0), result);
		return new Point2D_I32((int)result.x,(int)result.y);
	}
 
	private static List<String> getImageOrder( String pathfile ) 
		throws IOException 
	{
		List<String> result = new ArrayList<String>();
		File file = new File( pathfile );
		BufferedReader fr = new BufferedReader( new FileReader( file ) );
		fr.readLine();
		String line = fr.readLine();
		while( line != null ) {
			String[] parts = line.split("\t");	
			result.add( parts[ 0 ] );
			line = fr.readLine();
		}
		return result;
	}
	
	private static Map <String,String> getImages( String imagedir ) 
		throws IOException 
	{
		Map<String,String> images = new HashMap<String,String>();
		File folder = new File( imagedir );
		File[] listOfFiles = folder.listFiles(); 
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String img = listOfFiles[i].getName();
				String[] split = img.split(".jpg-m-");
				images.put( split[0], img);	
			}
		}
		return images;
	}
	
	private static Map <String,String> getTransforms( String transformdir ) 
		throws IOException 
	{
		Map<String,String> transforms = new HashMap<String,String>();
		File folder = new File( transformdir );
		File[] listOfFiles = folder.listFiles(); 
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String transform = listOfFiles[i].getName();
				if ( ! transform.endsWith( ".bin" )) {
					continue;
				}
				String[] split = transform.split("-m-");
				transforms.put( split[0], transform);	
			}
		}
		return transforms;
	}
	
	private static List<Integer> getWidthHeight( String basepath, String imageName ) throws IOException
	{
		List<Integer> wh = new ArrayList<Integer>();
		File file = new File( basepath + imageName );
		BufferedImage img = ImageIO.read( file );
		wh.add( img.getWidth() );
		wh.add( img.getHeight() );
		return wh;
	}
	
	public static void main( String args[] ) throws IOException {
		
		int width;
		int height;
		
		if ( args.length < 1 ) {
			System.err.println("Needs <arg> to input path");
			System.exit(-1);
		}
		String basePath = args[0];
		if ( ! basePath.endsWith( "/" ) ) {
			basePath = basePath +'/';
		}
		
		List<String> imgOrder = getImageOrder( basePath + "path.txt" );
		Map <String,String> images = getImages( basePath + "images/" );
		Map <String,String> transforms = getTransforms( basePath );

		Map <String,Homography2D_F64> homographies = new HashMap<String,Homography2D_F64>();
		
		List<Integer> wh = getWidthHeight( basePath + "images/", images.get( images.keySet().iterator().next() ) );
		width = wh.get( 0 );
		height = wh.get( 1 );
		
		double scale = 1.0;
		MultiSpectral<ImageFloat32> work = new MultiSpectral<ImageFloat32>(ImageFloat32.class,width,height,3);

		// Adjust the transform so that the whole image can appear inside of it
		Homography2D_F64 fromAToWork = new Homography2D_F64(scale, 0, width/4, 0, scale, height/8, 0, 0, 1 );
		Homography2D_F64 fromWorkToA = fromAToWork.invert(null);
		homographies.put( imgOrder.get( 0 ), fromWorkToA );

		for ( int i = 1; i < imgOrder.size(); i++ ) {
			String imgName = imgOrder.get( i );
			Transforms bestTransform = null;
			double quality = 0;
			
			for ( String t: transforms.values() ) {
				FileInputStream fis = new FileInputStream( new File( basePath + t ) );
				DataInputStream dis = new DataInputStream( fis );
			
				while ( dis.available() > 0 ) {
					int length = dis.readInt();
					int matchlength = dis.readInt();
					byte[] keystr = new byte[length];
					dis.readFully( keystr );
					byte[] b = new byte[ matchlength ];
					dis.readFully( b );
	
					Transforms transform = Transforms.parseFrom( b );
					if ( transform.getImageName2().equals( imgName )) {
						if (( transform.getHquality() > quality ) && ( homographies.containsKey( transform.getImageName1()) ) ) {
							bestTransform = transform;
							quality = transform.getHquality();
						}
					}
				}
			}
			Homography2D_F64 AtoB = new Homography2D_F64( bestTransform.getH( 0 ),bestTransform.getH( 1 ),bestTransform.getH( 2 ),
					bestTransform.getH( 3 ),bestTransform.getH( 4 ),bestTransform.getH( 5 ),
					bestTransform.getH( 6 ),bestTransform.getH( 7 ),bestTransform.getH( 8 ) );

			fromWorkToA = homographies.get( bestTransform.getImageName1() );
			Homography2D_F64 fromWorkToB = fromWorkToA.concat( AtoB, null );
			homographies.put( bestTransform.getImageName2(), fromWorkToB );
		}

		int left = 0;
		int top = 0;
		int right = 0;
		int bottom = 0;
		
		for ( int i = 1; i < imgOrder.size(); i++ ) {
			String imgName = imgOrder.get( i );
			Homography2D_F64 h = homographies.get( imgName );

			Point2D_I32 corners[] = new Point2D_I32[2];
			corners[0] = renderPoint( 0, 0, h);
			corners[1] = renderPoint( width, height, h);
			
			if ( corners[0].x < left ) {
				left = corners[0].x;
			}
			if ( corners[0].y < top ) {
				top = corners[0].y;
			}
			if ( corners[1].x > right ) {
				right = corners[1].x;
			}
			if ( corners[1].y > right ) {
				bottom = corners[1].y;
			}			
		}

		System.out.println( "l,r,t,b: " + left + ";" + right + ":" + top +":" + bottom );
		work = new MultiSpectral<ImageFloat32>(ImageFloat32.class, (right-left), (bottom-top), 3);
//		work = new MultiSpectral<ImageFloat32>(ImageFloat32.class, width, height, 3);
		for ( int i = 0; i < imgOrder.size(); i++ ) {
			String imgName = imgOrder.get( i );
			Homography2D_F64 h = homographies.get( imgName );
			
			// Used to render the results onto an image
			PixelTransformHomography_F32 model = new PixelTransformHomography_F32();
			ImageDistort<MultiSpectral<ImageFloat32>> distort =
					DistortSupport.createDistortMS(ImageFloat32.class, model, new ImplBilinearPixel_F32(), null);
 
			BufferedImage imageA = UtilImageIO.loadImage( basePath + "images/" + images.get( imgName ) );
			MultiSpectral<ImageFloat32> colorA =
					ConvertBufferedImage.convertFromMulti(imageA, null,true, ImageFloat32.class);

			// Render first image
			model.set(h);
			distort.apply( colorA, work );
		}
		
		BufferedImage output = new BufferedImage( work.width, work.height, 4 );
		ConvertBufferedImage.convertTo(work,output,true);
		
		UtilImageIO.saveImage( output, basePath + "orthomosaic.jpg" );
	}
}
